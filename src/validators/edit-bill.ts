import Joi from 'joi';
import {overrideMessages} from 'src/config/validator';
import {availableCurrencyCodes} from 'src/config/locale';

export const schema = Joi.object({
  payments: Joi.array()
    .items(
      Joi.object({
        useCurrencyExchange: Joi.boolean()
          .required()
          .label('使用匯率轉換'),
        item: Joi.string()
          .min(1)
          .max(100)
          .required()
          .label('項目'),
        amount: Joi.number()
          .greater(0)
          .max(Number.MAX_SAFE_INTEGER)
          .required()
          .label('金額'),
        sourceCurrency: Joi.when(
          'useCurrencyExchange',
          {
            is: false,
            then: Joi.forbidden(),
            otherwise: Joi.string().valid(...availableCurrencyCodes).required(),
          },
        )
          .label('貨幣'),
        exchangeRate: Joi.when(
          'useCurrencyExchange',
          {
            is: false,
            then: Joi.forbidden(),
            otherwise: Joi.number()
              .greater(0)
              .precision(2)
              .required(),
          },
        )
          .label('匯率'),
      })
        .required(),
    )
    .required()
    .label('款項'),
  host: Joi.string()
    .min(1)
    .max(100)
    .required()
    .label('付款人'),
  members: Joi.array()
    .min(1)
    .items({
      name: Joi.string()
        .min(1)
        .max(200)
        .required()
        .label('分攤人姓名'),
    })
    .required()
    .label('分攤人'),
})
  .strict()
  .messages(overrideMessages)
  .required();
