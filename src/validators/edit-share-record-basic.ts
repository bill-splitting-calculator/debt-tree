import Joi from 'joi';
import {availableCurrencyCodes} from 'src/config/locale';
import {overrideMessages} from 'src/config/validator';

const dateRegex = /^\d{4}-\d{2}-\d{2}$/;

export const schema = Joi.object({
  name: Joi.string()
    .min(1)
    .max(100)
    .required()
    .label('名稱'),
  mainHost: Joi.string()
    .min(1)
    .max(100)
    .allow('')
    .label('主要付款人'),
  dates: Joi.array()
    .ordered(
      Joi.string()
        .regex(dateRegex)
        .required()
        .label('開始日期'),
      Joi.string()
        .regex(dateRegex)
        .allow('')
        .label('結束日期'),
    )
    .required()
    .label('日期'),
  currency: Joi.string()
    .valid(...availableCurrencyCodes)
    .required()
    .label('使用貨幣'),
})
  .strict()
  .messages(overrideMessages)
  .required();
