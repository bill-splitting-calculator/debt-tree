export type Walk = (data: unknown, parentPath: string, onFound?: (finding: {path: string; data: unknown;}) => unknown) => unknown;

export const walk: Walk = (data, parentPath, onFound) => {
  if (!data || typeof data !== 'object') {
    return data;
  }

  return Object.entries(data)
    .reduce<Record<string, unknown>>((carry, [key, child]) => {
    const path = parentPath ? `${parentPath}.${key}` : key;
    const newChild = onFound?.({path, data: child}) ?? child;

    if (!newChild || typeof newChild !== 'object' || !Object.is(child, newChild)) {
      carry[key] = newChild;
    } else {
      if (Array.isArray(newChild)) {
        carry[key] = newChild.map((item, idx) => {
          const itemPath = `${path}.${idx}`;
          const newItem = onFound?.({path: itemPath, data: item}) ?? item;

          return walk(newItem, itemPath, onFound);
        });
      } else {
        carry[key] = walk(
          newChild,
          path,
          onFound,
        );
      }
    }

    return carry;
  }, {});
};
