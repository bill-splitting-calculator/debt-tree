export type ToKeyList = <T, K extends keyof T>(list: T[]) => Record<K, T[][]>;

export const toKeyList = <T extends Record<string, unknown>, K extends string>(list: T[], key: K) => (
  list.reduce<Record<string, T[]>>((carry, item) => {
    const {[key]: keyValue} = item;

    if (typeof keyValue !== 'string') return carry;

    if (!carry[keyValue]) {
      carry[keyValue] = [];
    }

    carry[keyValue].push(item);

    return carry;
  }, {} as unknown as Record<K, T[]>)
);
