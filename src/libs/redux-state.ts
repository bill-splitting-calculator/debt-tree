import lodashGet from 'lodash.get';
import lodashSet from 'lodash.set';
import {walk} from 'src/libs/object';
import type {RootState} from 'src/store';

export const localStorageKey = 'redux-state';

export type SaveToLocalStorage = (state: RootState) => void;

export const saveToLocalStorage: SaveToLocalStorage = state => {
  window.localStorage.setItem(
    localStorageKey,
    JSON.stringify(state),
  );
};

export const loadFromLocalStorage = () => {
  const rawState = window.localStorage.getItem(localStorageKey);
  const defaultState = {
    memberHistory: {
      memberReferences: [],
      memberSearchKeywords: [],
    },
    shareRecord: {
      shareRecords: [],
    },
  } as const;

  if (!rawState) return defaultState;

  const parsedState = JSON.parse(rawState);

  walk(defaultState, '', ({path, data}) => {
    if (!lodashGet(parsedState, path)) {
      lodashSet(parsedState, path, data);
    }
  });

  return parsedState;
};
