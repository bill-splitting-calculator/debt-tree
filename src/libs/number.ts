export type ParseFloat = (value: string) => number | undefined;

export const parseFloat: ParseFloat = value => {
  const {parseFloat: numberParseFloat, isNaN} = Number;
  const num = numberParseFloat(value);

  return !isNaN(num) ? num : undefined;
};
