export type GetMenuPosition = (options: {target: HTMLElement, menu: HTMLElement, offset: number}) => {
  top: number;
  left: number;
};

export const getVerticalMenuPosition: GetMenuPosition = ({target, menu, offset}) => {
  const {
    left: targetLeft,
    top: targetTop,
    height: targetHeight,
  } = target.getBoundingClientRect();
  const {height: menuHeight} = menu.getBoundingClientRect();
  const {innerHeight: winInnerHeight} = window;
  const defaultTop = targetTop + targetHeight + offset;

  return {
    top: defaultTop >= winInnerHeight
      ? targetTop - menuHeight - offset
      : defaultTop,
    left: targetLeft,
  };
};
