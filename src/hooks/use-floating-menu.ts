import debounce from 'lodash.debounce';
import type {MutableRefObject} from 'react';
import {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {getVerticalMenuPosition} from 'src/libs/menu-position';

export type UseFloatingMenuOptions = {
  offset?: number;
  menuScrollUpdateDelay?: number;
  disabled?: boolean;
};

export type UseFloatingMenu = (options?: UseFloatingMenuOptions) => {
  inputRef: MutableRefObject<HTMLInputElement | null>;
  menuRef: MutableRefObject<HTMLDivElement | null>;
  isMenuVisible: boolean;
  menuStyle: Record<string, string | number> | undefined;
  disabled: boolean;
  onIsMenuVisibleChange: (isMenuVisible: boolean) => void;
  onMenuStyleChange: (menuStyle: Record<string, string | number> | undefined) => void;
  onMenuStyleUpdate: () => void;
  onMenuHide: () => void;
  onDisabledChange: (disabled: boolean) => void;
};

export const useFloatingMenu: UseFloatingMenu = (options = {}) => {
  const {
    offset = 8,
    menuScrollUpdateDelay = 20,
    disabled: defaultDisabled = false,
  } = options;

  const [isMenuVisible, onIsMenuVisibleChange] = useState<boolean>(false);
  const [menuStyle, onMenuStyleChange] = useState<Record<string, string | number> | undefined>(undefined);
  const [disabled, onDisabledChange] = useState<boolean>(defaultDisabled);

  const inputRef = useRef<HTMLInputElement | null>(null);
  const menuRef = useRef<HTMLDivElement | null>(null);

  const onMenuHide = () => {
    onIsMenuVisibleChange(false);
    onMenuStyleChange(undefined);
  };

  const onMenuStyleUpdate = useCallback(
    () => {
      if (disabled) return;

      const {current: inputEl} = inputRef;
      const {current: menuEl} = menuRef;

      onMenuStyleChange(
        inputEl && menuEl
          ? {
            ...getVerticalMenuPosition({
              target: inputEl,
              menu: menuEl,
              offset,
            }),
            display: 'unset',
            width: inputEl.offsetWidth,
          }
          : undefined,
      );
    },
    [disabled, offset],
  );

  useEffect(() => {
    const onGlobalClick: EventListener = ({target}) => {
      const shouldPreventGlobalClick = disabled ||
        (
          target instanceof Element &&
          [inputRef, menuRef].some(ref => ref.current?.contains(target))
        );

      if (shouldPreventGlobalClick) return;

      onMenuHide();
    };

    document.addEventListener('click', onGlobalClick);

    return () => {
      document.removeEventListener('click', onGlobalClick);
    };
  }, [inputRef, menuRef, disabled, onMenuStyleUpdate]);

  const onDocumentDebouncedScroll = useMemo(
    () => debounce(
      onMenuStyleUpdate,
      menuScrollUpdateDelay,
    ),
    [onMenuStyleUpdate, menuScrollUpdateDelay],
  );

  useEffect(() => {
    document.addEventListener('scroll', onDocumentDebouncedScroll, true);

    return () => {
      document.removeEventListener('scroll', onDocumentDebouncedScroll);
    };
  }, [onDocumentDebouncedScroll]);

  return {
    inputRef,
    menuRef,
    isMenuVisible,
    menuStyle,
    disabled,
    onIsMenuVisibleChange,
    onMenuStyleChange,
    onMenuStyleUpdate,
    onMenuHide,
    onDisabledChange,
  };
};

