import {useEffect, useState} from 'react';

type AddHandlerOnce = (fn: () => void, timeout?: number) => void;

export type UseAfterRenderHandler = () => {
  addHandlerOnce: AddHandlerOnce;
};

export const useAfterRenderHandler: UseAfterRenderHandler = () => {
  const [afterRenderFnOnceMap, onAfterRenderFnOnceMapChange] = useState<Map<() => void, number | undefined>>(new Map());

  useEffect(() => {
    if (!afterRenderFnOnceMap.size) {
      return;
    }

    afterRenderFnOnceMap.forEach((timeout, fn) => {
      if (timeout === undefined) {
        fn();

        return;
      }

      window.setTimeout(fn, timeout);
    });

    onAfterRenderFnOnceMapChange(new Map());
  }, [afterRenderFnOnceMap, onAfterRenderFnOnceMapChange]);

  const addHandlerOnce: AddHandlerOnce = (fn, timeout) => {
    const newAfterRenderFnOnceMap = new Map(afterRenderFnOnceMap);

    if (timeout !== undefined && timeout < 0) {
      throw new Error('Invalid timeout');
    }

    newAfterRenderFnOnceMap.set(fn, timeout);

    onAfterRenderFnOnceMapChange(newAfterRenderFnOnceMap);
  };

  return {
    addHandlerOnce,
  };
};
