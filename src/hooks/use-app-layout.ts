import {useEffect, useState} from 'react';
import type {AppLayout} from 'src/types/layout';

export type UseAppLayout = () => {
  appLayout: AppLayout;
  getAppLayout: () => AppLayout | never;
};

export const useAppLayout: UseAppLayout = () => {
  const getAppLayout = () => {
    const layoutValue = JSON.parse(
      window.getComputedStyle(document.documentElement)
        .getPropertyValue('--app-layout'),
    );

    if (!['MOBILE', 'PAD', 'DESKTOP'].includes(layoutValue)) throw new Error('Invalid app layout');

    return layoutValue;
  };

  const [appLayout, onAppLayoutChange] = useState<AppLayout>(getAppLayout());

  useEffect(() => {
    const onWindowResize = () => {
      onAppLayoutChange(
        getAppLayout(),
      );
    };

    window.addEventListener('resize', onWindowResize);

    return () => {
      window.removeEventListener('resize', onWindowResize);
    };
  });

  return {
    getAppLayout,
    appLayout,
  };
};


