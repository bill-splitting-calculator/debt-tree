import {useEffect} from 'react';

export type UseAppHeight = () => {
  onAppHeightStyleUpdate: () => void;
};

export const useAppHeight: UseAppHeight = () => {
  const onAppHeightStyleUpdate = () => {
    document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`);
  };

  useEffect(() => {
    window.addEventListener('resize', onAppHeightStyleUpdate);

    onAppHeightStyleUpdate();

    return () => {
      window.removeEventListener('resize', onAppHeightStyleUpdate);
    };
  });

  return {
    onAppHeightStyleUpdate,
  };
};
