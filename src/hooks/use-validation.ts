import {
  useCallback,
  useState,
  useEffect,
} from 'react';
import {Schema, ValidationError} from 'joi';

type OnDataUpdate<T> = (data: Partial<T>) => void;
type ValidationResult = {
  result: boolean;
  error?: unknown;
}
type OnDataValidate = () => Promise<ValidationResult | undefined>;
type OnDataReset = () => void;

export type UseValidation = <T extends object, S extends Schema>(initData: T, schema: S) => {
  isDataChanged: boolean;
  data: T;
  onDataUpdate: OnDataUpdate<T>;
  validationError: ValidationError | null;
  errorDetailMap: Record<string, string>;
  onDataValidate: OnDataValidate;
  onDataReset: OnDataReset;
};

export const useValidation: UseValidation = (initData, schema) => {
  const [isDataChanged, setIsDataChanged] = useState<boolean>(false);
  const [data, setData] = useState<typeof initData>(initData);
  const [validationError, onValidationErrorChange] = useState<ValidationError | null>(null);

  const onDataUpdate = useCallback<OnDataUpdate<Partial<typeof initData>>>(newData => {
    if (!isDataChanged) {
      setIsDataChanged(true);
    }

    setData({
      ...data,
      ...newData,
    });
  }, [data, setData, isDataChanged, setIsDataChanged]);

  const onDataValidate = useCallback(async () => {
    if (!isDataChanged) return;

    try {
      await schema.validateAsync(data);

      onValidationErrorChange(null);

      return {
        result: true,
      };
    } catch (e) {
      if (e instanceof ValidationError) {
        onValidationErrorChange(e);
      }

      return {
        result: true,
        error: e,
      };
    }
  }, [data, schema, isDataChanged, onValidationErrorChange]);

  useEffect(() => {
    onDataValidate();
  }, [onDataValidate]);

  const errorDetailMap = validationError
    ? validationError.details
      .reduce((carry, {path, message}) => {
        const pathKey = path.join('.');

        return (pathKey in carry)
          ? carry
          : Object.assign(
            carry,
            {
              [pathKey]: message,
            },
          );
      }, {})
    : {};

  const onDataReset: OnDataReset = useCallback(() => {
    setIsDataChanged(false);
  }, [setIsDataChanged]);

  return {
    isDataChanged,
    data,
    validationError,
    errorDetailMap,
    onDataUpdate,
    onDataValidate,
    onDataReset,
  };
};
