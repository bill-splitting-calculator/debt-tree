import {useCallback, useMemo, useState} from 'react';
import {useAppDispatch, useAppSelector} from 'src/hooks';
import {
  addMemberReference,
  addMemberSearchKeyword,
  removeMemberSearchKeyword,
} from 'src/store/actions/member-history';
import type {Member} from 'src/types/resources';

type MemberSearchOptions = {
  keyword: string;
  limit?: number;
};

type SearchMembers = (options?: MemberSearchOptions) => Member[] | undefined;
type OnMemberReferenceAdd = (memberReference: Member) => void;
type OnMemberSearchOptionsChange = (options?: MemberSearchOptions) => void;
type IsMemberMatched = (memberReference: Member, keyword: string) => boolean;
type OnMemberSearchKeywordAdd = (keyword: string) => void;
type OnMemberSearchKeywordRemove = (index: number) => void;

const isMemberMatched: IsMemberMatched = (memberReference, keyword) => (
  memberReference.name.toLowerCase().includes(keyword.toLowerCase())
);

export type UseMemberHistory = () => {
  memberReferences: Member[];
  memberSearchResults?: Member[];
  memberSearchKeywords: string[];
  onMemberReferenceAdd: OnMemberReferenceAdd;
  onMemberSearchOptionsChange: OnMemberSearchOptionsChange;
  onMemberSearchKeywordAdd: OnMemberSearchKeywordAdd;
  onMemberSearchKeywordRemove: OnMemberSearchKeywordRemove;
};

export const useMemberHistory: UseMemberHistory = () => {
  const [memberSearchOptions, onMemberSearchOptionsChange] = useState<MemberSearchOptions | undefined>();

  const dispatch = useAppDispatch();
  const memberReferences = useAppSelector(state => state.memberHistory.memberReferences);
  const memberSearchKeywords = useAppSelector(state => state.memberHistory.memberSearchKeywords);

  const searchMembers: SearchMembers = useCallback(options => {
    if (!options) return undefined;

    const {keyword, limit} = options;

    if (!limit) {
      return memberReferences.filter(memberReference => isMemberMatched(memberReference, keyword));
    }

    const {length: memberReferencesLen} = memberReferences;
    const memberResults = [];

    for (let i = 0; i < memberReferencesLen; i += 1) {
      if (memberResults.length >= limit) break;

      const testMember = memberReferences[i];

      if (!isMemberMatched(testMember, keyword)) continue;

      memberResults.push(testMember);
    }

    return memberResults;
  }, [memberReferences]);

  const memberSearchResults = useMemo(
    () => searchMembers(memberSearchOptions),
    [searchMembers, memberSearchOptions],
  );

  const onMemberReferenceAdd: OnMemberReferenceAdd = memberRefence => {
    dispatch(
      addMemberReference(memberRefence),
    );
  };

  const onMemberSearchKeywordAdd: OnMemberSearchKeywordAdd = keyword => {
    dispatch(
      addMemberSearchKeyword(keyword),
    );
  };

  const onMemberSearchKeywordRemove: OnMemberSearchKeywordRemove = number => {
    dispatch(
      removeMemberSearchKeyword(number),
    );
  };

  return {
    memberReferences,
    memberSearchResults,
    memberSearchKeywords,
    onMemberReferenceAdd,
    onMemberSearchOptionsChange,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
  };
};
