import range from 'lodash.range';
import * as shareRecordFormatter from 'src/formatters/share-record';
import type {Payment, Member, Bill} from 'src/types/resources';

const {
  formatDates,
  formatMemberCount,
  formatMemberNameChain,
  formatBillMemberStatistics,
  formatCurrencyAmount,
  formatBillPaymentTotalAmount,
  formatMemberBillAmount,
  formatPaymentItemAmount,
  formatBillTotal,
  createMember,
  formatShareRecordDateString,
  createDefaultPayment,
  createBill,
  formatUniqueBillMembers,
  formatMemberBillCopyContent,
  createDefaultShareRecord,
  createDefaultBill,
} = shareRecordFormatter;

describe('function: formatDates', () => {
  it('output single date if only one date is set', () => {
    expect(formatDates(['2022-12-31', ''])).toBe('2022-12-31');
  });

  it('output dates concatencated with tilde sign if both dates are set', () => {
    expect(formatDates(['2022-12-01', '2022-12-31'])).toBe('2022-12-01 ~ 2022-12-31');
  });
});

describe('function: formatMemberCount', () => {
  it('outputs total member count of bills', () => {
    const mockBills = [
      {
        payments: [],
        host: '',
        members: [
          {name: '1'},
          {name: '2'},
        ],
      },
      {
        payments: [],
        host: '',
        members: [
          {name: '3'},
        ],
      },
    ];

    expect(formatMemberCount(mockBills)).toBe(3);
  });
});

describe('function: formatMemberNameChain', () => {
  it('chains each member name with dot', () => {
    const members = range(1, 4).map(idx => ({name: `${idx}`}));

    expect(formatMemberNameChain(members)).toBe('1, 2, 3');
  });
});

describe('function: formatBillMemberStatistics', () => {
  it('chains leading two member names and the count of other members', () => {
    const members = range(1, 5).map(idx => ({name: `${idx}`}));

    expect(formatBillMemberStatistics(members)).toBe('1, 2 與其他 2 位');
  });
});

describe('function: formatCurrencyAmount', () => {
  it('output amount with two decimal places in currency format', () => {
    const mockNumberFormatToParts = jest.spyOn(Intl.NumberFormat.prototype, 'formatToParts');

    mockNumberFormatToParts.mockReturnValueOnce([
      {type: 'currency', value: 'mock'},
      {type: 'integer', value: '1'},
    ]);

    const mockCurrency = 'TWD';
    const mockAmount = 100.123;

    const result = formatCurrencyAmount({
      currency: mockCurrency,
      amount: mockAmount,
    });

    expect(result).toBe('mock1');

    expect(mockNumberFormatToParts).toBeCalledTimes(1);
    expect(mockNumberFormatToParts).toBeCalledWith(100.12);

    mockNumberFormatToParts.mockRestore();
  });

  it('throw error if input amount is empty string', () => {
    expect(() => formatCurrencyAmount({
      currency: 'TWD',
      amount: Number.NaN,
    }))
      .toThrow(new TypeError('Invalid amount'));
  });
});

describe('function: formatBillPaymentTotalAmount', () => {
  it('sums up amount of all payment and exchange the amount if exchange rate is set', () => {
    const mockPayments: Payment[] = [
      {
        item: '1',
        useCurrencyExchange: false,
        amount: 100,
        exchangeRate: undefined,
        sourceCurrency: undefined,
      },
      {
        item: '2',
        useCurrencyExchange: true,
        amount: 40,
        exchangeRate: 0.25,
        sourceCurrency: 'TWD',
      },
    ];

    expect(formatBillPaymentTotalAmount(mockPayments)).toBe(110);
  });
});

describe('function: formatMemberBillAmount', () => {
  it('format bill amount for each member', () => {
    const mockPayments: Payment[] = [
      {
        item: '1',
        useCurrencyExchange: false,
        amount: 100,
        exchangeRate: undefined,
        sourceCurrency: undefined,
      },
      {
        item: '2',
        useCurrencyExchange: true,
        amount: 40,
        exchangeRate: 0.25,
        sourceCurrency: 'TWD',
      },
    ];
    const mockMembers: Member[] = [
      {
        name: '1',
      },
      {
        name: '2',
      },
    ];

    const result = formatMemberBillAmount({
      payments: mockPayments,
      members: mockMembers,
    });

    expect(result).toBe(55);
  });
});

describe('function: formatPaymentItemAmount', () => {
  it('outputs flattened payment amounts of input bills', () => {
    const mockBills: Bill[] = [
      {
        host: '1',
        payments: [
          {
            item: '1',
            useCurrencyExchange: false,
            amount: 100,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
          {
            item: '2',
            useCurrencyExchange: true,
            amount: 40,
            exchangeRate: 0.25,
            sourceCurrency: 'TWD',
          },
        ],
        members: [
          {
            name: '1',
          },
          {
            name: '2',
          },
        ],
      },
      {
        host: '2',
        payments: [
          {
            item: '3',
            useCurrencyExchange: false,
            amount: 120,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
          {
            item: '4',
            useCurrencyExchange: true,
            amount: 40,
            exchangeRate: 0.1,
            sourceCurrency: 'ABC',
          },
        ],
        members: [
          {
            name: '3',
          },
          {
            name: '4',
          },
          {
            name: '5',
          },
          {
            name: '6',
          },
        ],
      },
    ];

    const result = formatPaymentItemAmount(mockBills);

    expect(result).toEqual([
      {
        item: '1',
        amount: 50,
      },
      {
        item: '2',
        amount: 5,
      },
      {
        item: '3',
        amount: 30,
      },
      {
        item: '4',
        amount: 1,
      },
    ]);
  });
});

describe('function: formatBillTotal', () => {
  it('sums up all payment amounts of all bills without currency exchange', () => {
    const mockCurrency = 'TWD';
    const mockBills: Bill[] = [
      {
        host: '1',
        payments: [
          {
            item: '1',
            useCurrencyExchange: false,
            amount: 100,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
          {
            item: '2',
            useCurrencyExchange: false,
            amount: 200,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
        ],
        members: [],
      },
      {
        host: '2',
        payments: [
          {
            item: '3',
            useCurrencyExchange: false,
            amount: 300,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
          {
            item: '4',
            useCurrencyExchange: false,
            amount: 400,
            exchangeRate: undefined,
            sourceCurrency: undefined,
          },
        ],
        members: [],
      },
    ];

    const mockCurrencyAmount = 'mock 1';
    const mockFormatCurrencyAmount = jest.spyOn(shareRecordFormatter, 'formatCurrencyAmount');

    mockFormatCurrencyAmount.mockReturnValueOnce(mockCurrencyAmount);

    const result = formatBillTotal({
      currency: mockCurrency,
      bills: mockBills,
    });

    expect(result).toBe(mockCurrencyAmount);

    expect(mockFormatCurrencyAmount).toHaveBeenCalledTimes(1);
    expect(mockFormatCurrencyAmount).toHaveBeenCalledWith({
      currency: mockCurrency,
      amount: 1000,
    });

    mockFormatCurrencyAmount.mockRestore();
  });
});

describe('function: createMember', () => {
  it('creates member object', () => {
    expect(createMember('1')).toEqual({name: '1'});
  });
});

describe('function: formatShareRecordDateString', () => {
  it('outputs date string with default date format', () => {
    const mockDate = new Date();

    mockDate.setFullYear(2022);
    mockDate.setMonth(11);
    mockDate.setDate(1);

    expect(formatShareRecordDateString(mockDate)).toBe('2022-12-01');
  });
});

describe('function: createDefaultPayment', () => {
  it('creates default payment', () => {
    expect(createDefaultPayment()).toEqual({
      useCurrencyExchange: false,
      item: '',
      amount: '',
      sourceCurrency: undefined,
      exchangeRate: undefined,
    });
  });
});

describe('function: createBill', () => {
  it('creates bill based on input args', () => {
    const mockPayments: Payment[] = [];
    const mockHost = 'host';
    const mockMembers: Member[] = [];

    const result = createBill({
      payments: mockPayments,
      host: mockHost,
      members: mockMembers,
    });

    expect(result.payments).toBe(mockPayments);
    expect(result.host).toBe(mockHost);
    expect(result.members).toBe(mockMembers);
  });
});

describe('function: formatUniqueBillMembers', () => {
  it('outputs unique bill members', () => {
    const mockBills = [
      {
        payments: [],
        host: 'host',
        members: range(1, 2).map(idx => ({name: `${idx}`})),
      },
      {
        payments: [],
        host: 'host',
        members: range(2, 3).map(idx => ({name: `${idx}`})),
      },
      {
        payments: [],
        host: 'host',
        members: range(3, 1).map(idx => ({name: `${idx}`})),
      },
    ];

    expect(formatUniqueBillMembers(mockBills)).toEqual([
      {
        name: '1',
      },
      {
        name: '2',
      },
      {
        name: '3',
      },
    ]);
  });
});

describe('function: formatMemberBillCopyContent', () => {
  it('output copy content of bill payments for select members', () => {
    const mockPayments: Payment[] = [
      {
        useCurrencyExchange: false,
        item: '',
        amount: 100,
        sourceCurrency: undefined,
        exchangeRate: undefined,
      },
    ];
    const mockHost = 'mockHost';
    const mockMemberName1 = 'mockMember1';
    const mockMemberName2 = 'mockMember2';
    const mockBills = [
      {
        payments: mockPayments,
        host: mockHost,
        members: [
          {
            name: mockMemberName1,
          },
          {
            name: mockMemberName2,
          },
        ],
      },
    ];
    const mockCurrency = 'TWD';
    const mockMemberCheckMap = {
      [mockMemberName1]: true,
      [mockMemberName2]: true,
    };

    const mockCurrencyAmount = 'mock 1';
    const mockFormatCurrencyAmount = jest.spyOn(shareRecordFormatter, 'formatCurrencyAmount');

    mockFormatCurrencyAmount.mockReturnValueOnce(mockCurrencyAmount)
      .mockReturnValueOnce(mockCurrencyAmount);

    const result = formatMemberBillCopyContent({
      bills: mockBills,
      currency: mockCurrency,
      memberCheckMap: mockMemberCheckMap,
    });

    expect(result).toBe(`👤️ ${mockMemberName1}:
--------------------
 💸️ 須支付 ${mockHost}: ${mockCurrencyAmount}

👤️ ${mockMemberName2}:
--------------------
 💸️ 須支付 ${mockHost}: ${mockCurrencyAmount}
`);

    mockFormatCurrencyAmount.mockRestore();
  });
});

describe('function: createDefaultShareRecord', () => {
  it('creates default share record object', () => {
    expect(createDefaultShareRecord()).toEqual({
      name: '',
      dates: ['', ''],
      mainHost: '',
      currency: '',
      bills: [],
    });
  });
});

describe('function: createDefaultBill', () => {
  it('creates default bill', () => {
    expect(createDefaultBill()).toEqual({
      payments: [],
      host: '',
      members: [],
    });
  });
});
