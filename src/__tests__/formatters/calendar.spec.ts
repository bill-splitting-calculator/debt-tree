import dayjs from 'dayjs';
import MockDate from 'mockdate';
import {
  formatYearMonth,
  formatWeekdays,
  formatDate,
  formatDates,
  formatDateValue,
} from 'src/formatters/calendar';

describe('function: formatYearMonth', () => {
  it('accepts year and month value and output in YYYY / MMM format and zh-tw locale', () => {
    expect(formatYearMonth({year: 2022, month: 12})).toBe('2022 / 12月');
  });
});

describe('function: formatWeekdays', () => {
  it('output weekdays in zh-tw locale', () => {
    expect(formatWeekdays()).toEqual([
      '週日',
      '週一',
      '週二',
      '週三',
      '週四',
      '週五',
      '週六',
    ]);
  });
});

describe('function: formatDate', () => {
  it('returns dayjs object of today if input value is empty value', () => {
    expect(dayjs().isSame(formatDate(''))).toBe(true);
  });

  it('returns dayjs object with string-type date value', () => {
    const dayInstance = formatDate('2022-12-31');

    expect(dayInstance.year()).toBe(2022);
    expect(dayInstance.month()).toBe(11);
    expect(dayInstance.date()).toBe(31);
  });

  it('returns dayjs object with object-type date value', () => {
    const dayInstance = formatDate({
      year: 2022,
      month: 12,
      date: 31,
    });

    expect(dayInstance.year()).toBe(2022);
    expect(dayInstance.month()).toBe(11);
    expect(dayInstance.date()).toBe(31);
  });
});

describe('function: formatDates', () => {
  afterEach(() => {
    MockDate.reset();
  });

  it('formats calendar dates of input year and month', () => {
    const mockToday = '2022-12-31';
    const mockYear = 2022;
    const mockMonth = 12;

    MockDate.set(mockToday);

    const dates = formatDates({year: mockYear, month: mockMonth});

    expect(dates).toHaveLength(42);

    dates.slice(0, 3).forEach((item, idx) => {
      const {
        text,
        date,
        isCurrentMonth,
        isToday,
        isDisabled,
      } = item;

      expect(text).toBe(`${27 + idx}`);
      expect(dayjs.isDayjs(date)).toBe(true);
      expect(isCurrentMonth).toBe(false);
      expect(isToday).toBe(false);
      expect(isDisabled({min: '2022-12-31'})).toBe(true);
      expect(isDisabled({max: '2022-10-31'})).toBe(true);
      expect(isDisabled({min: '2022-10-31', max: '2022-12-31'})).toBe(false);
      expect(isDisabled({min: '2022-09-31', max: '2022-10-30'})).toBe(true);
      expect(isDisabled({min: '2023-01-31', max: '2023-02-28'})).toBe(true);
    });

    dates.slice(4, 35).forEach((item, idx) => {
      const {
        text,
        date,
        isCurrentMonth,
        isToday,
        isDisabled,
      } = item;

      expect(text).toBe(`${1 + idx}`.padStart(2, '0'));
      expect(dayjs.isDayjs(date)).toBe(true);
      expect(isCurrentMonth).toBe(true);
      expect(isToday).toBe(idx === 30);
      expect(isDisabled({min: '2023-01-31'})).toBe(true);
      expect(isDisabled({max: '2022-10-31'})).toBe(true);
      expect(isDisabled({min: '2022-10-31', max: '2022-12-31'})).toBe(false);
      expect(isDisabled({min: '2022-09-31', max: '2022-10-30'})).toBe(true);
      expect(isDisabled({min: '2023-01-31', max: '2023-02-28'})).toBe(true);
    });

    dates.slice(35, 42).forEach((item, idx) => {
      const {
        text,
        date,
        isCurrentMonth,
        isToday,
        isDisabled,
      } = item;

      expect(text).toBe(`${1 + idx}`.padStart(2, '0'));
      expect(dayjs.isDayjs(date)).toBe(true);
      expect(isCurrentMonth).toBe(false);
      expect(isToday).toBe(false);
      expect(isDisabled({min: '2023-01-31'})).toBe(true);
      expect(isDisabled({max: '2022-10-31'})).toBe(true);
      expect(isDisabled({min: '2022-10-31', max: '2023-02-28'})).toBe(false);
      expect(isDisabled({min: '2022-09-31', max: '2022-10-30'})).toBe(true);
      expect(isDisabled({min: '2023-01-31', max: '2023-02-28'})).toBe(true);
    });
  });
});

describe('function: formatDateValue', () => {
  it('outputs date string with default format', () => {
    const mockYear = 2022;
    const mockMonth = 12;
    const mockDate = 31;

    const value = formatDateValue({
      year: mockYear,
      month: mockMonth,
      date: mockDate,
    });

    expect(value).toBe('2022-12-31');
  });
});
