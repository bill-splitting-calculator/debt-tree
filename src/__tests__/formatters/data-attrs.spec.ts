import {formatDataAttrsProps} from 'src/formatters/data-attrs';

describe('function: formatDataAttrsProps', () => {
  it('converts input data attr map into props of data-* attributes', () => {
    expect(formatDataAttrsProps()).toEqual({});
    expect(formatDataAttrsProps({mock: 'test'})).toEqual({'data-mock': 'test'});
  });
});
