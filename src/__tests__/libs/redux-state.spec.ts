import {localStorageKey, saveToLocalStorage, loadFromLocalStorage} from 'src/libs/redux-state';

describe('function: saveToLocalStorage', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('saves serialized state to the local storage', () => {
    const mockSetItem = jest.spyOn(Storage.prototype, 'setItem');
    const mockStringify = jest.spyOn(JSON, 'stringify');
    const mockState = {
      shareRecord: {
        shareRecords: [],
      },
      memberHistory: {
        memberReferences: [],
        memberSearchKeywords: [],
      },
    };
    const mockSerializedState = 'mock-true';

    mockSetItem.mockReturnValueOnce(undefined);
    mockStringify.mockReturnValueOnce(mockSerializedState);

    expect(saveToLocalStorage(mockState)).toBeUndefined();

    expect(mockSetItem).toHaveBeenCalledTimes(1);
    expect(mockSetItem).toHaveBeenCalledWith(localStorageKey, mockSerializedState);

    expect(mockStringify).toHaveBeenCalledTimes(1);
    expect(mockStringify).toHaveBeenCalledWith(mockState);
  });

  it('load and restore default data from the local storage', () => {
    const mockGetItem = jest.spyOn(Storage.prototype, 'getItem');

    mockGetItem.mockReturnValueOnce(null);

    expect(loadFromLocalStorage()).toEqual({
      memberHistory: {
        memberReferences: [],
        memberSearchKeywords: [],
      },
      shareRecord: {
        shareRecords: [],
      },
    });

    expect(mockGetItem).toBeCalledTimes(1);
    expect(mockGetItem).toBeCalledWith(localStorageKey);
  });

  it('load data from the local storage and fill the empty fields', () => {
    const mockGetItem = jest.spyOn(Storage.prototype, 'getItem');

    mockGetItem.mockReturnValueOnce(
      JSON.stringify({
        memberHistory: {
          memberReferences: [],
        },
      }),
    );

    expect(loadFromLocalStorage()).toEqual({
      memberHistory: {
        memberReferences: [],
        memberSearchKeywords: [],
      },
      shareRecord: {
        shareRecords: [],
      },
    });

    expect(mockGetItem).toBeCalledTimes(1);
    expect(mockGetItem).toBeCalledWith(localStorageKey);
  });
});

