import {getVerticalMenuPosition} from 'src/libs/menu-position';

describe('function: getVerticalMenuPosition', () => {
  const {DOMRect: originalDOMRect} = globalThis;
  const {innerHeight: originalInnerHeight} = window;

  beforeAll(() => {
    globalThis.DOMRect = class DOMRect {
      public x = 0;
      public y = 0;
      public width = 0;
      public height = 0;
      public top = 0;
      public right = 0;
      public left = 0;
      public bottom = 0;

      constructor (x = 0, y = 0, width = 0, height = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.top = y;
        this.bottom = y + height;
        this.left = x;
        this.right = x + width;
      }

      static fromRect(rect?: DOMRectInit): DOMRect {
        return new DOMRect(
          rect?.x,
          rect?.y,
          rect?.width,
          rect?.height,
        );
      }

      toJSON() {
        return JSON.stringify(this);
      }
    };
  });

  afterAll(() => {
    globalThis.DOMRect = originalDOMRect;
  });

  let mockGetBoundingClientRect: jest.SpyInstance;

  beforeEach(() => {
    mockGetBoundingClientRect = jest.spyOn(Element.prototype, 'getBoundingClientRect');
  });

  afterEach(() => {
    mockGetBoundingClientRect.mockRestore();
    window.innerHeight = originalInnerHeight;
  });

  it('should calculate the position of vertical menu: menu is inside the viewport', () => {
    const mockTargetEl = document.createElement('div');
    const mockMenuEl = document.createElement('div');

    const mockTargetTop = 10;
    const mockTargetLeft = 20;
    const mockTargetHeight = 30;
    const mockMenuHeight = 100;
    const mockInnerHeight = 1000;
    const mockOffset = 200;

    mockGetBoundingClientRect.mockReturnValueOnce(
      new DOMRect(mockTargetLeft, mockTargetTop, 0, mockTargetHeight),
    )
      .mockReturnValueOnce(
        new DOMRect(0, 0, 0, mockMenuHeight),
      );

    window.innerHeight = mockInnerHeight;


    const position = getVerticalMenuPosition({
      target: mockTargetEl,
      menu: mockMenuEl,
      offset: mockOffset,
    });

    expect(position).toEqual({left: 20, top: 240});

    expect(mockGetBoundingClientRect).toHaveBeenCalledTimes(2);
    expect(mockGetBoundingClientRect).toHaveBeenCalledWith();
  });

  it('should calculate the position of vertical menu: menu is outside the viewport', () => {
    const mockTargetEl = document.createElement('div');
    const mockMenuEl = document.createElement('div');

    const mockTargetTop = 10;
    const mockTargetLeft = 20;
    const mockTargetHeight = 30;
    const mockMenuHeight = 100;
    const mockInnerHeight = 10;
    const mockOffset = 20;

    mockGetBoundingClientRect.mockReturnValueOnce(
      new DOMRect(mockTargetLeft, mockTargetTop, 0, mockTargetHeight),
    )
      .mockReturnValueOnce(
        new DOMRect(0, 0, 0, mockMenuHeight),
      );

    window.innerHeight = mockInnerHeight;

    const position = getVerticalMenuPosition({
      target: mockTargetEl,
      menu: mockMenuEl,
      offset: mockOffset,
    });

    expect(position).toEqual({left: 20, top: -110});

    expect(mockGetBoundingClientRect).toHaveBeenCalledTimes(2);
    expect(mockGetBoundingClientRect).toHaveBeenCalledWith();
  });
});
