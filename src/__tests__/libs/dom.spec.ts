import {getOrPrepareElementById} from 'src/libs/dom';

describe('function: getOrPrepareElementById', () => {
  it('should get element which already exists', () => {
    const mockId = 'mock-id';
    const mockRootEl = document.createElement('div');
    const mockTargetTag = 'span';
    const mockTargetEl = document.createElement(mockTargetTag);

    mockTargetEl.id = mockId;

    mockRootEl.appendChild(mockTargetEl);

    const elFound = getOrPrepareElementById(mockId, mockRootEl, mockTargetTag);

    expect(elFound).toStrictEqual(mockTargetEl);
  });

  it('should create element by input tag when element query is empty', () => {
    const mockId = 'mock-id';
    const mockRootEl = document.createElement('div');
    const mockTargetTag = 'span';

    const elFound = getOrPrepareElementById(mockId, mockRootEl, mockTargetTag);

    expect(elFound).toBeInstanceOf(HTMLSpanElement);
    expect(elFound).toHaveProperty('id', mockId);
    expect(mockRootEl.firstElementChild).toBe(elFound);
  });
});
