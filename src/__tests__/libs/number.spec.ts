import {parseFloat} from 'src/libs/number';

describe('funciton: parseFloat', () => {
  it('should return value if input string is float number', () => {
    expect(parseFloat('1.1')).toBe(1.1);
  });

  it('should return undefined if input string is not float number', () => {
    expect(parseFloat('a')).toBeUndefined();
  });
});
