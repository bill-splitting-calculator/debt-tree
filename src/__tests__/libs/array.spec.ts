import {toKeyList} from 'src/libs/array';

describe('function: toKeyList', () => {
  it('collects items in array which share the same property', () => {
    const mockList = [
      {
        same: 'one',
        different: 1,
      },
      {
        same: 'one',
        different: 2,
      },
      {
        same: 'two',
        different: 3,
      },
    ];

    expect(toKeyList(mockList, 'same')).toEqual({
      one: [
        {
          same: 'one',
          different: 1,
        },
        {
          same: 'one',
          different: 2,
        },
      ],
      two: [
        {
          same: 'two',
          different: 3,
        },
      ],
    });
  });
});
