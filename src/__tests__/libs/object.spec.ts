import {walk} from 'src/libs/object';
import lodashGet from 'lodash.get';

describe('function: walk', () => {
  it('returns data directly if input is falsy or not of object', () => {
    expect(walk(null, '')).toBeNull();
    expect(walk('string', '')).toBe('string');
    expect(walk(1, '')).toBe(1);
  });

  it('iterate through each key of input object', () => {
    const mockObj = {
      a: 1,
      b: [
        'string',
        {
          e: undefined,
        },
      ],
      c: {
        d: [
          1,
        ],
      },
    };

    const mockObjKeys: string[] = [];
    const result = walk(
      mockObj,
      '',
      ({path, data}) => {
        mockObjKeys.push(path);

        expect(lodashGet(mockObj, path)).toBe(data);

        return data;
      },
    );

    expect(result).toEqual(mockObj);
    expect(mockObjKeys).toEqual(['a', 'b', 'b.0', 'b.1', 'b.1.e', 'c', 'c.d', 'c.d.0']);
  });
});
