import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button, SearchInputMenuModal} from 'src/components/base';
import {createSearchKeywords, createSearchTextList} from 'src/stories/mock/share-record';

export default {
  title: 'components/base/SearchInputMenuModal',
  component: SearchInputMenuModal,
  argTypes: {
    name: {
      control: 'text',
    },
  },
  args: {
    name: 'name',
    textList: undefined,
    searchKeywords: undefined,
  },
} as ComponentMeta<typeof SearchInputMenuModal>;

const Template: ComponentStory<typeof SearchInputMenuModal> = args => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  return (
    <>
      <SearchInputMenuModal
        {...args}
        open={isModalOpen}
        onOpenChange={setIsModalOpen} />
      <Button onClick={() => setIsModalOpen(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});

export const MenuPopupWithResults = Template.bind({});

MenuPopupWithResults.args = {
  textList: createSearchTextList(),
};

export const MenuPopupWithEmptyList = Template.bind({});

MenuPopupWithEmptyList.args = {
  textList: [],
};

export const WithMenuPopupSearchKeywords = Template.bind({});

WithMenuPopupSearchKeywords.args = {
  searchKeywords: createSearchKeywords(),
};

export const WithMenuPopupEmptySearchKeywords = Template.bind({});

WithMenuPopupEmptySearchKeywords.args = {
  searchKeywords: [],
};
