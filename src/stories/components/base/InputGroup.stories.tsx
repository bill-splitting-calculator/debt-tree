import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {Input, InputGroup} from 'src/components/base';

export default {
  title: 'components/base/InputGroup',
  component: InputGroup,
  argTypes: {
    className: {
      control: 'text',
    },
  },
  args: {
    className: 'mock-class-name',
  },
} as ComponentMeta<typeof InputGroup>;

const Template: ComponentStory<typeof InputGroup> = args => (
  <InputGroup
    {...args}
    groupItem={'Group Item'}>
    <Input
      fieldName="mock"
      hasError={false} />
  </InputGroup>
);

export const Default = Template.bind({});
