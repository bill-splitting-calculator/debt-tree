import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {TextItem} from 'src/components/base';

export default {
  title: 'components/base/TextItem',
  component: TextItem,
} as ComponentMeta<typeof TextItem>;

const Template: ComponentStory<typeof TextItem> = args => (
  <TextItem {...args}>Text</TextItem>
);

export const Default = Template.bind({});
