import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button, FullScaledModalContainer} from 'src/components/base';

export default {
  title: 'components/base/FullScaledModalContainer',
  component: FullScaledModalContainer,
  argTypes: {
    name: {
      control: 'text',
    },
    header: {
      control: 'text',
    },
    doneText: {
      control: 'text',
    },
    doneDisabled: {
      control: 'boolean',
    },
  },
  args: {
    header: 'Header',
    name: 'modal-container',
    doneText: undefined,
    doneDisabled: undefined,
  },
} as ComponentMeta<typeof FullScaledModalContainer>;

const Template: ComponentStory<typeof FullScaledModalContainer> = args => {
  const [isOpen, onIsOpenChange] = useState<boolean>(false);

  return (
    <>
      <FullScaledModalContainer
        {...args}
        open={isOpen}
        onOpenChange={onIsOpenChange}>
        <div style={{width: 400, height: 400}}>
          Content
        </div>
      </FullScaledModalContainer>
      <Button onClick={() => onIsOpenChange(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});
