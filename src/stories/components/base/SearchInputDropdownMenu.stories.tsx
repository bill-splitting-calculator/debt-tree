import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {SearchInputDropdownMenu} from 'src/components/base';
import {createSearchKeywords, createSearchTextList} from 'src/stories/mock/share-record';

export default {
  title: 'components/base/SearchInputDropdownMenu',
  component: SearchInputDropdownMenu,
  argTypes: {
    open: {
      control: 'boolean',
    },
  },
  args: {
    open: true,
    searchKeywords: undefined,
  },
} as ComponentMeta<typeof SearchInputDropdownMenu>;

const Template: ComponentStory<typeof SearchInputDropdownMenu> = args => (
  <SearchInputDropdownMenu {...args} />
);

export const Default = Template.bind({});

export const MenuPopupWithResults = Template.bind({});

MenuPopupWithResults.args = {
  textList: createSearchTextList(),
};

export const MenuPopupWithEmptyList = Template.bind({});

MenuPopupWithEmptyList.args = {
  textList: [],
};

export const MenuPopupWithSearchKeywords = Template.bind({});

MenuPopupWithSearchKeywords.args = {
  open: false,
  searchKeywords: createSearchKeywords(),
};
