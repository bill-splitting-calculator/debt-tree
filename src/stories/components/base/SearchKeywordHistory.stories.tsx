import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {SearchKeywordHistory} from 'src/components/base';
import {createSearchKeywords} from 'src/stories/mock/share-record';

export default {
  title: 'components/base/SearchKeywordHistory',
  component: SearchKeywordHistory,
  args: {
    searchKeywords: [],
  },
} as ComponentMeta<typeof SearchKeywordHistory>;

const Template: ComponentStory<typeof SearchKeywordHistory> = args => (
  <SearchKeywordHistory {...args} />
);

export const Default = Template.bind({});

export const HasSearchKeyword = Template.bind({});

HasSearchKeyword.args = {
  searchKeywords: createSearchKeywords(),
};
