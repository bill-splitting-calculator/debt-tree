import {faker} from '@faker-js/faker';
import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {TextItemList} from 'src/components/base';
import {createSearchTextList} from 'src/stories/mock/share-record';

export default {
  title: 'components/base/TextItemList',
  component: TextItemList,
  argsTypes: {
    keyword: {
      control: 'text',
    },
  },
  args: {
    keyword: faker.word.noun(),
    textList: undefined,
  },
} as ComponentMeta<typeof TextItemList>;

const Template: ComponentStory<typeof TextItemList> = args => (
  <TextItemList {...args} />
);

export const Default = Template.bind({});

export const WithTextList = Template.bind({});

WithTextList.args = {
  textList: createSearchTextList(),
};

export const KeywordCreator = Template.bind({});

KeywordCreator.args = {
  textList: [],
};
