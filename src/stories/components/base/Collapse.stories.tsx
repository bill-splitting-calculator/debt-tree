import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {Collapse} from 'src/components/base';

export default {
  title: 'components/base/Collapse',
  component: Collapse,
  argTypes: {
    defaultCollapsed: {
      control: 'boolean',
    },
  },
  args: {
    defaultCollapsed: undefined,
  },
} as ComponentMeta<typeof Collapse>;

const Template: ComponentStory<typeof Collapse> = args => (
  <Collapse
    {...args}
    headerChildren={<div>Header</div>}>
    <div style={{height: 400}}>Children</div>
  </Collapse>
);

export const Default = Template.bind({});

export const WithDefaultCollapsedOn = Template.bind({});

WithDefaultCollapsedOn.args = {
  defaultCollapsed: true,
};

export const WithDefaultCollapsedOff = Template.bind({});

WithDefaultCollapsedOff.args = {
  defaultCollapsed: false,
};
