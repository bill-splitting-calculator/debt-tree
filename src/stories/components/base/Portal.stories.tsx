import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Portal} from 'src/components/base';

export default {
  title: 'components/base/Portal',
  component: Portal,
  argTypes: {
    portalId: {
      control: 'text',
    },
    portalClassName: {
      control: 'text',
    },
  },
  args: {
    portalId: undefined,
    portalClassName: undefined,
  },
} as ComponentMeta<typeof Portal>;

const Template: ComponentStory<typeof Portal> = args => (<Portal {...args}>Children</Portal>);

export const Default = Template.bind({});
