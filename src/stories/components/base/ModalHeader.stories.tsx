import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {ModalHeader} from 'src/components/base';

export default {
  title: 'components/base/ModalHeader',
  component: ModalHeader,
  argTypes: {
    header: {
      control: 'text',
    },
    doneDisabled: {
      control: 'boolean',
    },
    doneText: {
      control: 'text',
    },
  },
  args: {
    header: 'Header',
    doneText: undefined,
    doneDisabled: undefined,
  },
} as ComponentMeta<typeof ModalHeader>;

const Template: ComponentStory<typeof ModalHeader> = args => (
  <ModalHeader {...args} />
);

export const Default = Template.bind({});

export const CustomDoneText = Template.bind({});

CustomDoneText.args = {
  doneText: 'Done',
};

export const DoneTextDisabled = Template.bind({});

DoneTextDisabled.args = {
  doneDisabled: true,
};
