import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {CopyIndicator} from 'src/components/base';

export default {
  title: 'components/base/CopyIndicator',
  component: CopyIndicator,
  argTypes: {
    timeout: {
      control: 'number',
    },
  },
  args: {
    timeout: undefined,
  },
} as ComponentMeta<typeof CopyIndicator>;

const Template: ComponentStory<typeof CopyIndicator> = args => (
  <CopyIndicator {...args}>
    Children
  </CopyIndicator>
);

export const Default = Template.bind({});

export const CustomTimeout = Template.bind({});

CustomTimeout.args = {
  timeout: 1000,
};
