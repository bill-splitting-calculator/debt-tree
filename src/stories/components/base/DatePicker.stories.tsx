import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {DatePicker} from 'src/components/base';

export default {
  title: 'components/base/DatePicker',
  component: DatePicker,
  argTypes: {
    name: {
      control: 'text',
    },
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
    value: {
      control: 'text',
    },
    min: {
      control: 'text',
    },
    max: {
      control: 'text',
    },
  },
  args: {
    name: 'name',
    fieldName: 'Field Name',
    hasError: false,
  },
} as ComponentMeta<typeof DatePicker>;

const Template: ComponentStory<typeof DatePicker> = args => (
  <DatePicker {...args} />
);

export const Default = Template.bind({});

export const WithMin = Template.bind({});

WithMin.args = {
  min: '2022-12-10',
};

export const WithMax = Template.bind({});

WithMax.args = {
  max: '2022-12-20',
};
