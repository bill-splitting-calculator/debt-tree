import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {SearchInput} from 'src/components/base';
import {createSearchKeywords, createSearchTextList} from 'src/stories/mock/share-record';

export default {
  title: 'components/base/SearchInput',
  component: SearchInput,
  argTypes: {
    name: {
      control: 'text',
    },
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
    resetable: {
      control: 'boolean',
    },
  },
  args: {
    name: 'Field',
    fieldName: 'Field',
    hasError: false,
    resetable: false,
    searchKeywords: [],
    textList: undefined,
  },
} as ComponentMeta<typeof SearchInput>;

const Template: ComponentStory<typeof SearchInput> = args => (
  <SearchInput {...args} />
);

export const Default = Template.bind({});

export const MenuPopupWithResults = Template.bind({});

MenuPopupWithResults.args = {
  textList: createSearchTextList(),
};

export const MenuPopupWithEmptyList = Template.bind({});

MenuPopupWithEmptyList.args = {
  textList: [],
};

export const WithMenuPopupSearchKeywords = Template.bind({});

WithMenuPopupSearchKeywords.args = {
  searchKeywords: createSearchKeywords(),
};

export const WithMenuPopupEmptySearchKeywords = Template.bind({});

WithMenuPopupEmptySearchKeywords.args = {
  searchKeywords: [],
};
