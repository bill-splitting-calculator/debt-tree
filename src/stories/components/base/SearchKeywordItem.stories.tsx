import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {SearchKeywordItem} from 'src/components/base';

export default {
  title: 'components/base/SearchKeywordItem',
  component: SearchKeywordItem,
} as ComponentMeta<typeof SearchKeywordItem>;

const Template: ComponentStory<typeof SearchKeywordItem> = args => (
  <SearchKeywordItem {...args}>Text</SearchKeywordItem>
);

export const Default = Template.bind({});
