import type {ComponentMeta, ComponentStory} from '@storybook/react';
import dayjs from 'dayjs';
import {Calendar} from 'src/components/base';

export default {
  title: 'components/base/Calendar',
  component: Calendar,
  argTypes: {
    year: {
      control: 'number',
    },
    month: {
      control: 'number',
    },
    value: {
      control: 'text',
    },
    min: {
      control: 'text',
    },
    max: {
      control: 'text',
    },
    highlightToday: {
      control: 'boolean',
    },
  },
  args: {
    year: 2022,
    month: 12,
    value: '2022-12-15',
    min: undefined,
    max: undefined,
    highlightToday: undefined,
  },
} as ComponentMeta<typeof Calendar>;

const Template: ComponentStory<typeof Calendar> = args => (
  <Calendar {...args} />
);

export const Default = Template.bind({});

export const WithMinMonth = Template.bind({});

WithMinMonth.args = {
  min: '2022-12-01',
};

export const WithMaxMonth = Template.bind({});

WithMaxMonth.args = {
  max: '2022-12-31',
};

export const WithTodayHighlight = Template.bind({});

WithTodayHighlight.args = (() => {
  const today = dayjs();

  return {
    year: today.year(),
    month: today.month() + 1,
    highlightToday: true,
  };
})();
