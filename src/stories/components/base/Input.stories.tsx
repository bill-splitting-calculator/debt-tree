import {ComponentMeta, ComponentStory} from '@storybook/react';
import {Input} from 'src/components/base';

export default {
  title: 'components/base/Input',
  component: Input,
  argTypes: {
    type: {
      control: 'text',
      options: ['text', 'number'],
    },
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
    lazy: {
      control: 'boolean',
    },
    lazyInterval: {
      control: 'number',
    },
    resetable: {
      control: 'boolean',
    },
    containerClassName: {
      control: 'string',
    },
  },
  args: {
    type: undefined,
    fieldName: 'Field Name',
    hasError: undefined,
    lazy: undefined,
    lazyInterval: undefined,
    resetable: undefined,
    containerClassName: undefined,
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = args => (<Input {...args} />);

export const Default = Template.bind({});

Default.args = {
  fieldName: 'Field',
};

export const HasError = Template.bind({});

HasError.args = {
  fieldName: 'Field',
  hasError: true,
};

export const Resetable = Template.bind({});

Resetable.args = {
  resetable: true,
};
