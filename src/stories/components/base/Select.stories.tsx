import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {Select} from 'src/components/base';

export default {
  title: 'components/base/Select',
  component: Select,
  argTypes: {
    placeholder: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
  },
  args: {
    placeholder: 'Select',
  },
} as ComponentMeta<typeof Select>;

const Template: ComponentStory<typeof Select> = args => (
  <Select {...args}>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
  </Select>
);

export const Default = Template.bind({});

export const ControlledValue = Template.bind({});

ControlledValue.args = {
  value: '1',
};

export const HasError = Template.bind({});

HasError.args = {
  hasError: true,
};
