import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {LinkText} from 'src/components/base';

export default {
  title: 'components/base/LinkText',
  component: LinkText,
  argTypes: {
    disabled: {
      control: 'boolean',
    },
    variant: {
      control: 'radio',
      options: ['default', 'danger'],
    },
  },
  args: {
    variant: undefined,
    disabled: undefined,
  },
} as ComponentMeta<typeof LinkText>;

const Template: ComponentStory<typeof LinkText> = args => (
  <LinkText {...args}>Text</LinkText>
);

export const Default = Template.bind({});

export const Danger = Template.bind({});

Danger.args = {
  variant: 'danger',
};

export const Disabled = Template.bind({});

Disabled.args = {
  disabled: true,
};
