import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {BackdropModalContainer, Button} from 'src/components/base';

export default {
  title: 'components/base/BackdropModalContainer',
  component: BackdropModalContainer,
  argTypes: {
    header: {
      control: 'text',
    },
    name: {
      control: 'text',
    },
    modalContentClassName: {
      control: 'text',
    },
    doneText: {
      control: 'text',
    },
    doneDisabled: {
      control: 'boolean',
    },
  },
  args: {
    header: 'Header',
    name: 'modal-container',
    modalContentClassName: undefined,
    doneText: undefined,
    doneDisabled: undefined,
  },
} as ComponentMeta<typeof BackdropModalContainer>;

const Template: ComponentStory<typeof BackdropModalContainer> = args => {
  const [isOpen, onIsOpenChange] = useState<boolean>(false);

  return (
    <>
      <BackdropModalContainer
        {...args}
        open={isOpen}
        onOpenChange={onIsOpenChange}>
        <div style={{width: 400, height: 400}}>
          Content
        </div>
      </BackdropModalContainer>
      <Button onClick={() => onIsOpenChange(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});
