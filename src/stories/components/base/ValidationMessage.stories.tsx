import {ComponentMeta, ComponentStory} from '@storybook/react';
import {ValidationMessage} from 'src/components/base';

export default {
  title: 'components/base/ValidationMessage',
  component: ValidationMessage,
  argTypes: {
    message: {
      control: 'text',
    },
  },
  args: {
    message: undefined,
  },
} as ComponentMeta<typeof ValidationMessage>;

const Template: ComponentStory<typeof ValidationMessage> = args => (<ValidationMessage {...args} />);

export const Default = Template.bind({});

export const CustomMessage = Template.bind({});

CustomMessage.args = {
  message: 'Field should be of string',
};
