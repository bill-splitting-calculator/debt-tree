import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {Button} from 'src/components/base';

export default {
  title: 'components/base/Button',
  component: Button,
  argTypes: {
    variant: {
      control: 'radio',
      options: ['default', 'primary'],
    },
    disabled: {
      control: 'boolean',
    },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = args => (
  <Button {...args}>
    Button
  </Button>
);

export const Default = Template.bind({});

export const Primary = Template.bind({});

Primary.args = {
  variant: 'primary',
};

export const Disabled = Template.bind({});

Disabled.args = {
  disabled: true,
};
