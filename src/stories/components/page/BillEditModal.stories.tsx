import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button} from 'src/components/base';
import {BillEditModal} from 'src/components/page';
import {availableCurrencyCodes} from 'src/config/locale';
import {
  createBill,
  createCurrency,
  createSearchKeywords,
  createSearchTextList,
} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/BillEditModal',
  component: BillEditModal,
  argTypes: {
    currency: {
      control: 'select',
      options: availableCurrencyCodes,
    },
  },
  args: {
    currency: createCurrency(),
    bill: undefined,
    memberSearchList: createSearchTextList(),
    memberSearchKeywords: createSearchKeywords(),
  },
} as ComponentMeta<typeof BillEditModal>;

const Template: ComponentStory<typeof BillEditModal> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <BillEditModal
        {...args}
        open={isOpen}
        onOpenChange={setIsOpen} />
      <Button onClick={() => setIsOpen(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});

export const HasBill = Template.bind({});

HasBill.args = {
  bill: createBill(),
};
