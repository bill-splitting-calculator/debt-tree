import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberBillFieldSet} from 'src/components/page/MemberBillFieldSet';
import {createShareRecord} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberBillFieldSet',
  component: MemberBillFieldSet,
  args: (() => {
    const {bills, currency} = createShareRecord();

    return {
      bills,
      currency,
    };
  })(),
} as ComponentMeta<typeof MemberBillFieldSet>;

const Template: ComponentStory<typeof MemberBillFieldSet> = args => (
  <MemberBillFieldSet {...args} />
);

export const Default = Template.bind({});
