import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberBillList} from 'src/components/page';
import {createAmount, createCurrency, createPaymentItemAmount} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberBillList',
  component: MemberBillList,
  argTypes: {
    currency: {
      control: 'text',
    },
    totalAmount: {
      control: 'number',
    },
  },
  args: {
    currency: createCurrency(),
    paymentItemAmount: createPaymentItemAmount(),
    totalAmount: createAmount(),
  },
} as ComponentMeta<typeof MemberBillList>;

const Template: ComponentStory<typeof MemberBillList> = args => (
  <MemberBillList {...args} />
);

export const Default = Template.bind({});
