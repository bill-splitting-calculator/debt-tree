import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {BillFieldSet} from 'src/components/page/BillFieldSet';
import {createBills, createCurrency} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/BillFieldSet',
  component: BillFieldSet,
  args: {
    currency: createCurrency(),
    bills: createBills(),
  },
} as ComponentMeta<typeof BillFieldSet>;

const Template: ComponentStory<typeof BillFieldSet> = args => (
  <BillFieldSet {...args} />
);

export const Default = Template.bind({});
