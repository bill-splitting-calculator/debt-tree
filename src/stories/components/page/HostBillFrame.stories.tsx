import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {HostBillFrame, MemberBillList} from 'src/components/page';
import {
  createCurrency,
  createHost,
  createPaymentItemAmount,
} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/HostBillFrame',
  component: HostBillFrame,
  argTypes: {
    host: {
      control: 'text',
    },
  },
  args: {
    host: createHost(),
  },
} as ComponentMeta<typeof HostBillFrame>;

const Template: ComponentStory<typeof HostBillFrame> = args => (
  <HostBillFrame {...args}>
    <MemberBillList
      currency={createCurrency()}
      paymentItemAmount={createPaymentItemAmount()}
      totalAmount={100} />
  </HostBillFrame>
);

export const Default = Template.bind({});
