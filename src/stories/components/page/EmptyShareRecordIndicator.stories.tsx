import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {EmptyShareRecordIndicator} from 'src/components/page';

export default {
  title: 'components/page/EmptyDebtRecordIndication',
  component: EmptyShareRecordIndicator,
} as ComponentMeta<typeof EmptyShareRecordIndicator>;

const Template: ComponentStory<typeof EmptyShareRecordIndicator> = args => (
  <EmptyShareRecordIndicator {...args} />
);

export const Default = Template.bind({});
