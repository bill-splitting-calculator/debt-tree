import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {ShareRecordBasicEditForm} from 'src/components/page';
import {createSearchKeywords, createShareRecordBasic} from 'src/stories/mock/share-record';

const {
  name,
  mainHost,
  dates,
  currency,
} = createShareRecordBasic();

export default {
  title: 'components/page/ShareRecordBasicEditForm',
  component: ShareRecordBasicEditForm,
  args: {
    name,
    mainHost,
    dates,
    currency,
    memberSearchList: undefined,
    memberSearchKeywords: undefined,
  },
} as ComponentMeta<typeof ShareRecordBasicEditForm>;

const Template: ComponentStory<typeof ShareRecordBasicEditForm> = args => (
  <ShareRecordBasicEditForm {...args} />
);

export const Default = Template.bind({});

export const WithMemberList = Template.bind({});

WithMemberList.args = {
  memberSearchList: createSearchKeywords(),
  memberSearchKeywords: createSearchKeywords(),
};
