import type {ComponentMeta, ComponentStory} from '@storybook/react';
import range from 'lodash.range';
import {ShareRecordCard, ShareRecordCardList} from 'src/components/page';
import {createShareRecord} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/ShareRecordCardList',
  component: ShareRecordCardList,
} as ComponentMeta<typeof ShareRecordCardList>;

const Template: ComponentStory<typeof ShareRecordCardList> = args => (
  <ShareRecordCardList {...args}>
    {
      range(6).map((_item, idx) => (
        <ShareRecordCard
          key={idx}
          shareRecord={createShareRecord()}
          onEdit={() => null}
          onRemove={() => null} />
      ))
    }
  </ShareRecordCardList>
);

export const Default = Template.bind({});
