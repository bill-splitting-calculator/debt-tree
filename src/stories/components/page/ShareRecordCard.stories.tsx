import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {ShareRecordCard} from 'src/components/page';
import {createShareRecord} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/ShareRecordCard',
  component: ShareRecordCard,
  args: {
    shareRecord: createShareRecord(),
  },
} as ComponentMeta<typeof ShareRecordCard>;

const Template: ComponentStory<typeof ShareRecordCard> = args => (
  <ShareRecordCard {...args} />
);

export const Default = Template.bind({});
