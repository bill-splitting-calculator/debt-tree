import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {User} from 'grommet-icons';
import {IconHeader} from 'src/components/page';

export default {
  title: 'components/page/IconHeader',
  component: IconHeader,
  args: {
    icon: User,
  },
} as ComponentMeta<typeof IconHeader>;

const Template: ComponentStory<typeof IconHeader> = args => (
  <IconHeader {...args}>
    Title
  </IconHeader>
);

export const Default = Template.bind({});
