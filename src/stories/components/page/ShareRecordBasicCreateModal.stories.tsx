import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button} from 'src/components/base';
import {ShareRecordBasicCreateModal} from 'src/components/page';
import {createSearchKeywords} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/ShareRecordBasicCreateModal',
  component: ShareRecordBasicCreateModal,
  args: {
    memberSearchList: undefined,
    memberSearchKeywords: undefined,
  },
} as ComponentMeta<typeof ShareRecordBasicCreateModal>;

const Template: ComponentStory<typeof ShareRecordBasicCreateModal> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <ShareRecordBasicCreateModal
        {...args}
        open={isOpen}
        onOpenChange={setIsOpen} />
      <Button onClick={() => setIsOpen(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});

export const WithMemberList = Template.bind({});

WithMemberList.args = {
  memberSearchList: createSearchKeywords(),
  memberSearchKeywords: createSearchKeywords(),
};


