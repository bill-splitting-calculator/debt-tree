import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberTag} from 'src/components/page';
import {createMembers} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberTag',
  component: MemberTag,
  args: {
    member: createMembers({count: 1})[0],
  },
} as ComponentMeta<typeof MemberTag>;

const Template: ComponentStory<typeof MemberTag> = args => (
  <MemberTag {...args} />
);

export const Default = Template.bind({});
