import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {AddButton} from 'src/components/page';

export default {
  title: 'components/page/AddButton',
  component: AddButton,
} as ComponentMeta<typeof AddButton>;

const Template: ComponentStory<typeof AddButton> = args => (
  <AddButton {...args}>
    Button
  </AddButton>
);

export const Default = Template.bind({});
