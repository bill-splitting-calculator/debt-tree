import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberSearchInput} from 'src/components/page';
import {createSearchKeywords} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberSearchInput',
  component: MemberSearchInput,
  argTypes: {
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
    resetable: {
      control: 'boolean',
    },
  },
  args: {
    fieldName: 'Field Name',
    hasError: false,
    resetable: false,
    memberSearchList: undefined,
    memberSearchKeywords: undefined,
  },
} as ComponentMeta<typeof MemberSearchInput>;

const Template: ComponentStory<typeof MemberSearchInput> = args => (
  <MemberSearchInput {...args} />
);

export const Default = Template.bind({});

export const WithMemberList = Template.bind({});

WithMemberList.args = {
  memberSearchList: createSearchKeywords(),
  memberSearchKeywords: createSearchKeywords(),
};
