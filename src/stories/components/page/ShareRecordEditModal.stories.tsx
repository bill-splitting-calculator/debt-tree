import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button} from 'src/components/base';
import {ShareRecordEditModal} from 'src/components/page/ShareRecordEditModal';
import {createSearchKeywords, createShareRecord} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/ShareRecordEditModal',
  component: ShareRecordEditModal,
  args: {
    shareRecord: createShareRecord(),
    memberSearchList: undefined,
    memberSearchKeywords: undefined,
  },
} as ComponentMeta<typeof ShareRecordEditModal>;

const Template: ComponentStory<typeof ShareRecordEditModal> = args => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <ShareRecordEditModal
        {...args}
        open={isOpen}
        onOpenChange={setIsOpen} />
      <Button onClick={() => setIsOpen(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});

export const WithMemberList = Template.bind({});

WithMemberList.args = {
  memberSearchList: createSearchKeywords(),
  memberSearchKeywords: createSearchKeywords(),
};
