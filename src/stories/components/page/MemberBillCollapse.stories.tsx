import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberBillCollapse} from 'src/components/page';
import {faker} from '@faker-js/faker';
import {createShareRecord} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberBillCollapse',
  component: MemberBillCollapse,
  argTypes: {
    checked: {
      control: 'boolean',
    },
    defaultCollapsed: {
      control: 'boolean',
    },
  },
  args: (() => {
    const member = {
      name: faker.name.findName(),
    };
    const {bills, currency} = createShareRecord();

    bills.forEach(bill => {
      bill.members.push(member);
    });

    return {
      checked: false,
      defaultCollapsed: undefined,
      member,
      bills,
      currency,
    };
  })(),
} as ComponentMeta<typeof MemberBillCollapse>;

const Template: ComponentStory<typeof MemberBillCollapse> = args => (
  <MemberBillCollapse {...args} />
);

export const Default = Template.bind({});
