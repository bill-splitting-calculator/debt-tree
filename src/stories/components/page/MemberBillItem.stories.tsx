import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberBillItem} from 'src/components/page';
import {faker} from '@faker-js/faker';
import {createCurrency} from 'src/stories/mock/share-record';

const {commerce, datatype} = faker;

export default {
  title: 'components/page/MemberBillItem',
  component: MemberBillItem,
  argTypes: {
    item: {
      control: 'text',
    },
    amount: {
      control: 'number',
    },
    currency: {
      control: 'text',
    },
  },
  args: {
    item: commerce.product(),
    amount: datatype.number({min: 100}),
    currency: createCurrency(),
  },
} as ComponentMeta<typeof MemberBillItem>;

const Template: ComponentStory<typeof MemberBillItem> = args => (
  <MemberBillItem {...args} />
);

export const Default = Template.bind({});
