import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {MemberPicker} from 'src/components/page';
import {createMembers, createSearchKeywords} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/MemberPicker',
  component: MemberPicker,
  argTypes: {
    fieldName: {
      control: 'text',
    },
    hasError: {
      control: 'boolean',
    },
    resetable: {
      control: 'boolean',
    },
  },
  args: {
    fieldName: 'Field Name',
    resetable: undefined,
    members: createMembers(),
    memberSearchList: undefined,
    memberSearchKeywords: undefined,
    defaultMembers: undefined,
  },
} as ComponentMeta<typeof MemberPicker>;

const Template: ComponentStory<typeof MemberPicker> = args => (
  <MemberPicker {...args} />
);

export const Default = Template.bind({});

export const WithMemberList = Template.bind({});

WithMemberList.args = {
  memberSearchList: createMembers().map(({name}) => name),
};

export const WithEmptyMemberList = Template.bind({});

WithEmptyMemberList.args = {
  memberSearchList: [],
};

export const WithMemberSearchHistory = Template.bind({});

WithMemberSearchHistory.args = {
  memberSearchKeywords: createSearchKeywords(),
};



