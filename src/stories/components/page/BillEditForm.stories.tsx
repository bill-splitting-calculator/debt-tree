import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {BillEditForm} from 'src/components/page';
import {createBill, createCurrency, createSearchTextList, createSearchKeywords} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/BillEditForm',
  component: BillEditForm,
  args: {
    bill: createBill(),
    currency: createCurrency(),
    memberSearchList: createSearchTextList(),
    memberSearchKeywords: createSearchKeywords(),
  },
} as ComponentMeta<typeof BillEditForm>;

const Template: ComponentStory<typeof BillEditForm> = args => (
  <BillEditForm {...args} />
);

export const Default = Template.bind({});
