import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {BillCard} from 'src/components/page';
import {createBill, createCurrency} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/BillCard',
  component: BillCard,
  argTypes: {
    currency: {
      control: 'text',
    },
  },
  args: {
    currency: createCurrency(),
    bill: createBill(),
  },
} as ComponentMeta<typeof BillCard>;

const Template: ComponentStory<typeof BillCard> = args => (
  <BillCard {...args} />
);

export const Default = Template.bind({});
