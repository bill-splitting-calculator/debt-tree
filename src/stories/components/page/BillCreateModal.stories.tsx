import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {useState} from 'react';
import {Button} from 'src/components/base';
import {BillCreateModal} from 'src/components/page';
import {availableCurrencyCodes} from 'src/config/locale';
import {
  createCurrency,
  createMainHost,
  createMembers,
  createSearchKeywords,
} from 'src/stories/mock/share-record';

export default {
  title: 'components/page/BillCreateModal',
  component: BillCreateModal,
  argTypes: {
    currency: {
      control: 'select',
      options: availableCurrencyCodes,
    },
    mainHost: {
      control: 'text',
    },
  },
  args: {
    currency: createCurrency(),
    mainHost: undefined,
    memberSearchList: createMembers({count: 5}).map(({name}) => name),
    memberSearchKeywords: createSearchKeywords(),
    doneText: undefined,
  },
} as ComponentMeta<typeof BillCreateModal>;

const Template: ComponentStory<typeof BillCreateModal> = args => {
  const [isOpen, onIsOpenChange] = useState<boolean>(false);

  return (
    <>
      <BillCreateModal
        {...args}
        open={isOpen}
        onOpenChange={onIsOpenChange} />
      <Button onClick={() => onIsOpenChange(true)}>Open Modal</Button>
    </>
  );
};

export const Default = Template.bind({});

export const MainHostEnabled = Template.bind({});

MainHostEnabled.args = {
  mainHost: createMainHost(),
};
