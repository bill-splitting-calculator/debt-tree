import type {ComponentMeta, ComponentStory} from '@storybook/react';
import {AppHeader} from 'src/components/main';

export default {
  title: 'components/main/AppHeader',
  component: AppHeader,
  argTypes: {
    className: {
      control: 'text',
    },
  },
  args: {
    className: 'mock-class-name',
  },
} as ComponentMeta<typeof AppHeader>;

const Template: ComponentStory<typeof AppHeader> = args => (
  <AppHeader {...args}>分攤計算器</AppHeader>
);

export const Default = Template.bind({});
