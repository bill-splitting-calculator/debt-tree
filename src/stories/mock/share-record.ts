import {faker} from '@faker-js/faker';
import range from 'lodash.range';
import {availableCurrencyCodes} from 'src/config/locale';
import {formatShareRecordDateString} from 'src/formatters/share-record';
import type {
  Bill,
  Member,
  Payment,
  ShareRecord,
} from 'src/types/resources';

const {
  helpers,
  datatype,
  name: fakerName,
  word,
  date,
} = faker;

export type CreateMembers = (options?: {count?: number}) => Member[];

export const createMembers: CreateMembers = ({count = 5} = {}) => (
  helpers.uniqueArray<string>(fakerName.findName, count).map(name => ({name}))
);

export type CreateCurrency = () => string;

export const createCurrency: CreateCurrency = () => helpers.arrayElement(availableCurrencyCodes);

export type CreateAmount = () => number;

export const createAmount: CreateAmount = () => datatype.number({min: 1, max: 999.99});

export type CreateExchangeRate = () => number;

export const createExchangeRate: CreateExchangeRate = () => datatype.number({min: 0.1, max: 20, precision: 2});

export type CreatePayments = (options?: {count?: number}) => Payment[];

export const createPayments: CreatePayments = options => (
  helpers.uniqueArray<string>(word.noun, options?.count ?? 5).map(item => {
    const useCurrencyExchange = datatype.boolean();

    if (!useCurrencyExchange) {
      return {
        item,
        useCurrencyExchange: false,
        amount: createAmount(),
        sourceCurrency: undefined,
        exchangeRate:  undefined,
      };
    }

    return {
      item,
      useCurrencyExchange,
      amount: createAmount(),
      sourceCurrency: createCurrency(),
      exchangeRate: createExchangeRate(),
    };
  })
);

export type CreateHost = () => string;

export const createHost: CreateHost = () => fakerName.findName();

export type CreateBill = () => Bill;

export const createBill: CreateBill = () => ({
  payments: createPayments(),
  host: createHost(),
  members: createMembers({count: 3}),
});

export type CreateBills = (options?: {count?: number}) => Bill[];

export const createBills: CreateBills = ({count = datatype.number({min: 5, max: 20})} = {}) => (
  range(count).map(() => createBill())
);

export type CreateDates = () => ShareRecord['dates'];

export const createDates: CreateDates = () => [
  formatShareRecordDateString(date.past()),
  datatype.boolean() ? formatShareRecordDateString(date.future()) : '',
];

export type CreateMainHost = () => string;

export const createMainHost: CreateMainHost = () => fakerName.findName();

export type CreateShareRecordBasic = () => Pick<ShareRecord, 'name' | 'mainHost' | 'dates' | 'currency'>;

export const createShareRecordBasic: CreateShareRecordBasic = () => ({
  name: word.noun(),
  mainHost: createMainHost(),
  dates: createDates(),
  currency: createCurrency(),
});

export type CreateShareRecord = () => ShareRecord;

export const createShareRecord: CreateShareRecord = () => ({
  name: word.noun(),
  mainHost: createMainHost(),
  currency: createCurrency(),
  dates: createDates(),
  bills: createBills(),
});

export type CreateSearchTextList = (options?: {count: number}) => string[];

export const createSearchTextList: CreateSearchTextList = options => (
  helpers.uniqueArray<string>(word.noun, options?.count ?? 5)
);

export type CreatePaymentItemAmount = (options?: {count: number}) => Pick<Payment, 'item' | 'amount'>[];

export const createPaymentItemAmount: CreatePaymentItemAmount = options => (
  createPayments(options).map(({item, amount}) => ({
    item,
    amount,
  }))
);

export type CreateSearchKeywords = (options?: {count: number}) => string[];

export const createSearchKeywords: CreateSearchKeywords = options => (
  helpers.uniqueArray<string>(
    word.noun,
    options?.count ?? 5,
  )
);
