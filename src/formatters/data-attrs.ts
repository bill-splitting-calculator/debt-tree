export function formatDataAttrsProps(dataAttrs?: Record<string, string>): Record<string, string> {
  const dataAttrProps = {} as unknown as Record<string, string>;

  if (dataAttrs) {
    Object.entries(dataAttrs)
      .forEach(([key, value]) => {
        const dataKey = `data-${key}`;

        dataAttrProps[dataKey] = value;
      });
  }

  return dataAttrProps;
}
