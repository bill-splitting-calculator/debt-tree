
import type {Dayjs} from 'dayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-tw';
import localeData from 'dayjs/plugin/localeData';
import range from 'lodash.range';
import {dateFormat, yearMonthFormat} from 'src/config/date';
import * as selfModule from './calendar';

dayjs.extend(localeData);
dayjs.locale('zh-tw');

type DatePayload = {
  year: number;
  month: number;
  date: number;
};

export type FormatYearMonth = (payload: Pick<DatePayload, 'year' | 'month'>) => string;

export const formatYearMonth: FormatYearMonth = ({year, month}) => (
  selfModule.formatDate({year, month, date: 1})
    .format(yearMonthFormat)
);

export type FormatWeekdays = () => string[];

export const formatWeekdays: FormatWeekdays = () => dayjs.weekdaysShort();

export type Date = {
  text: string;
  date: Dayjs;
  isCurrentMonth: boolean;
  isToday: boolean;
  isDisabled: (payload: {min?: string; max?: string}) => boolean;
};

export type FormatDate = (payload: string | DatePayload) => Dayjs;

export const formatDate: FormatDate = payload => {
  if (!payload) return dayjs();

  if (typeof payload === 'string') return dayjs(payload, {format: dateFormat});

  const {year, month, date} = payload;

  return dayjs().year(year)
    .month(month - 1)
    .date(date);
};

export type FormatDates = (payload: Pick<DatePayload, 'year' | 'month'>) => Date[];

export const formatDates: FormatDates = ({year, month}) => {
  const monthFirstDate = selfModule.formatDate({year, month, date: 1});
  const displayFirstDate = monthFirstDate.subtract(monthFirstDate.day(), 'day');

  return range(0, 42).map(idx => {
    const date = displayFirstDate.add(idx, 'day');

    return {
      text: date.format('DD'),
      date,
      isCurrentMonth: date.month() + 1 === month,
      isToday: date.isSame(dayjs(), 'date'),
      isDisabled: ({min, max}) => !!(
        (min && date.isBefore(selfModule.formatDate(min), 'date')) ||
        (max && date.isAfter(selfModule.formatDate(max), 'date'))
      ),
    };
  });
};

export type FormatDateValue = (payload: DatePayload) => string;

export const formatDateValue: FormatDateValue = ({year, month, date}) => (
  selfModule.formatDate({year, month, date})
    .format(dateFormat)
);
