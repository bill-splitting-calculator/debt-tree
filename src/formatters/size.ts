export type Point = {x: number, y: number};

export type CreatePoint = (x: Point['x'], y: Point['y']) => Point;

export const createPoint: CreatePoint = (x, y) => ({x, y});
