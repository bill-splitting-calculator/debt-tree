import dayjs from 'dayjs';
import {Decimal} from 'decimal.js';
import {dateFormat} from 'src/config/date';
import type {
  Bill,
  Member,
  ShareRecord,
  Payment,
  PaymentWithNoCurrencyExchange,
} from 'src/types/resources';
import * as selfModule from './share-record';

export type FormatDates = (dates: ShareRecord['dates']) => string;

export const formatDates: FormatDates = dates => (
  dates.filter(date => date)
    .map(date => dayjs(date).format(dateFormat))
    .join(' ~ ')
);

export type FormatMemberCount = (bills: ShareRecord['bills']) => number;

export const formatMemberCount: FormatMemberCount = bills => (
  bills.reduce((carry, {members}) => (
    members.reduce((subCarry, {name}) => subCarry.add(name), carry)
  ), new Set<string>())
    .size
);

export type FormatMemberNameChain = (members: Bill['members']) => string;

export const formatMemberNameChain: FormatMemberNameChain = members => (
  members.map(({name}) => name).join(', ')
);

export type FormatBillMemberStatistics = (members: Bill['members']) => string;

export const formatBillMemberStatistics: FormatBillMemberStatistics = members => {
  const {length: memberLen} = members;
  const representMemberCount = 2;
  const representMembers = selfModule.formatMemberNameChain(
    members.slice(0, representMemberCount),
  );

  return (memberLen <= representMemberCount)
    ? representMembers
    : `${representMembers} 與其他 ${memberLen - representMemberCount} 位`;
};

export type FormatCurrencyAmountPayload = {
  currency: ShareRecord['currency'];
  amount: Payment['amount'];
};

export type FormatCurrencyAmount = (payload: FormatCurrencyAmountPayload) => string;

export const formatCurrencyAmount: FormatCurrencyAmount = ({currency, amount}) => {
  const amountNum = +amount;

  if (Number.isNaN(amountNum)) throw new TypeError('Invalid amount');

  return (
    new Intl.NumberFormat(
      'en',
      {
        currency,
        style: 'currency',
      },
    )
      .formatToParts(
        new Decimal(amountNum)
          .toDecimalPlaces(2)
          .toNumber(),
      )
      .reduce((carry, {value}) => `${carry}${value}`, '')
  );
};

export type FormatBillPaymentTotalAmount = (payments: Bill['payments']) => number;

export const formatBillPaymentTotalAmount: FormatBillPaymentTotalAmount = payments => (
  payments.reduce<Decimal>((carry, payment) => {
    const {
      useCurrencyExchange,
      amount,
      exchangeRate,
    } = payment;

    return carry.plus(
      !useCurrencyExchange
        ? amount
        : (new Decimal(amount)).times(exchangeRate),
    );
  }, new Decimal(0))
    .toNumber()
);

export type FormatMemberBillAmount = (payload: Pick<Bill, 'payments' | 'members'>) => number;

export const formatMemberBillAmount: FormatMemberBillAmount = ({payments, members}) => (
  new Decimal(
    selfModule.formatBillPaymentTotalAmount(payments),
  )
    .dividedBy(members.length)
    .toNumber()
);

export type FormatPaymentItemAmount = (bills: Bill[]) => Pick<Payment, 'item' | 'amount'>[];

export const formatPaymentItemAmount: FormatPaymentItemAmount = bills => bills.map(({payments, members: {length: memberLen}}) => (
  payments.map(({item, exchangeRate = 1, amount}) => ({
    item,
    amount: new Decimal(+amount)
      .times(exchangeRate)
      .dividedBy(memberLen)
      .toNumber(),
  }))
))
  .flat();

export type FormatBillTotal = (payload: Pick<ShareRecord, 'currency' | 'bills'>) => string;

export const formatBillTotal: FormatBillTotal = ({currency, bills}) => (
  selfModule.formatCurrencyAmount({
    currency,
    amount: bills.reduce((carry, {payments}) => (
      payments.reduce((subCarry, {amount}) => (
        subCarry.plus(new Decimal(amount))
      ), carry)
    ), new Decimal(0))
      .toNumber(),
  })
);

export type CreateMember = (name: string) => Member;

export const createMember: CreateMember = name => ({name});

export type FormatShareRecordDateString = (date: Date) => string;

export const formatShareRecordDateString: FormatShareRecordDateString = (date: Date) => (
  dayjs(date).format(dateFormat)
);

export type CreateDefaultPayment = () => PaymentWithNoCurrencyExchange;

export const createDefaultPayment: CreateDefaultPayment = () => ({
  useCurrencyExchange: false,
  item: '',
  amount: '',
  sourceCurrency: undefined,
  exchangeRate: undefined,
});

export type CreateBill = (payload: Bill) => Bill;

export const createBill: CreateBill = payload => {
  const {
    payments,
    host,
    members,
  } = payload;

  return {
    payments,
    host,
    members,
  };
};

export type FormatUniqueBillMembers = (bills: Bill[]) => Member[];

export const formatUniqueBillMembers: FormatUniqueBillMembers = bills => {
  const memberAddedMap: Record<string, boolean> = {};

  return bills.reduce<string[]>((carry, {members}) => (
    members.reduce<string[]>((subCarry, {name}) => {
      if (memberAddedMap[name]) {
        return subCarry;
      }

      memberAddedMap[name] = true;
      subCarry.push(name);

      return subCarry;
    }, carry)
  ), [])
    .map(name => createMember(name));
};

export type FormatMemberBillCopyContentPayload = {
  bills: Bill[];
  currency: ShareRecord['currency'];
  memberCheckMap: Record<Member['name'], boolean>;
};

export type FormatMemberBillCopyContent = (payload: FormatMemberBillCopyContentPayload) => string;

export const formatMemberBillCopyContent: FormatMemberBillCopyContent = ({bills, currency, memberCheckMap}) => {
  const memberAmountMap = bills.reduce<Record<string, Record<string, number>>>((carry, bill) => {
    const {host, members} = bill;
    const billAmountAverage = formatMemberBillAmount(bill);

    return members.reduce((subCarry, {name}) => {
      if (name === host) return subCarry;

      if (!subCarry[name]) {
        subCarry[name] = {};
      }

      if (!subCarry[name][host]) {
        subCarry[name][host] = 0;
      }

      subCarry[name][host] = new Decimal(subCarry[name][host])
        .add(billAmountAverage)
        .toNumber();

      return subCarry;
    }, carry);
  }, {});

  return Object.entries(memberAmountMap)
    .filter(([memberName]) => memberCheckMap[memberName])
    .reduce<string[]>((carry, [memberName, averageAmountMap]) => {
    carry.push(
      `👤️ ${memberName}:`,
      Array(20).fill('-').join(''),
    );

    return Object.entries(averageAmountMap)
      .reduce<string[]>((subCarry, [host, billAvarage]) => {
      const amount = new Decimal(billAvarage)
        .minus(memberAmountMap?.[host]?.[memberName] ?? 0)
        .toNumber();
      const currencyAmount = selfModule.formatCurrencyAmount({
        currency,
        amount,
      });

      if (amount > 0) {
        subCarry.push(` 💸️ 須支付 ${host}: ${currencyAmount}`, '');
      }

      return subCarry;
    }, carry);
  }, [])
    .join('\n');
};

export type CreateDefaultShareRecord = () => ShareRecord;

export const createDefaultShareRecord: CreateDefaultShareRecord = () => ({
  name: '',
  dates: ['', ''],
  mainHost: '',
  currency: '',
  bills: [],
});

export type CreateDefaultBill = () => Bill;

export const createDefaultBill: CreateDefaultBill = () => selfModule.createBill({
  payments: [],
  host: '',
  members: [],
});
