import {configureStore} from '@reduxjs/toolkit';
import {loadFromLocalStorage} from 'src/libs/redux-state';
import {memberHistoryReducer, shareRecordReducer} from 'src/store/reducers';
import {registerStoreSubscribers} from 'src/store/subscribers';

export const store = configureStore({
  reducer: {
    shareRecord: shareRecordReducer,
    memberHistory: memberHistoryReducer,
  },
  preloadedState: loadFromLocalStorage(),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

registerStoreSubscribers();
