import type {Reducer, AnyAction} from 'redux';
import type {Member} from 'src/types/resources';

export const updateMemberReferencesType = 'UPDATE_MEMBER_REFERENCES';

export const addMemberReferenceType = 'ADD_MEMBER_REFERENCES';

export const addMemberSearchKeywordType = 'ADD_MEMBER_SEARCH_KEYWORD';

export const removeMemberSearchKeywordType = 'REMOVE_MEMBER_SEARCH_KEYWORD';

type MemberHistoryInitState = {
  memberReferences: Member[];
  memberSearchKeywords: string[];
};

const initState: MemberHistoryInitState = {
  memberReferences: [],
  memberSearchKeywords: [],
};

interface UpdateMemberReferencesAction extends AnyAction {
  type: 'UPDATE_MEMBER_REFERENCES',
  payload?: {
    memberReferences: Member[];
  },
}

interface AddMemberReferencesAction extends AnyAction {
  type: 'ADD_MEMBER_REFERENCES',
  payload?: {
    memberReference: Member;
  },
}

interface AddSMemberSearchKeywordAction extends AnyAction {
  type: 'ADD_MEMBER_SEARCH_KEYWORD',
  payload?: {
    keyword: string;
  },
}

interface RemoveSMemberSearchKeywordAction extends AnyAction {
  type: 'REMOVE_MEMBER_SEARCH_KEYWORD',
  payload?: {
    index: number;
  },
}

export type Action =
  | UpdateMemberReferencesAction
  | AddMemberReferencesAction
  | AddSMemberSearchKeywordAction
  | RemoveSMemberSearchKeywordAction;

export const reducer: Reducer<MemberHistoryInitState, Action> = (inputState, action) => {
  const state = inputState ?? initState;
  const {type, payload} = action;

  switch (type) {
    case updateMemberReferencesType: {
      const memberReferences = payload?.memberReferences;

      return !memberReferences
        ? state
        : {
          ...state,
          memberReferences,
        };
    }
    case addMemberReferenceType: {
      const memberReference = payload?.memberReference;

      if (!memberReference) return state;

      const {name} = memberReference;

      if (state.memberReferences.find(member => member.name === name)) return state;

      return !memberReference
        ? state
        : {
          ...state,
          memberReferences: [
            ...state.memberReferences,
            memberReference,
          ],
        };
    }
    case addMemberSearchKeywordType: {
      const keyword = payload?.keyword;
      const {memberSearchKeywords} = state;

      if (!keyword) return state;

      const newMemberSearchKeywords = memberSearchKeywords.filter(curentKeyword => curentKeyword !== keyword);
      const searchKeywordSizeLimit = 5;

      return {
        ...state,
        memberSearchKeywords: [
          keyword,
          ...(
            newMemberSearchKeywords.length === searchKeywordSizeLimit
              ? state.memberSearchKeywords.slice(0, searchKeywordSizeLimit - 1)
              : newMemberSearchKeywords
          ),
        ],
      };
    }
    case removeMemberSearchKeywordType: {
      const index = payload?.index;
      const searchKeywordSizeLimit = 5;

      if (index === undefined || index < 0 || index >= searchKeywordSizeLimit) return state;

      return {
        ...state,
        memberSearchKeywords: state.memberSearchKeywords.filter((_curentKeyword, itemIndex) => itemIndex !== index),
      };
    }
    default:
      return state;
  }
};
