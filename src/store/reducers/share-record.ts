import type {Reducer, AnyAction} from 'redux';
import type {ShareRecord} from 'src/types/resources';

export const updateShareRecordsType = 'UPDATE_SHARE_RECORDS';

export const addShareRecordType = 'ADD_SHARE_RECORD';

export const updateShareRecordType = 'UPDATE_SHARE_RECORD';

export const removeShareRecordType = 'REMOVE_SHARE_RECORD';

type InitState = {
  shareRecords: ShareRecord[];
};

const initState: InitState = {
  shareRecords: [],
};

interface UpdateShareRecordsAction extends AnyAction {
  type: 'UPDATE_SHARE_RECORDS';
  payload?: {
    shareRecords: ShareRecord[];
  };
}

interface AddShareRecordAction extends AnyAction {
  type: 'ADD_SHARE_RECORD';
  payload?: {
    shareRecord: ShareRecord;
  };
}

interface UpdateShareRecordAction extends AnyAction {
  type: 'UPDATE_SHARE_RECORD';
  payload?: {
    index: number;
    shareRecord: Partial<ShareRecord>;
  };
}

interface RemoveShareRecordAction extends AnyAction {
  type: 'REMOVE_SHARE_RECORD';
  payload?: {
    index: number;
  };
}

export type Action =
 | UpdateShareRecordAction
 | AddShareRecordAction
 | UpdateShareRecordsAction
 | RemoveShareRecordAction;

export const reducer: Reducer<InitState, Action> = (inputState, action) => {
  const state = inputState ?? initState;
  const {type, payload} = action;

  switch(type) {
    case updateShareRecordsType: {
      const shareRecords = payload?.shareRecords;

      return shareRecords
        ? {
          ...state,
          shareRecords,
        }
        : state;
    }
    case addShareRecordType: {
      const shareRecord = payload?.shareRecord;

      return shareRecord
        ? {
          ...state,
          shareRecords: [
            ...state.shareRecords,
            shareRecord,
          ],
        }
        : state;
    }
    case updateShareRecordType: {
      const {shareRecords} = state;
      const shareRecord = payload?.shareRecord;
      const index = payload?.index;

      if (
        shareRecord === undefined ||
        index === undefined ||
        !shareRecords[index]
      ) {
        return state;
      }

      const newShareRecords = [...state.shareRecords];

      newShareRecords.splice(
        index,
        1,
        {
          ...shareRecords[index],
          ...shareRecord,
        },
      );

      return {
        ...state,
        shareRecords: newShareRecords,
      };
    }
    case removeShareRecordType: {
      const {shareRecords} = state;
      const index = payload?.index;

      if (index === undefined || !shareRecords[index]) return state;

      return {
        ...state,
        shareRecords: state.shareRecords.filter((_item, listIdx) => listIdx !== index),
      };
    }
    default:
      return state;
  }
};
