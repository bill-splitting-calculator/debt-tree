import type {Action} from 'src/store/reducers/member-history';
import {
  addMemberReferenceType,
  addMemberSearchKeywordType,
  removeMemberSearchKeywordType,
  updateMemberReferencesType,
} from 'src/store/reducers/member-history';
import type {Member} from 'src/types/resources';

export type UpdateMemberReferences = (memberReferences: Member[]) => Action;

export const updateMemberReferences: UpdateMemberReferences = memberReferences => ({
  type: updateMemberReferencesType,
  payload: {
    memberReferences,
  },
});

export type AddMemberReference = (memberReference: Member) => Action;

export const addMemberReference: AddMemberReference = memberReference => ({
  type: addMemberReferenceType,
  payload: {
    memberReference,
  },
});

export type AddMemberSearchKeyword = (keyword: string) => Action;

export const addMemberSearchKeyword: AddMemberSearchKeyword = keyword => ({
  type: addMemberSearchKeywordType,
  payload: {
    keyword,
  },
});

export type RemoveMemberSearchKeyword = (index: number) => Action;

export const removeMemberSearchKeyword: RemoveMemberSearchKeyword = index => ({
  type: removeMemberSearchKeywordType,
  payload: {
    index,
  },
});
