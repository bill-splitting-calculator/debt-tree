import type {Action} from 'src/store/reducers/share-record';
import {
  addShareRecordType,
  removeShareRecordType,
  updateShareRecordsType,
  updateShareRecordType,
} from 'src/store/reducers/share-record';
import type {ShareRecord} from 'src/types/resources';

export type UpdateShareRecords = (shareRecords: ShareRecord[]) => Action;

export const updateShareRecords: UpdateShareRecords = shareRecords => ({
  type: updateShareRecordsType,
  payload: {
    shareRecords,
  },
});

export type AddShareRecord = (shareRecord: ShareRecord) => Action;

export const addShareRecord: AddShareRecord = shareRecord => ({
  type: addShareRecordType,
  payload: {
    shareRecord,
  },
});

export type UpdateShareRecord = (index: number, shareRecord: Partial<ShareRecord>) => Action;

export const updateShareRecord: UpdateShareRecord = (index, shareRecord) => ({
  type: updateShareRecordType,
  payload: {
    index,
    shareRecord,
  },
});

export type RemoveShareRecord = (index: number) => Action;

export const removeShareRecord: RemoveShareRecord = index => ({
  type: removeShareRecordType,
  payload: {
    index,
  },
});
