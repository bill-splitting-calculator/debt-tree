import debounce from 'lodash.debounce';
import {saveToLocalStorage} from 'src/libs/redux-state';
import {store} from 'src/store';

export type RegisterLocalStorageSaving = () => void;

export const registerLocalStorageSaving: RegisterLocalStorageSaving = () => {
  store.subscribe(
    debounce(
      () => saveToLocalStorage(store.getState()),
      1000,
    ),
  );
};

export type Register = () => void;

export const register: Register = () => {
  registerLocalStorageSaving();
};
