import {StrictMode} from 'react';
import ReactDOM from 'react-dom/client';
import {Provider} from 'react-redux';
import {App} from 'src/components/main';
import {register as registerServiceWorker} from 'src/service-worker/registration';
import {store} from 'src/store';
import 'src/styles/_base.scss';

const rootContainer = document.getElementById('root');

if (!rootContainer) throw new Error('Invalid root, please checkout your template');

ReactDOM.createRoot(rootContainer)
  .render(
    <StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </StrictMode>,
  );

registerServiceWorker();
