export type Member = {
  name: string;
};

export type PaymentBase = {
  useCurrencyExchange: boolean;
  item: string;
  amount: number | '';
};

export type PaymentWithNoCurrencyExchange = PaymentBase & {
  useCurrencyExchange: false;
  sourceCurrency?: never;
  exchangeRate?: never;
};

export type PaymentWithCurrencyExchange = PaymentBase & {
  useCurrencyExchange: true;
  sourceCurrency: string;
  exchangeRate: number | '';
};

export type Payment =
 | PaymentWithNoCurrencyExchange
 | PaymentWithCurrencyExchange;

export type Bill = {
  payments: Payment[];
  host: string;
  members: Member[];
};

export type ShareRecord= {
  name: string;
  dates: [string, string];
  mainHost?: string;
  currency: string;
  bills: Bill[];
};
