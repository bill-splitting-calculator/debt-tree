export type ModalType =
  | 'CREATE_SHARE_RECORD'
  | 'EDIT_SHARE_RECORD'
  | 'CREATE_SHARE_RECORD_BILL'
  | 'EDIT_SHARE_RECORD_BILL';

export type Variant = 'default' | 'primary' | 'danger';

export type DataAttrProps = {
  dataAttrs?: Record<string, string>;
};
