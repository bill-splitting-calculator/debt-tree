export type AppLayout =
 | 'MOBILE'
 | 'PAD'
 | 'DESKTOP';
