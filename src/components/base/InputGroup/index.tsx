import classnames from 'classnames';
import type {FC, HTMLAttributes, ReactElement, ReactNode} from 'react';
import type {InputProps} from 'src/components/base/Input';
import styles from './styles.module.scss';

export type InputGroupProps = HTMLAttributes<HTMLDivElement> & {
  children: ReactElement<InputProps>;
  groupItem: ReactNode;
};

export const InputGroup: FC<InputGroupProps> = props => {
  const {
    children,
    className,
    groupItem,
    ...otherProps
  } = props;

  return (
    <div
      {...otherProps}
      className={classnames(styles.main, className)}>
      <span className={styles.containerInput}>
        {children}
      </span>
      <div className={styles.containerGroupItem}>{groupItem}</div>
    </div>
  );
};
