import type {FC, MouseEventHandler, ReactNode} from 'react';
import {useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import {ModalHeader, Portal} from 'src/components/base';
import {DataAttrProps} from 'src/types/components';
import {ModalHeaderProps} from '../ModalHeader';
import styles from './styles.module.scss';

export type FullScaledModalContainerProps = DataAttrProps & {
  name: string;
  header: string;
  children: ReactNode;
  open: boolean;
  doneText?: ModalHeaderProps['doneText'];
  doneDisabled?: ModalHeaderProps['doneDisabled'];
  onDoneClick?: ModalHeaderProps['onDoneClick'];
  onOpenChange: (open: boolean) => void;
  onExited?: () => void;
};

export const FullScaledModalContainer: FC<FullScaledModalContainerProps> = props => {
  const {
    name,
    header,
    children,
    open,
    doneText,
    doneDisabled,
    onDoneClick,
    onOpenChange,
    onExited,
  } = props;

  const modalContentRef = useRef<HTMLDivElement | null>(null);

  const onBackPreviousClick: MouseEventHandler<HTMLDivElement> = () => {
    onOpenChange(false);
  };

  return (
    <Portal portalId={`${name}-modal`}>
      <CSSTransition
        in={open}
        timeout={500}
        nodeRef={modalContentRef}
        appear
        unmountOnExit
        classNames={{
          enter: styles.modalEnter,
          enterActive: styles.modalEnterActive,
          exit: styles.modalExit,
          exitActive: styles.modalExitActive,
        }}
        onExited={onExited}>
        <div
          ref={modalContentRef}
          className={styles.main}
          data-testid={`${name}-modal`}>
          <ModalHeader
            header={header}
            className={styles.header}
            doneText={doneText}
            doneDisabled={doneDisabled}
            onDoneClick={onDoneClick}
            onBackPreviousClick={onBackPreviousClick} />
          <div className={styles.body}>
            {children}
          </div>
        </div>
      </CSSTransition>
    </Portal>
  );
};
