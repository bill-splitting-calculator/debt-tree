import type {FC} from 'react';
import styles from './styles.module.scss';

export type TextItemProps = {
  children: string;
};

export const TextItem: FC<TextItemProps> = ({children}) => (
  <span className={styles.main}>{children}</span>
);
