import type {HTMLAttributes} from 'react';
import {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import {
  Input,
  Portal,
  SearchKeywordHistory,
  TextItemList,
} from 'src/components/base';
import type {InputProps} from 'src/components/base/Input';
import type {SearchKeywordHistoryProps} from 'src/components/base/SearchKeywordHistory';
import type {TextItemListProps} from 'src/components/base/TextItemList';
import styles from './styles.module.scss';

export type SearchInputDropdownMenuProps = {
  name: string;
  open: boolean;
  textList: TextItemListProps['textList'];
  searchKeywords?: TextItemListProps['textList'];
  className: HTMLAttributes<HTMLDivElement>['className'];
  style: HTMLAttributes<HTMLDivElement>['style'];
  onKeywordSearch: (keyword: string) => void | Promise<void>;
  onOpenChange: (open: boolean) => void;
  onKeywordCreate: TextItemListProps['onKeywordCreate'];
  onSearchResultClick: TextItemListProps['onTextItemClick'];
  onSearchKeywordRemove: SearchKeywordHistoryProps['onSearchKeywordRemove'];
};

export const SearchInputDropdownMenu = forwardRef<HTMLDivElement | null, SearchInputDropdownMenuProps>((props, ref) => {
  const {
    name,
    open,
    textList,
    searchKeywords,
    className,
    style,
    onKeywordSearch,
    onOpenChange,
    onKeywordCreate,
    onSearchResultClick,
    onSearchKeywordRemove,
  } = props;

  const [keyword, setKeyword] = useState<string>('');
  const mainRef = useRef<HTMLDivElement | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);

  useImperativeHandle<HTMLDivElement | null, HTMLDivElement | null>(ref, () => mainRef.current);

  const onInputFocus: InputProps['onFocus'] = () => {
    if (!keyword) return;

    onKeywordSearch(keyword);
  };

  const onInputChange: InputProps['onChange'] = ({target: {value: newKeyword}}) => {
    setKeyword(newKeyword);
    onKeywordSearch(newKeyword);
  };

  const onInnerKeywordCreate: TextItemListProps['onKeywordCreate'] = newKeyword => {
    onKeywordCreate(newKeyword);
    onOpenChange(false);
  };

  const onInnerKeywordItemClick: TextItemListProps['onTextItemClick'] = payload => {
    onSearchResultClick(payload);
    onOpenChange(false);
  };

  const onInputReset: InputProps['onReset'] = () => {
    onSearchResultClick({
      value: '',
      index: -1,
    });
  };

  useEffect(() => {
    if (!open) return;

    inputRef.current?.focus();
  });

  useEffect(() => {
    if (open) return;

    setKeyword('');
  }, [open]);

  return (
    <Portal
      portalId="search-input-dropdown-menu"
      portalClassName={styles.portalMenu}>
      <div
        ref={mainRef}
        className={className}
        style={style}
        data-testid={`${name}-search-input-dropdown-menu`}>
        <Input
          key={open ? '1' : '0'}
          ref={inputRef}
          name={`${name}-search-input-dropdown-menu-keyword`}
          fieldName="請輸入查詢關鍵字"
          lazy
          lazyInterval={100}
          resetable={!!keyword}
          onFocus={onInputFocus}
          onChange={onInputChange}
          onReset={onInputReset} />
        {
          open && keyword && textList && (
            <TextItemList
              keyword={keyword}
              textList={textList}
              onKeywordCreate={onInnerKeywordCreate}
              onTextItemClick={onInnerKeywordItemClick} />
          )
        }
        {
          !open && !keyword && searchKeywords && (
            <SearchKeywordHistory
              searchKeywords={searchKeywords}
              onSearchKeywordClick={onInnerKeywordItemClick}
              onSearchKeywordRemove={onSearchKeywordRemove} />
          )
        }
      </div>
    </Portal>
  );
});
