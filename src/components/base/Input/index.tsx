import classnames from 'classnames';
import {Close} from 'grommet-icons';
import debounce from 'lodash.debounce';
import type {ChangeEventHandler, InputHTMLAttributes, MouseEventHandler} from 'react';
import {
  forwardRef,
  useImperativeHandle,
  useMemo,
  useRef,
} from 'react';
import styles from './styles.module.scss';

type OnReset = MouseEventHandler<HTMLSpanElement>;

export type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  type?: 'text' | 'number';
  fieldName: string;
  hasError?: boolean;
  lazy?: boolean;
  lazyInterval?: number;
  resetable?: boolean;
  containerClassName?: string;
  onReset?: OnReset;
};

export const Input = forwardRef<HTMLInputElement | null, InputProps>((
  {
    name = window.crypto.randomUUID(),
    type = 'text',
    fieldName,
    hasError = false,
    lazy = false,
    lazyInterval = 500,
    className,
    containerClassName,
    resetable,
    onChange,
    onReset,
    ...otherProps
  },
  ref,
) => {
  const onInputChange = useMemo(() => (
    lazy
      ? debounce<ChangeEventHandler<HTMLInputElement>>(e => {
        onChange?.(e);
      }, lazyInterval)
      : onChange
  ), [lazy, lazyInterval, onChange]);

  const internalRef = useRef<HTMLInputElement | null>(null);

  useImperativeHandle<HTMLInputElement | null, HTMLInputElement | null>(ref, () => internalRef.current);

  const onInnerReset: OnReset = e => {
    const {current: inputEl} = internalRef;

    if (!inputEl) return;

    inputEl.value = '';
    inputEl.dispatchEvent(
      new Event('change', {bubbles: true}),
    );
    inputEl.focus();

    onReset?.(e);
  };

  return (
    <span className={
      classnames(
        styles.main,
        containerClassName,
      )
    }>
      <input
        {...otherProps}
        data-testid={`${name}-input`}
        ref={internalRef}
        type={type}
        placeholder={fieldName}
        className={
          classnames(
            className,
            styles.input,
            {[styles.hasError]: hasError},
          )
        }
        onChange={onInputChange} />
      {
        fieldName && (
          <span className={
            classnames(
              styles.fieldName,
              {[styles.hasError]: hasError},
            )
          }>
            {fieldName}
          </span>
        )
      }
      {
        resetable && (
          <span
            className={styles.containerClear}
            data-testid={`${name}-reset-icon`}
            onClick={onInnerReset}>
            <Close color="plain" />
          </span>
        )
      }
    </span>
  );
});

Input.displayName = 'Input';

