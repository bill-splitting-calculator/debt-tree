import classnames from 'classnames';
import {CaretDownFill, CaretUpFill} from 'grommet-icons';
import type {FC, MouseEventHandler, ReactNode} from 'react';
import {ReactElement, useEffect, useRef, useState} from 'react';
import {createRoot} from 'react-dom/client';
import {formatDataAttrsProps} from 'src/formatters/data-attrs';
import {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type CollapseProps = DataAttrProps & {
  defaultCollapsed?: boolean;
  headerChildren: ReactNode;
  children: ReactElement;
  onClick?: MouseEventHandler<HTMLDivElement>;
};

export const Collapse: FC<CollapseProps> = props => {
  const {
    defaultCollapsed = true,
    headerChildren,
    children,
    dataAttrs,
    onClick,
  } = props;

  const [collapsed, setCollapsed] = useState<boolean>(defaultCollapsed);
  const collapseMenuRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const container = document.createElement('div');

    container.style.position = 'absolute';
    container.style.top = '-9999px';
    container.style.padding = '0.75rem 1rem';

    const root = createRoot(container);

    root.render(children);
    document.body.appendChild(container);

    const observer = new MutationObserver(() => {
      if (!container.childElementCount) return;

      const {clientHeight: menuHeight} = container;
      const {current: collapseMenuEl} = collapseMenuRef;

      if (!collapseMenuEl) return;

      collapseMenuEl.style.setProperty('--menu-height', `${menuHeight}px`);
      collapseMenuEl.style.setProperty('--menu-transition-second', `${(menuHeight / 1000 * 0.5).toFixed(2)}s`);

      root.unmount();
      container.parentElement?.removeChild(container);

      observer.disconnect();
    });

    observer.observe(
      container,
      {
        attributes: false,
        childList: true,
        characterData: false,
        subtree: false,
      },
    );
  }, [children]);

  const onCollapseClick: MouseEventHandler<HTMLDivElement> = e => {
    onClick?.(e);

    if (e.isDefaultPrevented()) return;

    setCollapsed(!collapsed);
  };

  return (
    <div
      className={styles.main}
      {...formatDataAttrsProps(dataAttrs)}>
      <header
        className={styles.containerCollapseHeader}
        onClick={onCollapseClick}>
        <div className={styles.collpasedHeaderChildren}>{headerChildren}</div>
        <aside className={styles.containerIconNext}>
          {
            collapsed
              ? <CaretDownFill color="plain" />
              : <CaretUpFill color="plain" />
          }
        </aside>
      </header>
      <div
        ref={collapseMenuRef}
        className={
          classnames(
            styles.containerCollapseMenu,
            {
              [styles.expanded]: !collapsed,
            },
          )
        }>
        <div className={styles.containerCollapseMenuChildren}>{children}</div>
      </div>
    </div>
  );
};
