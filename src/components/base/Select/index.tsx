import classnames from 'classnames';
import type {FC, HTMLAttributes} from 'react';
import styles from './styles.module.scss';

export type SelectProps = HTMLAttributes<HTMLSelectElement> & {
  name?: string;
  value?: string;
  defaultValue?: string;
  hasError?: boolean;
  placeholder?: string;
};

export const Select: FC<SelectProps> = props => {
  const {
    name,
    children,
    placeholder,
    className,
    hasError,
    ...otherProps
  } = props;

  return (
    <span className={styles.main}>
      <select
        {...otherProps}
        data-testid={`${name}-select`}
        className={
          classnames(
            styles.select,
            {
              [styles.hasError]: hasError,
            },
            className,
          )
        }>
        {
          placeholder && (
            <option value="">
              {placeholder}
            </option>
          )
        }
        {children}
      </select>
    </span>
  );
};
