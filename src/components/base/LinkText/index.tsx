import classnames from 'classnames';
import type {FC, HTMLAttributes} from 'react';
import {formatDataAttrsProps} from 'src/formatters/data-attrs';
import {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type LinkTextProps = HTMLAttributes<HTMLElement> & DataAttrProps & {
  children: string;
  disabled?: boolean;
  variant?: 'default' | 'danger';
};

export const LinkText: FC<LinkTextProps> = props => {
  const {
    className,
    children,
    disabled,
    variant = 'default',
    dataAttrs,
    ...otherProps
  } = props;

  return (
    <span
      {...otherProps}
      {...formatDataAttrsProps(dataAttrs)}
      className={
        classnames(
          styles.main,
          className,
          {
            [styles.disabled]: disabled,
          },
          styles[variant],
        )
      }>
      {children}
    </span>
  );
};
