import classnames from 'classnames';
import {
  Checkbox as CheckboxIcon,
  CheckboxSelected as CheckboxSelectedIcon,
} from 'grommet-icons';
import type {ChangeEventHandler, InputHTMLAttributes} from 'react';
import {forwardRef} from 'react';
import styles from './styles.module.scss';

export type CheckboxProps = InputHTMLAttributes<HTMLInputElement> & {
  type?: never;
  checked: boolean;
  onChange: ChangeEventHandler<HTMLInputElement>;
};

export const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>((
  {
    name,
    checked,
    className,
    ...otherProps
  },
  ref,
) => (
  <span className={styles.main}>
    <input
      {...otherProps}
      ref={ref}
      name={`${name}-checkbox`}
      data-testid={`${name}-checkbox`}
      checked={checked}
      className={classnames(className, styles.input)}
      type="checkbox" />
    <span className={styles.containerIconInput}>
      {
        checked
          ? <CheckboxSelectedIcon color="plain" />
          : <CheckboxIcon color="plain" />
      }
    </span>
  </span>
));

Checkbox.displayName = 'Checkbox';
