import type {FC} from 'react';
import {CircleInformation} from 'grommet-icons';
import styles from './styles.module.scss';

export type ValidationMessageProps = {
  message?: string;
};

export const ValidationMessage: FC<ValidationMessageProps> = ({message}) => {
  if (!message) return null;

  return (
    <div className={styles.main}>
      <CircleInformation color="plain" />
      <span>{message}</span>
    </div>
  );
};
