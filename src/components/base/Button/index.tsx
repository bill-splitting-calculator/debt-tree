import classnames from 'classnames';
import type {DataAttrProps} from 'src/types/components';
import type {FC, HTMLAttributes} from 'react';
import styles from './styles.module.scss';
import {formatDataAttrsProps} from 'src/formatters/data-attrs';

export type ButtonProps = HTMLAttributes<HTMLButtonElement> & DataAttrProps & {
  variant?: 'default' | 'primary';
  disabled?: boolean;
};

export const Button: FC<ButtonProps> = props => {
  const {
    variant = 'default',
    disabled = false,
    className,
    children,
    dataAttrs,
    ...otherProps
  } = props;

  return (
    <button
      {...otherProps}
      {...formatDataAttrsProps(dataAttrs)}
      disabled={disabled}
      className={
        classnames(
          styles.main,
          className,
          styles[variant],
          {[styles.disabled]: disabled},
        )
      }>
      {children}
    </button>
  );
};
