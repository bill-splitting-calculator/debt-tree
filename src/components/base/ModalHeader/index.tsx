import classnames from 'classnames';
import {Previous} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {LinkText} from 'src/components/base';
import styles from './styles.module.scss';

export type ModalHeaderProps = {
  header: string;
  className?: string;
  doneText?: string;
  doneDisabled?: boolean;
  onBackPreviousClick: MouseEventHandler<HTMLDivElement>;
  onDoneClick?: MouseEventHandler<HTMLElement>;
};

export const ModalHeader: FC<ModalHeaderProps> = props => {
  const {
    header,
    className,
    doneText = '儲存',
    doneDisabled = false,
    onBackPreviousClick,
    onDoneClick,
  } = props;

  return (
    <header className={classnames(styles.main, className)}>
      <aside
        className={styles.containerBackPrevious}
        data-testid="modal-header-back-icon-text"
        onClick={onBackPreviousClick}>
        <Previous color="plain" />
        <span>返回</span>
      </aside>
      <span
        data-testid="modal-header-title"
        className={styles.textHeader}>{header}</span>
      <aside
        className={styles.containerSave}
        onClick={onDoneClick}>
        <LinkText
          disabled={doneDisabled}
          data-testid="modal-header-done-text">
          {doneText}
        </LinkText>
      </aside>
    </header>
  );
};
