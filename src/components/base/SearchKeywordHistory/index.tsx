import {DocumentTime} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {SearchKeywordItem} from 'src/components/base';
import type {SearchKeywordItemProps} from 'src/components/base/SearchKeywordItem';
import {IconHeader} from 'src/components/page';
import {formatDataAttrsProps} from 'src/formatters/data-attrs';
import {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type SearchKeywordHistoryProps = DataAttrProps & {
  searchKeywords: string[];
  onSearchKeywordClick: (payload: {index: number; value: string;}) => void;
  onSearchKeywordRemove: (index: number) => void;
};

type CreateOnSearchKeywordClick = (index: number, value: string) => MouseEventHandler<HTMLLIElement>;
type CreateOnSearchKeywordRemove = (index: number) => SearchKeywordItemProps['onClose'];

export const SearchKeywordHistory: FC<SearchKeywordHistoryProps> = props => {
  const {
    searchKeywords,
    dataAttrs,
    onSearchKeywordClick,
    onSearchKeywordRemove,
  } = props;

  const createOnSearchKeywordClick: CreateOnSearchKeywordClick = (index, value) => () => {
    onSearchKeywordClick({
      index,
      value,
    });
  };

  const createOnSearchKeywordRemove: CreateOnSearchKeywordRemove = index => () => {
    onSearchKeywordRemove(index);
  };

  return (
    <div
      {...formatDataAttrsProps(dataAttrs)}
      className={styles.main}>
      <IconHeader icon={DocumentTime}>最近搜尋紀錄</IconHeader>
      {
        !searchKeywords.length
          ? (
            <div className={styles.containerEmptySearchKeywords}>
              <i className={styles.textDetective}>🕵️</i>
              <span>無搜尋紀錄</span>
            </div>
          )
          : (
            <ul className={styles.listTextItem}>
              {
                searchKeywords.map((searchKeyword, index) => (
                  <li
                    key={index}
                    className={styles.listItemSearchKeyword}
                    onClick={createOnSearchKeywordClick(index, searchKeyword)}>
                    <SearchKeywordItem onClose={createOnSearchKeywordRemove(index)}>
                      {searchKeyword}
                    </SearchKeywordItem>
                  </li>
                ))
              }
            </ul>
          )
      }
    </div>
  );
};
