import classnames from 'classnames';
import {Checkmark} from 'grommet-icons';
import type {FC, MouseEventHandler, ReactNode} from 'react';
import {useState} from 'react';
import {formatDataAttrsProps} from 'src/formatters/data-attrs';
import {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type CopyIndicatorProps = DataAttrProps & {
  children: ReactNode;
  timeout?: number;
  className?: string;
  onClick: MouseEventHandler<HTMLDivElement>;
};

export const CopyIndicator: FC<CopyIndicatorProps> = props => {
  const {
    children,
    timeout = 3000,
    className,
    dataAttrs,
    onClick,
  } = props;

  const [isNotificationPresent, onIsNotificationPresentChange] = useState<boolean>(false);

  const onInnerClick: CopyIndicatorProps['onClick'] = e => {
    onIsNotificationPresentChange(true);

    const timer = window.setTimeout(() => {
      onIsNotificationPresentChange(false);

      window.clearTimeout(timer);
    }, timeout);

    onClick(e);
  };

  return (
    <div
      {...formatDataAttrsProps(dataAttrs)}
      className={
        classnames(
          styles.main,
          className,
        )
      }>
      {
        isNotificationPresent
          ? (
            <span className={styles.containerCopyNotification}>
              <Checkmark color="plain" />
              已複製至剪貼簿
            </span>
          )
          : (
            <div onClick={onInnerClick}>
              {children}
            </div>
          )
      }
    </div>
  );
};
