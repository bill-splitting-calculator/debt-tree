import classnames from 'classnames';
import type {FC, UIEventHandler, HTMLAttributes} from 'react';
import {TextItem, LinkText} from 'src/components/base';
import styles from './styles.module.scss';

export type TextItemListProps = HTMLAttributes<HTMLDivElement> & {
  keyword: string;
  textList?: string[];
  onKeywordCreate: (keyword: string) => void;
  onTextItemClick: (payload: {value: string, index: number}) => void;
};

export const TextItemList: FC<TextItemListProps> = props => {
  const {
    textList,
    keyword,
    className,
    onKeywordCreate,
    onTextItemClick,
  } = props;

  const onSearchResultClick: UIEventHandler<HTMLDivElement> = () => {
    onKeywordCreate(keyword);
  };

  if (!textList) return null;

  return (
    <div className={
      classnames(
        styles.main,
        className,
      )
    }>
      {
        textList.length
          ? (
            <ul className={styles.listTextItem}>
              {
                textList.map((text, index) => (
                  <li
                    key={index}
                    className={styles.listItemTextItem}
                    onClick={() => onTextItemClick({value: text, index})}>
                    <TextItem>{text}</TextItem>
                  </li>
                ))
              }
            </ul>
          )
          : (
            <div className={styles.textNoTextList}>查無資料</div>
          )
      }
      {
        keyword && !textList.includes(keyword) && (
          <div
            className={styles.containerCreateKeywordText}
            onClick={onSearchResultClick}>
            <LinkText>{` + 新增 ${keyword}`}</LinkText>
          </div>
        )
      }
    </div>
  );
};
