import classnames from 'classnames';
import type {FC} from 'react';
import {useEffect, useState} from 'react';
import {Input, SearchInputDropdownMenu, SearchInputMenuModal} from 'src/components/base';
import type {InputProps} from 'src/components/base/Input';
import type {SearchInputDropdownMenuProps} from 'src/components/base/SearchInputDropdownMenu';
import type {SearchInputMenuModalProps} from 'src/components/base/SearchInputMenuModal';
import type {TextItemListProps} from 'src/components/base/TextItemList';
import {useAppLayout, useFloatingMenu} from 'src/hooks';
import styles from './styles.module.scss';

export type SearchInputProps = Omit<InputProps, 'value'> & {
  textList: TextItemListProps['textList'];
  searchKeywords: string[];
  onKeywordSearch: (keyword: string) => void | Promise<void>;
  onKeywordCreate: TextItemListProps['onKeywordCreate'];
  onSearchResultClick: TextItemListProps['onTextItemClick'];
  onSearchKeywordRemove: SearchInputDropdownMenuProps['onSearchKeywordRemove'] | SearchInputMenuModalProps['onSearchKeywordRemove'];
};

export const SearchInput: FC<SearchInputProps> = props => {
  const {
    name = window.crypto.randomUUID(),
    fieldName,
    hasError,
    lazy,
    lazyInterval,
    defaultValue,
    textList,
    searchKeywords,
    onKeywordSearch,
    onKeywordCreate,
    onSearchResultClick,
    onSearchKeywordRemove,
    ...inputProps
  } = props;

  const {appLayout} = useAppLayout();
  const isDesktopLayout = appLayout === 'DESKTOP';

  const [dropdownMenuOffset, onDropdownMenuOffsetChange] = useState<number>(0);

  const {
    inputRef,
    menuRef,
    isMenuVisible,
    menuStyle,
    onMenuStyleUpdate,
    onIsMenuVisibleChange,
  } = useFloatingMenu({
    disabled: !isDesktopLayout,
    offset: dropdownMenuOffset,
  });

  const [keyword, setKeyword] = useState<string>(defaultValue ? `${defaultValue}` : '');
  const [isSearchMenuOpen, onSearchMenuOpenChange] = useState<boolean>(false);

  useEffect(() => {
    setKeyword(
      typeof defaultValue === 'string'
        ? defaultValue
        : '',
    );
  }, [defaultValue]);

  useEffect(() => {
    onDropdownMenuOffsetChange(
      (inputRef.current?.offsetHeight ?? 0) * -1,
    );
  }, [inputRef]);

  const onInputClick: InputProps['onClick'] = () => {
    if (isDesktopLayout) {
      if (isMenuVisible) return;

      onIsMenuVisibleChange(true);
      onMenuStyleUpdate();

      return;
    }

    if (isSearchMenuOpen) return;

    onSearchMenuOpenChange(true);
  };

  return (
    <>
      {
        isDesktopLayout
          ? (
            <SearchInputDropdownMenu
              ref={menuRef}
              name={name}
              open={isMenuVisible}
              textList={textList}
              searchKeywords={searchKeywords}
              style={menuStyle}
              className={
                classnames(
                  styles.dropdownMenu,
                  {
                    [styles.onscreen]: isMenuVisible,
                  },
                )
              }
              onOpenChange={onIsMenuVisibleChange}
              onKeywordSearch={onKeywordSearch}
              onKeywordCreate={onKeywordCreate}
              onSearchResultClick={onSearchResultClick}
              onSearchKeywordRemove={onSearchKeywordRemove} />
          )
          : (
            <SearchInputMenuModal
              name={name}
              open={isSearchMenuOpen}
              onOpenChange={onSearchMenuOpenChange}
              textList={textList}
              searchKeywords={searchKeywords}
              onKeywordSearch={onKeywordSearch}
              onKeywordCreate={onKeywordCreate}
              onSearchResultClick={onSearchResultClick}
              onSearchKeywordRemove={onSearchKeywordRemove} />
          )
      }
      <Input
        {...inputProps}
        ref={inputRef}
        name={name}
        fieldName={fieldName}
        hasError={hasError}
        defaultValue={lazy ? keyword : undefined}
        value={lazy ? undefined : keyword}
        lazy={lazy}
        lazyInterval={lazyInterval}
        readOnly
        containerClassName={
          classnames(
            {
              [styles.hidden]: isMenuVisible,
            },
          )
        }
        onClick={onInputClick} />
    </>
  );
};
