import classnames from 'classnames';
import type {FC} from 'react';
import {useRef, useState} from 'react';
import {CSSTransition} from 'react-transition-group';
import {Calendar, Input, Portal} from 'src/components/base';
import type {CalendarProps} from 'src/components/base/Calendar';
import type {InputProps} from 'src/components/base/Input';
import {formatDate} from 'src/formatters/calendar';
import {useAppLayout, useFloatingMenu} from 'src/hooks';
import styles from './styles.module.scss';

export type DatePickerProps = {
  name: string;
  fieldName: InputProps['fieldName'];
  hasError: InputProps['hasError'];
  value?: CalendarProps['value'];
  min?: CalendarProps['min'];
  max?: CalendarProps['max'];
  onChange: (value: string) => void;
};

export const DatePicker: FC<DatePickerProps> = props => {
  const {
    name,
    fieldName,
    hasError,
    value,
    min,
    max,
    onChange,
  } = props;

  const now = formatDate(value ?? '').toDate();
  const [year, setYear] = useState<number>(now.getFullYear());
  const [month, setMonth] = useState<number>(now.getMonth() + 1);

  const {
    inputRef,
    menuRef,
    isMenuVisible,
    menuStyle,
    onIsMenuVisibleChange,
    onMenuStyleUpdate,
    onMenuHide,
  } = useFloatingMenu();

  const {appLayout} = useAppLayout();

  const calendarContainerRef = useRef<HTMLDivElement | null>(null);

  const onInputClick: InputProps['onClick'] = () => {
    onIsMenuVisibleChange(true);
    onMenuStyleUpdate();
  };

  const onDateReset: InputProps['onReset'] = () => {
    onChange('');
    onMenuHide();
  };

  const onDateChange: CalendarProps['onChange'] = newValue => {
    onChange(newValue);
    onMenuHide();
  };

  return (
    <>
      <Portal
        portalId={`${name}-date-picker-menu`}
        portalClassName={styles.portalDatePicker}>
        {
          appLayout === 'DESKTOP'
            ? (
              <div
                ref={calendarContainerRef}
                className={
                  classnames(
                    styles.containerCalendar,
                    {
                      [styles.onscreen]: isMenuVisible,
                    },
                  )
                }
                style={menuStyle}
                data-testid={`${name}-date-picker-calendar-container`}>
                <Calendar
                  ref={menuRef}
                  name={name}
                  year={year}
                  month={month}
                  value={value}
                  min={min}
                  max={max}
                  highlightToday
                  onYearChange={setYear}
                  onMonthChange={setMonth}
                  onChange={onDateChange} />
              </div>
            )
            : (
              <CSSTransition
                nodeRef={calendarContainerRef}
                in={isMenuVisible}
                timeout={300}
                appear
                unmountOnExit
                classNames={{
                  enter: styles.containerCalendarEnter,
                  enterActive: styles.containerCalendarEnterActive,
                  exit: styles.containerCalendarExit,
                  exitActive: styles.containerCalendarExitActive,
                }}>
                <div
                  ref={calendarContainerRef}
                  className={styles.containerCalendar}
                  data-testid={`${name}-date-picker-calendar-container`}>
                  <Calendar
                    ref={menuRef}
                    name={name}
                    year={year}
                    month={month}
                    value={value}
                    min={min}
                    max={max}
                    highlightToday
                    onYearChange={setYear}
                    onMonthChange={setMonth}
                    onChange={onDateChange} />
                </div>
              </CSSTransition>
            )
        }
      </Portal>
      <Input
        ref={inputRef}
        name={`${name}-date-picker`}
        fieldName={fieldName}
        hasError={hasError}
        value={value}
        resetable={!!value}
        readOnly
        onClick={onInputClick}
        onReset={onDateReset} />
    </>
  );
};
