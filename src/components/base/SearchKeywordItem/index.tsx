import {FormClose, History} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import styles from './styles.module.scss';

export type SearchKeywordItemProps = {
  children: string;
  onClose?: () => void;
};

export const SearchKeywordItem: FC<SearchKeywordItemProps> = ({children, onClose}) => {
  const onInnerClose: MouseEventHandler<HTMLSpanElement> = e => {
    e.stopPropagation();

    onClose?.();
  };

  return (
    <div className={styles.main}>
      <span className={styles.containerIconHistory}>
        <History color="plain" />
      </span>
      <span className={styles.textChildren}>{children}</span>
      <span
        className={styles.containerIconFormClose}
        onClick={onInnerClose}>
        <FormClose color="plain" />
      </span>
    </div>
  );
};
