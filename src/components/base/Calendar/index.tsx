import classnames from 'classnames';
import {CaretLeftFill, CaretRightFill} from 'grommet-icons';
import {forwardRef, UIEventHandler} from 'react';
import type {Date} from 'src/formatters/calendar';
import {
  formatDate,
  formatDates,
  formatDateValue,
  formatWeekdays,
  formatYearMonth,
} from 'src/formatters/calendar';
import styles from './styles.module.scss';

export type CalendarProps = {
  name?: string;
  year: number;
  month: number;
  value?: string;
  min?: string;
  max?: string;
  highlightToday?: boolean;
  onYearChange: (year: number) => void;
  onMonthChange: (month: number) => void;
  onChange: (date: string) => void;
};

export const Calendar = forwardRef<HTMLDivElement, CalendarProps>((props, ref) => {
  const {
    name,
    year,
    month,
    value,
    min,
    max,
    highlightToday = false,
    onYearChange,
    onMonthChange,
    onChange,
  } = props;

  const onMonthSwitchBack: UIEventHandler<HTMLElement> = () => {
    const previousMonth = month - 1 || 12;

    onMonthChange(previousMonth);

    if (month === 1 && previousMonth === 12) {
      onYearChange(year - 1);
    }
  };

  const onMonthSwitchForward: UIEventHandler<HTMLElement> = () => {
    const nextMonth = (month + 1) % 12 || 12;

    onMonthChange(nextMonth);

    if (month === 12 && nextMonth === 1) {
      onYearChange(year + 1);
    }
  };

  type OnDateClick = (date: Date) => void;

  const onDateClick: OnDateClick = calendarDate => {
    if (
      (value && calendarDate.date.isSame(formatDate(value))) ||
      calendarDate.isDisabled({min, max})
    ) return;

    onChange(
      formatDateValue({
        year,
        month,
        date: calendarDate.date.date(),
      }),
    );
  };

  return (
    <div
      ref={ref}
      className={styles.main}
      data-testid={`${name}-calendar`}>
      <header className={styles.containerYear}>
        <aside
          className={styles.containerIconYearSwitch}
          onClick={onMonthSwitchBack}>
          <CaretLeftFill color="plain" />
        </aside>
        <span className={styles.textYear}>{formatYearMonth({year, month})}</span>
        <aside
          className={styles.containerIconYearSwitch}
          onClick={onMonthSwitchForward}>
          <CaretRightFill color="plain" />
        </aside>
      </header>
      <section className={styles.containerWeekDate}>
        <header className={styles.containerWeekdays}>
          {
            formatWeekdays().map(weekday => (
              <span
                key={weekday}
                className={styles.textWeekday}>
                {weekday}
              </span>
            ))
          }
        </header>
        <section className={styles.containerDates}>
          {
            formatDates({year, month}).map(calendarDate => (
              <span
                key={calendarDate.date.toISOString()}
                className={
                  classnames(
                    styles.textDate,
                    {
                      [styles.currentMonth]: calendarDate.isCurrentMonth,
                      [styles.today]: highlightToday && calendarDate.isToday,
                      [styles.selected]: value && calendarDate.date.isSame(formatDate(value), 'day'),
                      [styles.disabled]: calendarDate.isDisabled({min, max}),
                    },
                  )
                }
                onClick={() => onDateClick(calendarDate)}>
                {calendarDate.text}
              </span>
            ))
          }
        </section>
      </section>
    </div>
  );
});
