import classnames from 'classnames';
import {Search} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {useRef, useState} from 'react';
import {CSSTransition} from 'react-transition-group';
import {
  Input,
  Portal,
  SearchKeywordHistory,
  TextItemList,
} from 'src/components/base';
import type {InputProps} from 'src/components/base/Input';
import type {SearchKeywordHistoryProps} from 'src/components/base/SearchKeywordHistory';
import type {TextItemListProps} from 'src/components/base/TextItemList';
import {IconHeader} from 'src/components/page';
import styles from './styles.module.scss';

export type SearchInputMenuModalProps = {
  name: string;
  open: boolean;
  textList: TextItemListProps['textList'];
  searchKeywords: SearchKeywordHistoryProps['searchKeywords'];
  onOpenChange: (open: boolean) => void;
  onKeywordSearch: (keyword: string) => void | Promise<void>;
  onKeywordCreate: TextItemListProps['onKeywordCreate'];
  onSearchResultClick: TextItemListProps['onTextItemClick'];
  onSearchKeywordRemove: SearchKeywordHistoryProps['onSearchKeywordRemove'];
};

export const SearchInputMenuModal: FC<SearchInputMenuModalProps> = props => {
  const {
    name,
    open,
    textList,
    searchKeywords,
    onOpenChange,
    onKeywordSearch,
    onKeywordCreate,
    onSearchResultClick,
    onSearchKeywordRemove,
  } = props;

  const [keyword, setKeyword] = useState<string>('');

  const modalRef = useRef<HTMLDivElement | null>(null);

  const onInputFocus: InputProps['onFocus'] = () => {
    if (!keyword) return;

    onKeywordSearch(keyword);
  };

  const onInputChange: InputProps['onChange'] = ({target: {value: newKeyword}}) => {
    setKeyword(newKeyword);
    onKeywordSearch(newKeyword);
  };

  const onInnerKeywordCreate: TextItemListProps['onKeywordCreate'] = newKeyword => {
    onKeywordCreate(newKeyword);
    onOpenChange(false);
    setKeyword('');
  };

  const onInnerKeywordItemClick: TextItemListProps['onTextItemClick'] = payload => {
    onSearchResultClick(payload);
    onOpenChange(false);
    setKeyword('');
  };

  const onFloatingMenuInputReset: InputProps['onReset'] = () => {
    setKeyword('');
  };

  const onBackdropClick: MouseEventHandler = e => {
    if (e.target !== e.currentTarget) return;

    onOpenChange(false);
    setKeyword('');
  };

  return (
    <>
      <Portal
        portalId={`${name}-search-input-menu-modal`}
        portalClassName={styles.portalModal}>
        <CSSTransition
          nodeRef={modalRef}
          timeout={100}
          in={open}
          unmountOnExit
          classNames={{
            enter: styles.modalEnter,
            enterActive: styles.modalEnterActive,
            exit: styles.modalExit,
            exitActive: styles.modalExitActive,
          }}>
          <div
            ref={modalRef}
            className={styles.main}
            data-testid={`${name}-search-menu-modal`}
            onClick={onBackdropClick}>
            <div className={styles.body}>
              <div className={styles.containerInputToolset}>
                <span className={styles.containerSearchInput}>
                  <Input
                    name={`${name}-keyword`}
                    fieldName="請輸入查詢關鍵字"
                    defaultValue={keyword}
                    lazyInterval={100}
                    lazy
                    resetable={!!keyword}
                    autoFocus
                    onFocus={onInputFocus}
                    onChange={onInputChange}
                    onReset={onFloatingMenuInputReset} />
                </span>
              </div>
              {
                keyword
                  ? (
                    <div
                      className={styles.containerSearchMenu}
                      data-testid={`${name}-search-input-menu-modal-search-menu-container`}>
                      <IconHeader icon={Search}>搜尋結果</IconHeader>
                      <TextItemList
                        keyword={keyword}
                        textList={textList}
                        className={
                          classnames(
                            styles.listSearchMenu,
                            styles.overide,
                          )
                        }
                        onKeywordCreate={onInnerKeywordCreate}
                        onTextItemClick={onInnerKeywordItemClick} />
                    </div>
                  )
                  : (
                    <SearchKeywordHistory
                      searchKeywords={searchKeywords}
                      dataAttrs={{
                        testid: `${name}-search-input-menu-modal-search-history`,
                      }}
                      onSearchKeywordClick={onInnerKeywordItemClick}
                      onSearchKeywordRemove={onSearchKeywordRemove} />
                  )
              }
            </div>
          </div>
        </CSSTransition>
      </Portal>
    </>
  );
};
