import classnames from 'classnames';
import type {MouseEventHandler, ReactNode} from 'react';
import {forwardRef, useImperativeHandle, useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import {ModalHeader, Portal} from 'src/components/base';
import type {ModalHeaderProps} from 'src/components/base/ModalHeader';
import {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type BackdropModalContainerProps = DataAttrProps & {
  name: string;
  header: string;
  children: ReactNode;
  modalContentClassName?: string;
  open: boolean;
  doneText?: ModalHeaderProps['doneText'];
  doneDisabled?: ModalHeaderProps['doneDisabled'];
  onDoneClick?: ModalHeaderProps['onDoneClick'];
  onOpenChange: (open: boolean) => void;
  onExited?: () => void;
};

export type ModalRef = {
  getBodyElement: () => HTMLDivElement | null;
};

export const BackdropModalContainer = forwardRef<ModalRef, BackdropModalContainerProps>((props, ref) => {
  const {
    name,
    header,
    children,
    modalContentClassName,
    open,
    doneText,
    doneDisabled,
    onDoneClick,
    onOpenChange,
    onExited,
  } = props;

  const modalBackdropRef = useRef<HTMLDivElement | null>(null);
  const modalBody = useRef<HTMLDivElement | null>(null);

  useImperativeHandle<ModalRef | null, ModalRef | null>(ref, () => ({
    getBodyElement() {
      return modalBody.current;
    },
  }));

  const onBackPreviousClick: MouseEventHandler<HTMLDivElement> = () => {
    onOpenChange(false);
  };

  return (
    <Portal portalId={`${name}-modal`}>
      <CSSTransition
        in={open}
        timeout={500}
        nodeRef={modalBackdropRef}
        appear
        unmountOnExit
        classNames={{
          appear: styles.modalAppear,
          appearActive: styles.modalAppearActive,
          appearDone: styles.modalAppearDone,
          enter: styles.modalEnter,
          enterActive: styles.modalEnterActive,
          enterDone: styles.modalEnterDone,
          exit: styles.modalExit,
          exitActive: styles.modalExitActive,
        }}
        onExited={onExited}>
        <div
          ref={modalBackdropRef}
          data-testid={`${name}-modal`}
          className={styles.main}>
          <div className={
            classnames(
              styles.modalContent,
              modalContentClassName,
            )
          }>
            <ModalHeader
              header={header}
              doneText={doneText}
              doneDisabled={doneDisabled}
              onDoneClick={onDoneClick}
              onBackPreviousClick={onBackPreviousClick} />
            <div
              ref={modalBody}
              className={styles.modalBody}>
              {children}
            </div>
          </div>
        </div>
      </CSSTransition>
    </Portal>
  );
});
