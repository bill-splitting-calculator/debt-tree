import type {FC} from 'react';
import {SearchInput} from 'src/components/base';
import type {SearchInputProps} from 'src/components/base/SearchInput';
import {createMember} from 'src/formatters/share-record';
import type {Member} from 'src/types/resources';

type OnMemberClickPayload = {
  member: Member;
  index: number;
};

export type MemberSearchInputProps = Omit<SearchInputProps, 'textList' | 'searchKeywords' | 'onKeywordSearch' | 'onKeywordCreate' | 'onSearchResultClick' | 'onSearchKeywordRemove'> & {
  memberSearchList: SearchInputProps['textList'];
  memberSearchKeywords: SearchInputProps['searchKeywords'];
  onMemberSearch: SearchInputProps['onKeywordSearch'];
  onMemberCreate: (member: Member) => void;
  onMemberClick: (payload: OnMemberClickPayload) => void;
  onMemberSearchKeywordAdd: (keyword: string) => void;
  onMemberSearchKeywordRemove: SearchInputProps['onSearchKeywordRemove'];
};

export const MemberSearchInput: FC<MemberSearchInputProps> = props => {
  const {
    name,
    fieldName,
    defaultValue,
    hasError,
    resetable = true,
    lazy,
    lazyInterval = 100,
    memberSearchList,
    memberSearchKeywords,
    onMemberSearch,
    onMemberCreate,
    onMemberClick,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    ...inputProps
  } = props;

  const onInnerMemberCreate: SearchInputProps['onKeywordCreate'] = keyword => {
    onMemberCreate(createMember(keyword));
    onMemberSearchKeywordAdd(keyword);
  };

  const onMemberItemClick: SearchInputProps['onSearchResultClick'] = ({value, index}) => {
    onMemberClick({
      member: createMember(value),
      index,
    });
    onMemberSearchKeywordAdd(value);
  };

  return (
    <SearchInput
      {...inputProps}
      name={name}
      fieldName={fieldName}
      defaultValue={defaultValue}
      hasError={hasError}
      textList={memberSearchList}
      searchKeywords={memberSearchKeywords}
      lazy={lazy}
      lazyInterval={lazyInterval}
      resetable={resetable}
      onKeywordSearch={onMemberSearch}
      onKeywordCreate={onInnerMemberCreate}
      onSearchResultClick={onMemberItemClick}
      onSearchKeywordRemove={onMemberSearchKeywordRemove} />
  );
};
