import classnames from 'classnames';
import type {FC} from 'react';
import {formatCurrencyAmount} from 'src/formatters/share-record';
import styles from './styles.module.scss';

export type MemberBillItemProps = {
  item: string;
  amount: number;
  currency: string;
};

export const MemberBillItem: FC<MemberBillItemProps> = ({item, amount, currency}) => {
  const displayCurrentAmount = formatCurrencyAmount({amount, currency});

  return (
    <div
      className={styles.main}
      data-testid="member-bill-item">
      <span className={styles.textItem}>{item}</span>
      <span
        className={
          classnames(
            styles.textCurrencyAmount,
            {
              [styles.negative]: displayCurrentAmount.startsWith('-'),
            },
          )
        }>
        {displayCurrentAmount}
      </span>
    </div>
  );
};
