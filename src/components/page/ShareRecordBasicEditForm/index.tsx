import {List} from 'grommet-icons';
import type {ChangeEventHandler, FC} from 'react';
import {useEffect} from 'react';
import {
  DatePicker,
  Input,
  Select,
  ValidationMessage,
} from 'src/components/base';
import type {DatePickerProps} from 'src/components/base/DatePicker';
import type {InputProps} from 'src/components/base/Input';
import {IconHeader, MemberSearchInput} from 'src/components/page';
import type {MemberSearchInputProps} from 'src/components/page/MemberSearchInput';
import {availableCurrencyCodes} from 'src/config/locale';
import {useValidation} from 'src/hooks';
import type {ShareRecord} from 'src/types/resources';
import {editShareRecordBasicSchema} from 'src/validators';
import styles from './styles.module.scss';

type ShareRecordBasic = Pick<ShareRecord, 'name' | 'mainHost' | 'dates' | 'currency'>;

export type ShareRecordBasicEditFormProps = ShareRecordBasic & {
  memberSearchList: MemberSearchInputProps['memberSearchList'];
  memberSearchKeywords: MemberSearchInputProps['memberSearchKeywords'];
  onMemberSearch: MemberSearchInputProps['onMemberSearch'];
  onMemberCreate: MemberSearchInputProps['onMemberCreate'];
  onMemberSearchKeywordAdd: MemberSearchInputProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: MemberSearchInputProps['onMemberSearchKeywordRemove'];
  onSubmit: (data: ShareRecordBasic | null) => void;
};

export const ShareRecordBasicEditForm: FC<ShareRecordBasicEditFormProps> = props => {
  const {
    name,
    mainHost,
    dates,
    currency,
    memberSearchList,
    memberSearchKeywords,
    onMemberSearch,
    onMemberCreate,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onSubmit,
  } = props;

  const {
    isDataChanged,
    data,
    validationError,
    errorDetailMap,
    onDataUpdate,
  } = useValidation(
    {
      name,
      mainHost,
      dates,
      currency,
    },
    editShareRecordBasicSchema,
  );

  useEffect(() => {
    onSubmit(
      (!isDataChanged || !!validationError) ? null : data,
    );
  }, [data, isDataChanged, validationError, onSubmit]);

  const onNameFieldChange: InputProps['onChange'] = (e => {
    onDataUpdate({
      name: e.target.value,
    });
  });

  const onInnerMemberCreate: MemberSearchInputProps['onMemberCreate'] = member => {
    onDataUpdate({
      mainHost: member.name,
    });
    onMemberCreate(member);
  };

  const onMainHostItemClick: MemberSearchInputProps['onMemberClick'] = ({member}) => {
    onDataUpdate({
      mainHost: member.name,
    });
  };

  const onMainHostFieldReset: MemberSearchInputProps['onReset'] = () => {
    onDataUpdate({
      mainHost: '',
    });
  };

  const onBeginDateFieldChange: DatePickerProps['onChange'] = beginDate => {
    onDataUpdate({
      dates: [
        beginDate,
        data.dates[1],
      ],
    });
  };

  const onEndDateFieldChange: DatePickerProps['onChange'] = endDate => {
    onDataUpdate({
      dates: [
        data.dates[0],
        endDate,
      ],
    });
  };

  const onCurrencyFieldChange: ChangeEventHandler<HTMLSelectElement> = e => {
    onDataUpdate({
      currency: e.target.value,
    });
  };

  return (
    <div className={styles.main}>
      <IconHeader icon={List}>基本設定</IconHeader>
      <div className={styles.containerShareRecordFields}>
        <div>
          <Input
            name="share-record-name"
            fieldName="名稱"
            hasError={!!errorDetailMap.name}
            defaultValue={data.name}
            lazy
            lazyInterval={100}
            onChange={onNameFieldChange} />
          <ValidationMessage message={errorDetailMap.name} />
        </div>
        <div>
          <MemberSearchInput
            name="share-record-main-host"
            fieldName="主要付款人"
            hasError={!!errorDetailMap.mainHost}
            defaultValue={data.mainHost}
            memberSearchList={memberSearchList}
            memberSearchKeywords={memberSearchKeywords}
            resetable
            onMemberSearch={onMemberSearch}
            onMemberCreate={onInnerMemberCreate}
            onMemberClick={onMainHostItemClick}
            onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
            onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
            onReset={onMainHostFieldReset} />
          <ValidationMessage message={errorDetailMap.mainHost} />
        </div>
        <div className={styles.containerDates}>
          <div className={styles.containerDate}>
            <DatePicker
              name="share-record-begin-date"
              fieldName="開始日期"
              hasError={!!errorDetailMap['dates.0']}
              value={data.dates[0]}
              max={data.dates[1]}
              onChange={onBeginDateFieldChange} />
            <ValidationMessage message={errorDetailMap['dates.0']} />
          </div>
          <div className={styles.containerDate}>
            <DatePicker
              name="share-record-end-date"
              fieldName="結束日期（選填）"
              hasError={!!errorDetailMap['dates.1']}
              value={data.dates[1]}
              min={data.dates[0]}
              onChange={onEndDateFieldChange} />
            <ValidationMessage message={errorDetailMap['dates.1']} />
          </div>
        </div>
        <div className={styles.containerCurrency}>
          <Select
            name="share-record-currency"
            value={data.currency ?? ''}
            hasError={!!errorDetailMap.currency}
            placeholder="使用貨幣"
            onChange={onCurrencyFieldChange}>
            {
              availableCurrencyCodes.map(code => (
                <option
                  key={code}
                  value={code}>
                  {code}
                </option>
              ))
            }
          </Select>
          <ValidationMessage message={errorDetailMap.currency} />
        </div>
      </div>
    </div>
  );
};

export default ShareRecordBasicEditForm;
