import type {Icon} from 'grommet-icons/icons';
import type {FC, ReactNode} from 'react';
import styles from './styles.module.scss';

export type IconHeaderProps = {
  icon: Icon;
  children: ReactNode;
};

export const IconHeader: FC<IconHeaderProps> = ({icon: HeaderIcon, children}) => (
  <header className={styles.main}>
    <HeaderIcon color="plain" />
    <span className={styles.text}>{children}</span>
  </header>
);
