import {UserManager} from 'grommet-icons';
import type {FC, ReactElement} from 'react';
import type {MemberBillListProps} from 'src/components/page/MemberBillList';
import type {Bill} from 'src/types/resources';
import styles from './styles.module.scss';

export type HostBillFrameProps = {
  host: Bill['host'];
  children: ReactElement<MemberBillListProps>
};

export const HostBillFrame: FC<HostBillFrameProps> = ({host, children}) => (
  <fieldset
    className={styles.main}
    data-testid="host-bill-frame">
    <legend
      className={styles.containerHost}
      data-testid="host-bill-frame-legend">
      <span>應支付{' '}</span>
      <span className={styles.containerIconTextHost}>
        <UserManager color="plain" />
        <span>{host}</span>
      </span>
      <span>{' '}帳款</span>
    </legend>
    {children}
  </fieldset>
);
