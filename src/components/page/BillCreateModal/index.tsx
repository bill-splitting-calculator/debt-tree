import type {FC} from 'react';
import {useRef, useState} from 'react';
import {BackdropModalContainer} from 'src/components/base';
import type {BackdropModalContainerProps, ModalRef} from 'src/components/base/BackdropModalContainer';
import {BillEditForm} from 'src/components/page';
import type {BillEditFormProps} from 'src/components/page/BillEditForm';
import {createDefaultPayment, createMember} from 'src/formatters/share-record';
import {useAfterRenderHandler} from 'src/hooks';
import type {Bill, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type BillCreateModalProps = {
  currency: BillEditFormProps['currency'];
  mainHost?: ShareRecord['mainHost'];
  memberSearchList: BillEditFormProps['memberSearchList'];
  memberSearchKeywords: BillEditFormProps['memberSearchKeywords'];
  open: BackdropModalContainerProps['open'];
  doneText?: BackdropModalContainerProps['doneText'];
  onOpenChange: BackdropModalContainerProps['onOpenChange'];
  onExited?: BackdropModalContainerProps['onExited'];
  onMemberSearch: BillEditFormProps['onMemberSearch'];
  onMemberCreate: BillEditFormProps['onMemberCreate'];
  onMemberSearchKeywordAdd: BillEditFormProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: BillEditFormProps['onMemberSearchKeywordRemove'];
  onSubmit: (bill: Bill) => void;
};

export const BillCreateModal: FC<BillCreateModalProps> = props => {
  const {
    currency,
    mainHost = '',
    memberSearchList,
    memberSearchKeywords,
    open,
    doneText,
    onOpenChange,
    onMemberSearch,
    onMemberCreate,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onSubmit,
    onExited,
  } = props;

  const modalRef = useRef<ModalRef | null>(null);
  const [billCreateData, setBillCreateData] = useState<Bill | null>(null);

  const {addHandlerOnce} = useAfterRenderHandler();

  const onMemberClick: BillEditFormProps['onMemberClick'] = () => {
    addHandlerOnce(() => {
      const modalBody = modalRef.current?.getBodyElement();

      if (!modalBody) return;

      modalBody.scrollTo(0, modalBody.scrollHeight);
    }, 100);
  };

  const onModalDoneClick: BackdropModalContainerProps['onDoneClick'] = () => {
    if (!billCreateData) return;

    onSubmit(billCreateData);
    onOpenChange(false);
  };

  const onInnerExited: BackdropModalContainerProps['onExited'] = () => {
    setBillCreateData(null);
    onExited?.();
  };

  return (
    <BackdropModalContainer
      ref={modalRef}
      name="bill-create"
      header="新增款項設定"
      open={open}
      doneText={doneText}
      doneDisabled={!billCreateData}
      onDoneClick={onModalDoneClick}
      onOpenChange={onOpenChange}
      onExited={onInnerExited}>
      <div className={styles.main}>
        <BillEditForm
          bill={{
            payments: [
              createDefaultPayment(),
            ],
            host: mainHost,
            members: mainHost
              ? [createMember(mainHost)]
              : [],
          }}
          currency={currency}
          memberSearchList={memberSearchList}
          memberSearchKeywords={memberSearchKeywords}
          onMemberSearch={onMemberSearch}
          onMemberCreate={onMemberCreate}
          onMemberClick={onMemberClick}
          onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
          onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
          onSubmit={setBillCreateData} />
      </div>
    </BackdropModalContainer>
  );
};
