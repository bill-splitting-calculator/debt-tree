import {Calendar, Group, Trash} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {formatBillTotal, formatDates, formatMemberCount} from 'src/formatters/share-record';
import type {ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type ShareRecordCardProps = {
  shareRecord: ShareRecord;
  onEdit: () => void;
  onRemove: () => void;
};

export const ShareRecordCard: FC<ShareRecordCardProps> = props => {
  const {
    shareRecord: {
      name,
      dates,
      bills,
      currency,
    },
    onEdit,
    onRemove,
  } = props;

  const onRemoveBtnClick: MouseEventHandler<SVGElement> = e => {
    e.stopPropagation();

    onRemove();
  };

  return (
    <div
      className={styles.main}
      data-testid="share-record-card"
      onClick={onEdit}>
      <header className={styles.header}>
        <aside
          className={styles.iconTextInfo}
          data-testid="share-record-card-dates-container">
          <Calendar color="plain" />
          <span>{formatDates(dates)}</span>
        </aside>
        <aside
          className={styles.iconToolset}
          data-testid="share-record-card-remove-icon-container">
          <Trash
            color="plain"
            onClick={onRemoveBtnClick} />
        </aside>
      </header>
      <section className={styles.body}>
        <span
          className={styles.textName}
          data-testid="share-record-card-name">{name}</span>
      </section>
      <footer className={styles.footer}>
        <aside
          className={styles.iconTextInfo}
          data-testid="share-record-card-member-count-container">
          <Group color="plain" />
          <span>{formatMemberCount(bills)}</span>
        </aside>
        <aside
          data-testid="share-record-card-bill-total-container">
          <span className={styles.textBillTotal}>
            {formatBillTotal({currency, bills})}
          </span>
        </aside>
      </footer>
    </div>
  );
};
