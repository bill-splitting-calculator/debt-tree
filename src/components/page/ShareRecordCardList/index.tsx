import type {FC, ReactElement} from 'react';
import {ClickActiveHighlight} from 'src/components/base/ClickActiveHighlight';
import type {ShareRecordCardProps} from 'src/components/page/ShareRecordCard';
import styles from './styles.module.scss';

export type ShareRecordCardListProps = {
  children: ReactElement<ShareRecordCardProps> | ReactElement<ShareRecordCardProps>[];
};

export const ShareRecordCardList: FC<ShareRecordCardListProps> = props => {
  const {children} = props;
  const displayChildren = Array.isArray(children) ? children : [children];

  return (
    <ul
      className={styles.main}
      data-testid="share-record-card-list">
      {
        displayChildren.map((child, idx) => (
          <li
            key={idx}
            className={styles.containerShareRecordCard}>
            <ClickActiveHighlight>
              {child}
            </ClickActiveHighlight>
          </li>
        ))
      }
    </ul>
  );
};
