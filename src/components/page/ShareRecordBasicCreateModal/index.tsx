import type {FC} from 'react';
import {useState} from 'react';
import {BackdropModalContainer} from 'src/components/base';
import type {BackdropModalContainerProps} from 'src/components/base/BackdropModalContainer';
import {ShareRecordBasicEditForm} from 'src/components/page';
import type {ShareRecordBasicEditFormProps} from 'src/components/page/ShareRecordBasicEditForm';
import type {ShareRecord} from 'src/types/resources';

type ShareRecordBasic = Pick<ShareRecord, 'name' | 'dates' | 'currency'>;

export type ShareRecordBasicCreateModalProps = {
  open: BackdropModalContainerProps['open'];
  memberSearchList: ShareRecordBasicEditFormProps['memberSearchList'];
  memberSearchKeywords: ShareRecordBasicEditFormProps['memberSearchKeywords'];
  onOpenChange: BackdropModalContainerProps['onOpenChange'];
  onMemberSearch: ShareRecordBasicEditFormProps['onMemberSearch'];
  onMemberCreate: ShareRecordBasicEditFormProps['onMemberCreate'];
  onMemberSearchKeywordAdd: ShareRecordBasicEditFormProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: ShareRecordBasicEditFormProps['onMemberSearchKeywordRemove'];
  onSubmit: (shareRecordBasic: ShareRecordBasic) => void;
};

export const ShareRecordBasicCreateModal: FC<ShareRecordBasicCreateModalProps> = props => {
  const {
    open,
    memberSearchList,
    memberSearchKeywords,
    onOpenChange,
    onMemberSearch,
    onMemberCreate,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onSubmit,
  } = props;

  const [shareRecordBasic, setShareRecordBasic] = useState<ShareRecordBasic | null>(null);

  const onModalDoneClick: BackdropModalContainerProps['onDoneClick'] = () => {
    if (!shareRecordBasic) return;

    onSubmit(shareRecordBasic);
    onOpenChange(false);
  };

  const onModalExit: BackdropModalContainerProps['onExited'] = () => {
    setShareRecordBasic(null);
  };

  return (
    <BackdropModalContainer
      name="share-record-basic-create"
      header="新增分攤紀錄"
      open={open}
      doneDisabled={!shareRecordBasic}
      dataAttrs={{
        testid: 'share-record-basic-create-modal',
      }}
      onDoneClick={onModalDoneClick}
      onOpenChange={onOpenChange}
      onExited={onModalExit}>
      <div data-testid="share-record-basic-create-modal-body">
        <ShareRecordBasicEditForm
          name=""
          mainHost=""
          dates={['', '']}
          currency="TWD"
          memberSearchList={memberSearchList}
          memberSearchKeywords={memberSearchKeywords}
          onMemberSearch={onMemberSearch}
          onMemberCreate={onMemberCreate}
          onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
          onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
          onSubmit={setShareRecordBasic} />
      </div>
    </BackdropModalContainer>
  );
};
