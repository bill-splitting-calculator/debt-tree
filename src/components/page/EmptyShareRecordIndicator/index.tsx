import classnames from 'classnames';
import type {FC} from 'react';
import styles from './styles.module.scss';

export const EmptyShareRecordIndicator: FC = () => (
  <div
    className={styles.main}
    data-testid="empty-share-record-indicator">
    <div className={classnames(styles.containerTextGreeting, styles.top)}>
      <div
        className={styles.textGreeting}
        data-testid="empty-share-record-indicator-greeting">嘿喲嘿喲</div>
    </div>
    <div className={classnames(styles.containerTextGreeting, styles.bottom)}>
      <div
        className={styles.textGreeting}
        data-testid="empty-share-record-indicator-greeting">嘿喲嘿喲</div>
    </div>
    <div className={styles.textEmoticon}>
      (๑<span className={styles.textEmoticonEye}>•</span>ω<span className={styles.textEmoticonEye}>•</span>)<span className={styles.textArm}>ノ</span>
    </div>
  </div>
);
