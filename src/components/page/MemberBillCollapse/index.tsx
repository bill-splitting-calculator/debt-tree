import {Decimal} from 'decimal.js';
import {User} from 'grommet-icons';
import type {FC} from 'react';
import {useMemo} from 'react';
import {Checkbox, Collapse} from 'src/components/base';
import type {CheckboxProps} from 'src/components/base/Checkbox';
import type {CollapseProps} from 'src/components/base/Collapse';
import {HostBillFrame, MemberBillList} from 'src/components/page';
import {createBill, formatPaymentItemAmount} from 'src/formatters/share-record';
import {toKeyList} from 'src/libs/array';
import type {Member, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type MemberBillCollapseProps = {
  checked: boolean;
  defaultCollapsed?: CollapseProps['defaultCollapsed'];
  member: Member;
  currency: ShareRecord['currency'];
  bills: ShareRecord['bills'];
  onCheckChange: (value: boolean) => void;
};

export const MemberBillCollapse: FC<MemberBillCollapseProps> = props => {
  const {
    checked,
    defaultCollapsed,
    member,
    member: {
      name: memberName,
    },
    currency,
    bills,
    onCheckChange,
  } = props;

  const hostShareMapEntries = useMemo(
    () => {
      const memberBillsMap = toKeyList(bills, 'host');

      const {
        [memberName]: hostBills = [],
        ...otherMemberBillsMap
      } = memberBillsMap;

      const hostBillsMap = hostBills.reduce((carry, bill) => {
        const {
          payments,
          host,
          members,
        } = bill;
        const {length: memberLen} = members;

        return members.reduce((subCarry, {name: otherMemberName}) => {
          if (otherMemberName === memberName) return subCarry;

          if (!subCarry[otherMemberName]) {
            subCarry[otherMemberName] = [];
          }

          subCarry[otherMemberName].push(
            createBill({
              payments: payments.map(payment => ({
                ...payment,
                amount: new Decimal(payment.amount)
                  .dividedBy(memberLen)
                  .times(-1)
                  .toNumber(),
              })),
              host,
              members: [member],
            }),
          );

          return subCarry;
        }, carry);
      }, otherMemberBillsMap);

      return Object.entries(hostBillsMap)
        .filter(([host]) => host !== memberName)
        .reduce<[string, ShareRecord['bills']][]>((carry, [host, memberBills]) => {
        const filteredMemberBills = memberBills.filter(({members: billMembers}) => (
          billMembers.some(({name}) => name === memberName)
        ));

        if (!filteredMemberBills.length) return carry;

        carry.push([host, filteredMemberBills]);

        return carry;
      }, []);
    },
    [bills, member, memberName],
  );

  const onCheckboxChange: CheckboxProps['onChange'] = e => {
    onCheckChange(e.target.checked);
  };

  const onCheckboxClick: CheckboxProps['onClick'] = e => {
    e.stopPropagation();
  };

  if (!hostShareMapEntries.length) return null;

  const hostBillsPaymentAmount = hostShareMapEntries.map(([, hostBills]) => {
    const paymentItemAmount = formatPaymentItemAmount(hostBills);
    const totalAmount = paymentItemAmount.reduce(
      (carry, {amount}) => carry.add(new Decimal(amount)),
      new Decimal(0),
    )
      .toNumber();

    return {
      paymentItemAmount,
      totalAmount,
    };
  });

  if (!hostBillsPaymentAmount.filter(({totalAmount}) => totalAmount > 0).length) return null;

  return (
    <Collapse
      defaultCollapsed={defaultCollapsed}
      headerChildren={
        <div className={styles.containerCheckboxMember}>
          <Checkbox
            name="member-bill-collapse"
            checked={checked}
            onClick={onCheckboxClick}
            onChange={onCheckboxChange} />
          <div
            className={styles.containerIconTextUser}
            data-testid="member-bill-collapse-member-name-container">
            <User color="plain" />
            <span>{memberName}</span>
          </div>
        </div>
      }
      dataAttrs={{
        testid: 'member-bill-collapse',
      }}>
      <div className={styles.containerHostBills}>
        {
          hostShareMapEntries.map(([host, memberBills], idx) => {
            if (!memberBills.length) return (
              <span className={styles.textEmptyBill}>無帳款</span>
            );

            const {paymentItemAmount, totalAmount} = hostBillsPaymentAmount[idx];

            if (totalAmount <= 0) return null;

            return (
              <HostBillFrame
                key={host}
                host={host}>
                <MemberBillList
                  paymentItemAmount={paymentItemAmount}
                  totalAmount={totalAmount}
                  currency={currency}/>
              </HostBillFrame>
            );
          })
        }
      </div>
    </Collapse>
  );
};
