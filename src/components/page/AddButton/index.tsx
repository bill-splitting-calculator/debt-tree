import classnames from 'classnames';
import {Add} from 'grommet-icons';
import type {FC} from 'react';
import {Button} from 'src/components/base';
import type {ButtonProps} from 'src/components/base/Button';
import type {DataAttrProps} from 'src/types/components';
import styles from './styles.module.scss';

export type AddButtonProps = ButtonProps & DataAttrProps &  {
  variant?: never;
  disabled?: never;
};

export const AddButton: FC<AddButtonProps> = ({className, ...otherProps}) => (
  <Button
    {...otherProps}
    className={
      classnames(
        styles.main,
        styles.overide,
        className,
      )
    }
    variant="primary">
    <Add color="plain" />
  </Button>
);
