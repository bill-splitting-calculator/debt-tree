import type {FC} from 'react';
import {useEffect, useState} from 'react';
import {FullScaledModalContainer} from 'src/components/base';
import type {FullScaledModalContainerProps} from 'src/components/base/FullScaledModalContainer';
import {
  BillCreateModal,
  BillEditModal,
  BillFieldSet,
  MemberBillFieldSet,
  ShareRecordBasicEditForm,
} from 'src/components/page';
import type {BillCreateModalProps} from 'src/components/page/BillCreateModal';
import type {BillEditModalProps} from 'src/components/page/BillEditModal';
import type {BillFieldSetProps} from 'src/components/page/BillFieldSet';
import type {ShareRecordBasicEditFormProps} from 'src/components/page/ShareRecordBasicEditForm';
import type {ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

type ShareRecordBasic = Pick<ShareRecord, 'name' | 'mainHost' | 'dates' | 'currency'>;

export type ShareRecordEditModalProps = {
  shareRecord: ShareRecord;
  open: FullScaledModalContainerProps['open'];
  memberSearchList: ShareRecordBasicEditFormProps['memberSearchList'];
  memberSearchKeywords: ShareRecordBasicEditFormProps['memberSearchKeywords'];
  onMemberSearch: ShareRecordBasicEditFormProps['onMemberSearch'];
  onMemberCreate: ShareRecordBasicEditFormProps['onMemberCreate'];
  onMemberSearchKeywordAdd: ShareRecordBasicEditFormProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: ShareRecordBasicEditFormProps['onMemberSearchKeywordRemove'];
  onOpenChange: FullScaledModalContainerProps['onOpenChange'];
  onExited: FullScaledModalContainerProps['onExited'];
  onShareRecordUpdate: (data: Partial<ShareRecord>) => void;
};

export const ShareRecordEditModal: FC<ShareRecordEditModalProps> = props => {
  const {
    shareRecord,
    shareRecord: {
      name,
      mainHost,
      dates,
      currency,
      bills,
    },
    open,
    memberSearchList,
    memberSearchKeywords,
    onMemberSearch,
    onMemberCreate,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onShareRecordUpdate,
    onOpenChange,
  } = props;

  const [shareRecordBasic, onShareRecordBasicChange] = useState<ShareRecordBasic | null>(null);
  const [innerBills, onInnerBillsChange] = useState<ShareRecord['bills'] | null>(null);
  const [activeBillIdx, onActiveBillIdxChange] = useState<number | undefined>();
  const [isBillCreateModalOpen, onIsBillCreateModalOpenChange] = useState<boolean>(false);
  const [isBillEditModalOpen, onIsBillEditModalOpenChange] = useState<boolean>(false);

  const activeBills = innerBills ?? bills;
  const activeBill = activeBillIdx !== undefined ? activeBills[activeBillIdx] : undefined;

  useEffect(
    () => {
      onShareRecordBasicChange(null);
      onInnerBillsChange(null);
    },
    [
      shareRecord,
      bills,
      onShareRecordBasicChange,
      onInnerBillsChange,
    ],
  );

  const isModalDoneDisabled = !shareRecordBasic && !innerBills;

  const onModalDoneClick: FullScaledModalContainerProps['onDoneClick'] = () => {
    onShareRecordUpdate({
      ...shareRecordBasic,
      bills: innerBills ?? bills,
    });
  };

  const onBillEdit: BillFieldSetProps['onBillEdit'] = editBillIdx => {
    onActiveBillIdxChange(editBillIdx);
    onIsBillEditModalOpenChange(true);
  };

  const onBillRemove: BillFieldSetProps['onBillRemove'] = removeBillIdx => {
    onInnerBillsChange(
      activeBills.filter((_bill, billIdx) => billIdx !== removeBillIdx),
    );
  };

  const onBillAdd: BillFieldSetProps['onBillAdd'] = () => {
    onIsBillCreateModalOpenChange(true);
  };

  const onCreateBillSubmit: BillCreateModalProps['onSubmit'] = bill => {
    onInnerBillsChange([
      ...activeBills,
      bill,
    ]);
  };

  const onEditBillSubmit: BillEditModalProps['onSubmit'] = bill => {
    if (activeBillIdx === undefined) throw new TypeError('Invalid bill index');

    const newBills = [...activeBills];

    newBills[activeBillIdx] = bill;

    onInnerBillsChange(newBills);
  };

  const onBillCreateModalExited: BillCreateModalProps['onExited'] = () => {
    onActiveBillIdxChange(undefined);
  };

  const onBillEditModalExited: BillEditModalProps['onExited'] = () => {
    onActiveBillIdxChange(undefined);
  };

  return (
    <>
      <FullScaledModalContainer
        name="share-record-edit"
        header="分攤設定 / 紀錄"
        open={open}
        doneDisabled={isModalDoneDisabled}
        onDoneClick={onModalDoneClick}
        onOpenChange={onOpenChange}>
        <div className={styles.main}>
          <div>
            <ShareRecordBasicEditForm
              name={name}
              dates={dates}
              mainHost={mainHost}
              currency={currency}
              memberSearchList={memberSearchList}
              memberSearchKeywords={memberSearchKeywords}
              onMemberSearch={onMemberSearch}
              onMemberCreate={onMemberCreate}
              onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
              onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
              onSubmit={onShareRecordBasicChange} />
          </div>
          <div>
            <BillFieldSet
              currency={currency}
              bills={activeBills}
              onBillEdit={onBillEdit}
              onBillRemove={onBillRemove}
              onBillAdd={onBillAdd} />
          </div>
          {
            activeBills.length
              ? (
                <div>
                  <MemberBillFieldSet
                    currency={currency}
                    bills={activeBills} />
                </div>
              )
              : null
          }
        </div>
      </FullScaledModalContainer>
      <BillCreateModal
        currency={currency}
        mainHost={mainHost}
        open={isBillCreateModalOpen}
        memberSearchList={memberSearchList}
        memberSearchKeywords={memberSearchKeywords}
        onOpenChange={onIsBillCreateModalOpenChange}
        onMemberSearch={onMemberSearch}
        onMemberCreate={onMemberCreate}
        onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
        onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
        onSubmit={onCreateBillSubmit}
        onExited={onBillCreateModalExited} />
      <BillEditModal
        bill={activeBill}
        currency={currency}
        memberSearchList={memberSearchList}
        memberSearchKeywords={memberSearchKeywords}
        open={isBillEditModalOpen}
        onOpenChange={onIsBillEditModalOpenChange}
        onMemberSearch={onMemberSearch}
        onMemberCreate={onMemberCreate}
        onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
        onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
        onSubmit={onEditBillSubmit}
        onExited={onBillEditModalExited} />
    </>
  );
};
