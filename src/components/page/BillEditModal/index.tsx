import type {FC} from 'react';
import {useRef, useState} from 'react';
import {BackdropModalContainer} from 'src/components/base';
import type {BackdropModalContainerProps, ModalRef} from 'src/components/base/BackdropModalContainer';
import {BillEditForm} from 'src/components/page';
import type {BillEditFormProps} from 'src/components/page/BillEditForm';
import {useAfterRenderHandler} from 'src/hooks';
import type {Bill} from 'src/types/resources';
import styles from './styles.module.scss';

export type BillEditModalProps = {
  currency: BillEditFormProps['currency'];
  bill?: Bill;
  memberSearchList: BillEditFormProps['memberSearchList'];
  memberSearchKeywords: BillEditFormProps['memberSearchKeywords'];
  open: BackdropModalContainerProps['open'];
  onOpenChange: BackdropModalContainerProps['onOpenChange'];
  onExited?: BackdropModalContainerProps['onExited'];
  onMemberSearch: BillEditFormProps['onMemberSearch'];
  onMemberCreate: BillEditFormProps['onMemberCreate'];
  onMemberSearchKeywordAdd: BillEditFormProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: BillEditFormProps['onMemberSearchKeywordRemove'];
  onSubmit: (bill: Bill) => void;
};

export const BillEditModal: FC<BillEditModalProps> = props => {
  const {
    currency,
    bill,
    memberSearchList,
    memberSearchKeywords,
    open,
    onOpenChange,
    onExited,
    onMemberSearch,
    onMemberCreate,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onSubmit,
  } = props;

  const [billEditData, setBillEditData] = useState<Bill | null>(null);
  const modalRef = useRef<ModalRef | null>(null);
  const {addHandlerOnce} = useAfterRenderHandler();

  const onMemberClick: BillEditFormProps['onMemberClick'] = () => {
    addHandlerOnce(() => {
      const modalBody = modalRef.current?.getBodyElement();

      if (!modalBody) return;

      modalBody.scrollTo(0, modalBody.scrollHeight);
    }, 100);
  };

  const onModalDoneClick: BackdropModalContainerProps['onDoneClick'] = () => {
    if (!billEditData) return;

    onSubmit(billEditData);
    onOpenChange(false);
    setBillEditData(null);
  };

  return (
    <BackdropModalContainer
      ref={modalRef}
      name="bill-edit"
      header="編輯款項設定"
      open={open}
      doneDisabled={!billEditData}
      onDoneClick={onModalDoneClick}
      onOpenChange={onOpenChange}
      onExited={onExited}>
      <div className={styles.main}>
        {
          bill && (
            <BillEditForm
              bill={bill}
              currency={currency}
              memberSearchList={memberSearchList}
              memberSearchKeywords={memberSearchKeywords}
              onMemberSearch={onMemberSearch}
              onMemberCreate={onMemberCreate}
              onMemberClick={onMemberClick}
              onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
              onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
              onSubmit={setBillEditData} />
          )
        }
      </div>
    </BackdropModalContainer>
  );
};
