import type {FC} from 'react';
import {MemberSearchInput, MemberTag} from 'src/components/page';
import type {MemberSearchInputProps} from 'src/components/page/MemberSearchInput';
import {createMember} from 'src/formatters/share-record';
import type {Member} from 'src/types/resources';
import styles from './styles.module.scss';

type Members = Member[];

export type MemberPickerProps = {
  fieldName: MemberSearchInputProps['fieldName'];
  hasError: MemberSearchInputProps['hasError'];
  resetable?: MemberSearchInputProps['resetable'];
  members: Members;
  memberSearchList: MemberSearchInputProps['memberSearchList'];
  memberSearchKeywords: MemberSearchInputProps['memberSearchKeywords'];
  onReset?: MemberSearchInputProps['onReset'];
  onMemberSearch: MemberSearchInputProps['onMemberSearch'];
  onMemberClick: MemberSearchInputProps['onMemberClick'];
  onMemberSearchKeywordAdd: MemberSearchInputProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: MemberSearchInputProps['onMemberSearchKeywordRemove'];
  onMemberCreate: (member: Member) => void;
  onMembersChange: (members: Members) => void;
};

type OnMemberItemRemove = (idx: number) => void;
type OnMemberItemAdd = (name: string) => void;

export const MemberPicker: FC<MemberPickerProps> = props => {
  const {
    fieldName,
    hasError,
    resetable,
    members,
    memberSearchList,
    memberSearchKeywords,
    onMemberSearch,
    onMemberCreate,
    onMembersChange,
    onMemberClick,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onReset,
  } = props;

  const onMemberItemRemove: OnMemberItemRemove = idx => {
    const newMembers = [...members];

    newMembers.splice(idx, 1);

    onMembersChange(newMembers);
  };

  const onMemberItemAdd: OnMemberItemAdd = name => {
    if (members.find(member => member.name === name)) return;

    const newMembers = [
      ...members,
      createMember(name),
    ];

    onMembersChange(newMembers);
  };

  const onInnerMemberClick: MemberSearchInputProps['onMemberClick'] = payload => {
    onMemberItemAdd(payload.member.name);
    onMemberClick(payload);
  };

  const onInnerMemberCreate: MemberSearchInputProps['onMemberCreate'] = member => {
    onMemberItemAdd(member.name);
    onMemberCreate(member);
  };

  return (
    <div
      className={styles.main}
      data-testid="member-picker">
      <div>
        <MemberSearchInput
          name="member-picker"
          fieldName={fieldName}
          hasError={hasError}
          resetable={resetable}
          memberSearchList={memberSearchList}
          memberSearchKeywords={memberSearchKeywords}
          onMemberSearch={onMemberSearch}
          onMemberCreate={onInnerMemberCreate}
          onMemberClick={onInnerMemberClick}
          onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
          onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
          onReset={onReset} />
      </div>
      {
        members.length
          ? (
            <div>
              <ul
                className={styles.listMemberTag}
                data-testid="member-tag-list">
                {
                  members.map((member, idx) => (
                    <li key={idx}>
                      <MemberTag
                        member={member}
                        onRemove={() => onMemberItemRemove(idx)} />
                    </li>
                  ))
                }
              </ul>
            </div>
          )
          : null
      }
    </div>
  );
};
