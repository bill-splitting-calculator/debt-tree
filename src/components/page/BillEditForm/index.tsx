import {Currency, UserAdd} from 'grommet-icons';
import type {ChangeEventHandler, FC, MouseEventHandler} from 'react';
import {useEffect} from 'react';
import {
  Checkbox,
  Input,
  InputGroup,
  LinkText,
  Select,
  ValidationMessage,
} from 'src/components/base';
import type {CheckboxProps} from 'src/components/base/Checkbox';
import type {InputProps} from 'src/components/base/Input';
import {IconHeader, MemberPicker, MemberSearchInput} from 'src/components/page';
import type {MemberPickerProps} from 'src/components/page/MemberPicker';
import type {MemberSearchInputProps} from 'src/components/page/MemberSearchInput';
import {availableCurrencyCodes} from 'src/config/locale';
import {createDefaultPayment} from 'src/formatters/share-record';
import {useValidation} from 'src/hooks';
import {parseFloat} from 'src/libs/number';
import type {Bill, Payment, ShareRecord} from 'src/types/resources';
import {editBillSchema} from 'src/validators';
import styles from './styles.module.scss';

export type BillEditFormProps = {
  bill: Bill;
  currency: ShareRecord['currency'];
  memberSearchList: MemberPickerProps['memberSearchList'];
  memberSearchKeywords: MemberPickerProps['memberSearchKeywords'];
  onMemberSearch: MemberPickerProps['onMemberSearch'];
  onMemberCreate: MemberSearchInputProps['onMemberCreate'] | MemberPickerProps['onMemberCreate'];
  onMemberClick: MemberSearchInputProps['onMemberClick'];
  onMemberSearchKeywordAdd: MemberSearchInputProps['onMemberSearchKeywordAdd'];
  onMemberSearchKeywordRemove: MemberSearchInputProps['onMemberSearchKeywordRemove'];
  onSubmit: (bill: Bill | null) => void;
};

export const BillEditForm: FC<BillEditFormProps> = props => {
  const {
    bill,
    currency,
    memberSearchList,
    memberSearchKeywords,
    onMemberSearch,
    onMemberCreate,
    onMemberClick,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
    onSubmit,
  } = props;

  const {
    data,
    isDataChanged,
    validationError,
    errorDetailMap,
    onDataUpdate,
  } = useValidation(
    bill,
    editBillSchema,
  );

  useEffect(() => {
    onSubmit(
      (!isDataChanged || !!validationError) ? null : data,
    );
  }, [data, isDataChanged, validationError, onSubmit]);

  const onPaymentItemFieldChange: (index: number) => InputProps['onChange'] = index => e => {
    const newPayments = [...data.payments];

    newPayments.splice(index, 1, {
      ...newPayments[index],
      item: e.target.value,
    });

    onDataUpdate({
      payments: newPayments,
    });
  };

  const onPaymentAmountFieldChange: (index: number) => InputProps['onChange'] = index => e => {
    const newPayments = [...data.payments];

    newPayments.splice(index, 1, {
      ...newPayments[index],
      amount: parseFloat(e.target.value) ?? '',
    });

    onDataUpdate({
      payments: newPayments,
    });
  };

  type OnPaymentAmountRelatedFields = Partial<Pick<Payment, 'useCurrencyExchange' | 'sourceCurrency' | 'exchangeRate'>>;
  type OnPaymentAmountRelatedFieldsChange = (index: number) => (payload: OnPaymentAmountRelatedFields) => void;

  const onPaymentAmountRelatedFieldsChange: OnPaymentAmountRelatedFieldsChange = (index: number) => payload => {
    const newPayments = [...data.payments];
    const {[index]: currentPayment} = newPayments;

    if (payload.useCurrencyExchange ?? currentPayment.useCurrencyExchange) {
      newPayments.splice(index, 1, {
        ...newPayments[index],
        useCurrencyExchange: true,
        exchangeRate: payload.exchangeRate ?? currentPayment.exchangeRate ?? '',
        sourceCurrency: payload.sourceCurrency ?? currentPayment.sourceCurrency ?? '',
      });
    } else {
      newPayments.splice(index, 1, {
        ...newPayments[index],
        useCurrencyExchange: false,
        exchangeRate: undefined,
        sourceCurrency: undefined,
      });
    }

    onDataUpdate({
      payments: newPayments,
    });
  };

  const onPaymentSourceCurrencyFieldChange: (index: number) => ChangeEventHandler<HTMLSelectElement> = index => e => {
    onPaymentAmountRelatedFieldsChange(index)({
      sourceCurrency: e.target.value,
    });
  };

  const onPaymentExchangeRateFieldChange: (index: number) => InputProps['onChange'] = index => e => {
    onPaymentAmountRelatedFieldsChange(index)({
      exchangeRate: parseFloat(e.target.value),
    });
  };

  const onPaymentUseCurrencyExchangeFieldChange: (index: number) => CheckboxProps['onChange'] = index => e => {
    onPaymentAmountRelatedFieldsChange(index)({
      useCurrencyExchange: e.target.checked,
    });
  };

  const onPaymentRemove: (index: number) => MouseEventHandler = index => () => {
    onDataUpdate({
      payments: [...data.payments].filter((_payment, idx) => idx !== index),
    });
  };

  const onPaymentCreate: MouseEventHandler = () => {
    onDataUpdate({
      payments: [
        ...data.payments,
        createDefaultPayment(),
      ],
    });
  };

  const onHostMemberChange: MemberSearchInputProps['onMemberClick'] = ({member: {name: host}}) => {
    onDataUpdate({host});
  };

  const onHostFieldReset: MemberPickerProps['onReset'] = () => {
    onDataUpdate({
      host: '',
    });
  };

  const onHostMemberCreate: MemberSearchInputProps['onMemberCreate'] = member => {
    onMemberCreate(member);
    onDataUpdate({
      host: member.name,
    });
  };

  const onMembersFieldChange: MemberPickerProps['onMembersChange'] = members => {
    onDataUpdate({members});
  };

  return (
    <div className={styles.main}>
      <div className={styles.containerAmountCurrency}>
        <IconHeader icon={Currency}>款項</IconHeader>
        {
          data.payments.map((payment, idx) => (
            <div
              key={idx}
              className={styles.containerPayment}>
              <div className={styles.containerAmountCurrencyFields}>
                <div>
                  <Input
                    name="bill-item"
                    fieldName="項目"
                    hasError={!!errorDetailMap[`payments.${idx}.item`]}
                    value={payment.item}
                    autoFocus
                    onChange={onPaymentItemFieldChange(idx)} />
                  <ValidationMessage message={errorDetailMap[`payments.${idx}.item`]} />
                </div>
                <div>
                  <InputGroup groupItem={currency}>
                    <Input
                      name="bill-amount"
                      fieldName="金額"
                      hasError={!!errorDetailMap[`payments.${idx}.amount`]}
                      value={payment.amount}
                      type="number"
                      min="0"
                      onChange={onPaymentAmountFieldChange(idx)} />
                  </InputGroup>
                  <ValidationMessage
                    message={errorDetailMap[`payments.${idx}.amount`]} />
                </div>
                {
                  payment.useCurrencyExchange && (
                    <div className={styles.containerCurrencyRate}>
                      <div>
                        <Select
                          name="bill-source-currency"
                          value={payment.sourceCurrency ?? ''}
                          hasError={!!errorDetailMap[`payments.${idx}.sourceCurrency`]}
                          placeholder="貨幣"
                          onChange={onPaymentSourceCurrencyFieldChange(idx)}>
                          {
                            availableCurrencyCodes.map(code => (
                              <option
                                key={code}
                                value={code}>
                                {code}
                              </option>
                            ))
                          }
                        </Select>
                        <ValidationMessage message={errorDetailMap[`payments.${idx}.sourceCurrency`]} />
                      </div>
                      <div>
                        <Input
                          name="bill-exchange-rate"
                          type="number"
                          fieldName="匯率"
                          hasError={!!errorDetailMap[`payments.${idx}.exchangeRate`]}
                          value={payment.exchangeRate}
                          min="0"
                          step="0.01"
                          onChange={onPaymentExchangeRateFieldChange(idx)} />
                        <ValidationMessage
                          message={errorDetailMap[`payments.${idx}.exchangeRate`]} />
                      </div>
                    </div>
                  )
                }
              </div>
              <footer className={styles.containerPaymentOptions}>
                <label className={styles.contianerUseCurrencyExchange}>
                  <Checkbox
                    name="bill-use-currency-exchange"
                    checked={payment.useCurrencyExchange}
                    onChange={onPaymentUseCurrencyExchangeFieldChange(idx)} />
                  <span>轉換外幣</span>
                </label>
                {
                  data.payments.length > 1 && (
                    <div className={styles.containerRemovePayment}>
                      <LinkText
                        variant="danger"
                        dataAttrs={{
                          testid: 'remove-bill-link-text',
                        }}
                        onClick={onPaymentRemove(idx)}>
                    刪除
                      </LinkText>
                    </div>
                  )
                }
              </footer>
            </div>
          ))
        }
        <footer>
          <div className={styles.containerRemovePayment}>
            <LinkText
              variant="default"
              dataAttrs={{
                testid: 'add-bill-payment-link-text',
              }}
              onClick={onPaymentCreate}>
                + 新增項目
            </LinkText>
          </div>
        </footer>
      </div>
      <div className={styles.containerMembers}>
        <IconHeader icon={UserAdd}>人員</IconHeader>
        <div className={styles.containerMemberFields}>
          <div>
            <MemberSearchInput
              defaultValue={data.host}
              name="host"
              fieldName="付款人"
              hasError={!!errorDetailMap.host}
              memberSearchList={memberSearchList}
              memberSearchKeywords={memberSearchKeywords}
              resetable
              onMemberSearch={onMemberSearch}
              onMemberCreate={onHostMemberCreate}
              onMemberClick={onHostMemberChange}
              onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
              onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
              onReset={onHostFieldReset} />
            <ValidationMessage
              message={errorDetailMap.host} />
          </div>
          <div>
            <MemberPicker
              fieldName="分攤人"
              hasError={!!errorDetailMap.members}
              resetable
              members={data.members}
              memberSearchList={memberSearchList}
              memberSearchKeywords={memberSearchKeywords}
              onMemberSearch={onMemberSearch}
              onMemberCreate={onMemberCreate}
              onMemberClick={onMemberClick}
              onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
              onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
              onMembersChange={onMembersFieldChange} />
            <ValidationMessage
              message={errorDetailMap.members} />
          </div>
        </div>
      </div>
    </div>
  );
};
