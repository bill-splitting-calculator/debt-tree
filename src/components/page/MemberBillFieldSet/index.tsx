import {Group} from 'grommet-icons';
import type {FC} from 'react';
import {useMemo, useState} from 'react';
import {CopyIndicator, LinkText} from 'src/components/base';
import type {LinkTextProps} from 'src/components/base/LinkText';
import {IconHeader, MemberBillCollapse} from 'src/components/page';
import type {MemberBillCollapseProps} from 'src/components/page/MemberBillCollapse';
import {formatMemberBillCopyContent, formatUniqueBillMembers} from 'src/formatters/share-record';
import {Member, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type MemberBillFieldSetProps = {
  bills: ShareRecord['bills'];
  currency: ShareRecord['currency'];
};

export const MemberBillFieldSet: FC<MemberBillFieldSetProps> = props => {
  const {bills, currency} = props;

  const uniqueMembers = useMemo(() => formatUniqueBillMembers(bills), [bills]);

  const [memberCheckMap, setMemberCheckMap] = useState<Record<Member['name'], boolean>>({});

  type CreateOnMemberBillCheckChange = (name: string) => MemberBillCollapseProps['onCheckChange'];
  const createOnMemberBillCheckChange: CreateOnMemberBillCheckChange = name => (
    checked => {
      setMemberCheckMap({
        ...memberCheckMap,
        [name]: checked,
      });
    }
  );

  const hasCheckedItem = Object.values(memberCheckMap).some(value => value);

  const onMemberSelectCancel: LinkTextProps['onClick'] = () => {
    setMemberCheckMap({});
  };

  const onMemberSelectAll: LinkTextProps['onClick'] = () => {
    setMemberCheckMap(
      uniqueMembers.reduce((carry, {name}) => (
        Object.assign(carry, {[name]: true})
      ), {}),
    );
  };

  const onMemberBillsDataCopy: LinkTextProps['onClick'] = () => {
    navigator.clipboard.writeText(
      formatMemberBillCopyContent({
        bills,
        currency,
        memberCheckMap,
      }),
    );
  };

  return (
    <div className={styles.main}>
      <IconHeader icon={Group}>個人分攤費用</IconHeader>
      <div className={styles.containerMemberBills}>
        {
          bills.length
            ? (
              uniqueMembers.map(member => (
                <MemberBillCollapse
                  key={member.name}
                  checked={memberCheckMap[member.name] ?? false}
                  defaultCollapsed={false}
                  member={member}
                  bills={bills}
                  currency={currency}
                  onCheckChange={createOnMemberBillCheckChange(member.name)} />
              ))
            )
            : null
        }
      </div>
      <footer className={styles.containerSelectToolset}>
        {
          hasCheckedItem
            ? (
              <LinkText
                dataAttrs={{
                  testid: 'member-bill-field-set-cancel-select-link-text',
                }}
                onClick={onMemberSelectCancel}>
                取消選取
              </LinkText>
            )
            : (
              <LinkText
                dataAttrs={{
                  testid: 'member-bill-field-set-select-all-link-text',
                }}
                onClick={onMemberSelectAll}>
                全選
              </LinkText>
            )
        }
        <CopyIndicator
          dataAttrs={{
            testid: 'member-bills-copy-indicator',
          }}
          onClick={onMemberBillsDataCopy}>
          <LinkText
            disabled={!hasCheckedItem}
            dataAttrs={{
              testid: 'member-bill-field-set-copy-selected-member-bill-link-text',
            }}>
          複製選取人員款項
          </LinkText>
        </CopyIndicator>

      </footer>
    </div>
  );
};

export default MemberBillFieldSet;
