import {Group, Trash, UserManager} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {
  formatCurrencyAmount,
  formatBillMemberStatistics,
  formatMemberNameChain,
  formatBillPaymentTotalAmount,
} from 'src/formatters/share-record';
import type {Bill, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type BillCardProps = {
  currency: ShareRecord['currency'];
  bill: Bill;
  onEdit: () => void;
  onRemove: () => void;
};

export const BillCard: FC<BillCardProps> = props => {
  const {
    bill: {
      payments,
      host,
      members,
    },
    currency,
    onEdit,
    onRemove,
  } = props;

  const onInnerRemove: MouseEventHandler<SVGElement> = e => {
    e.stopPropagation();

    onRemove();
  };

  return (
    <div
      className={styles.main}
      data-testid="bill-card"
      onClick={onEdit}>
      <header className={styles.containerHostToolset}>
        <aside
          className={styles.containerHost}
          data-testid="bill-card-host-container">
          <UserManager color="plain" />
          <span>{host}</span>
        </aside>
        <aside className={styles.toolset}>
          <Trash
            color="plain"
            data-testid="bill-card-remove-icon"
            onClick={onInnerRemove} />
        </aside>
      </header>
      <div className={styles.body}>
        <header className={styles.textPayments}>款項</header>
        <ul className={styles.listPayments}>
          {
            payments.map((payment, idx) => {
              const {
                item,
                useCurrencyExchange,
                sourceCurrency,
                amount,
              } = payment;

              return (
                <li
                  key={idx}
                  className={styles.listItemPaymentAmount}
                  data-testid="bill-card-payment-item">
                  <span className={styles.textPaymentItem}>{item}</span>
                  <span className={styles.textPaymentAmountTotal}>
                    {
                      formatCurrencyAmount({
                        currency: useCurrencyExchange ? sourceCurrency : currency,
                        amount,
                      })
                    }
                  </span>
                </li>
              );
            })
          }
        </ul>
      </div>
      <footer className={styles.containerMemberCurrency}>
        <aside className={styles.containerMemberStatistics}>
          <Group color="plain" />
          <span
            title={formatMemberNameChain(members)}
            className={styles.textMemberStatistics}
            data-testid="bill-card-member-statistics">
            {formatBillMemberStatistics(members)}
          </span>
        </aside>
        <aside className={styles.containerCurrencyAmount}>
          <span
            className={styles.textCurrencyAmount}
            data-testid="bill-card-currency-amount">
            {
              formatCurrencyAmount({
                currency,
                amount: formatBillPaymentTotalAmount(payments),
              })
            }
          </span>
        </aside>
      </footer>
    </div>
  );
};
