import classnames from 'classnames';
import type {FC} from 'react';
import {MemberBillItem} from 'src/components/page';
import {formatCurrencyAmount} from 'src/formatters/share-record';
import type {Payment, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type MemberBillListProps = {
  paymentItemAmount: Pick<Payment, 'item' | 'amount'>[];
  currency: ShareRecord['currency'];
  totalAmount: number;
};

export const MemberBillList: FC<MemberBillListProps> = ({paymentItemAmount, currency, totalAmount}) => {
  const dispalyTotalAmount = formatCurrencyAmount({
    currency,
    amount: totalAmount,
  });

  return (
    <div className={styles.main}>
      <ol className={styles.listMemberBill}>
        {
          paymentItemAmount.map(({item, amount}, idx) => {
            return (
              <li
                key={idx}
                className={styles.containerMemberBillItem}>
                <MemberBillItem
                  item={item}
                  amount={amount || 0}
                  currency={currency} />
              </li>
            );
          })
        }
      </ol>
      <footer className={styles.footer}>
        <aside className={styles.containerTotal}>
          <sup className={styles.textTotalTitle}>總計</sup>
          <span className={
            classnames(
              styles.textTotalAmount,
              {
                [styles.negative]: dispalyTotalAmount.startsWith('-'),
              },
            )
          }>
            {dispalyTotalAmount}
          </span>
        </aside>
      </footer>
    </div>
  );
};
