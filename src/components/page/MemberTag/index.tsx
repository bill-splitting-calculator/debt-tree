import {FormClose, User} from 'grommet-icons';
import type {FC} from 'react';
import type {Member} from 'src/types/resources';
import styles from './styles.module.scss';

export type MemberTagProps = {
  member: Member;
  onRemove: () => void;
};

export const MemberTag: FC<MemberTagProps> = ({member, onRemove}) => (
  <div
    className={styles.main}
    data-testid="member-tag">
    <User color="plain" />
    <span className={styles.textMemberName}>{member.name}</span>
    <aside
      className={styles.toolset}
      data-testid="member-tag-remove-icon">
      <FormClose
        color="plain"
        onClick={onRemove} />
    </aside>
  </div>
);
