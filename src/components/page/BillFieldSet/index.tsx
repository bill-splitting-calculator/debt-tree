import {Money} from 'grommet-icons';
import type {FC} from 'react';
import {ClickActiveHighlight, LinkText} from 'src/components/base';
import {BillCard, IconHeader} from 'src/components/page';
import type {BillCardProps} from 'src/components/page/BillCard';
import type {Bill, ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export type BillFieldSetProps = {
  currency: ShareRecord['currency'];
  bills: Bill[];
  onBillEdit: (idx: number) => void;
  onBillRemove: (idx: number) => void;
  onBillAdd: () => void;
};

export const BillFieldSet: FC<BillFieldSetProps> = props => {
  const {
    currency,
    bills,
    onBillEdit,
    onBillRemove,
    onBillAdd,
  } = props;

  const createOnBillEdit: (idx: number) => BillCardProps['onEdit'] = idx => () => {
    onBillEdit(idx);
  };

  const createOnBillRemove: (idx: number) => BillCardProps['onRemove'] = idx => () => {
    onBillRemove(idx);
  };

  return (
    <div
      className={styles.main}
      data-testid="bill-field-set">
      <IconHeader icon={Money}>款項紀錄</IconHeader>
      {
        bills.length
          ? (
            <div>
              <ul
                className={styles.containerBills}
                data-testid="bill-card-list">
                {
                  bills.map((bill, idx) => (
                    <li
                      key={idx}
                      className={styles.containerBillCard}>
                      <ClickActiveHighlight>
                        <BillCard
                          currency={currency}
                          bill={bill}
                          onEdit={createOnBillEdit(idx)}
                          onRemove={createOnBillRemove(idx)} />
                      </ClickActiveHighlight>
                    </li>
                  ))
                }
              </ul>
            </div>
          )
          : null
      }
      <footer>
        <LinkText
          dataAttrs={{
            testid: 'bill-field-set-add-bill',
          }}
          onClick={onBillAdd}>
          + 新增款項
        </LinkText>
      </footer>
    </div>
  );
};

export default BillFieldSet;
