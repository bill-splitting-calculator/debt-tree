import classnames from 'classnames';
import type {FC} from 'react';
import {publicUrl} from 'src/config/app';
import styles from './styles.module.scss';

export type AppHeaderProps = {
  children: string;
  className?: string;
};

export const AppHeader: FC<AppHeaderProps> = ({children, className}) => (
  <header className={classnames(styles.main, className)}>
    <img
      src={`${publicUrl}/logo.svg`}
      width="24"
      height="24"
      alt="App Logo" />
    <span className={styles.textHeader}>{children}</span>
  </header>
);
