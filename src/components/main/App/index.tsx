import {Catalog} from 'grommet-icons';
import type {FC, MouseEventHandler} from 'react';
import {useState} from 'react';
import {Button, ClickActiveHighlight} from 'src/components/base';
import {AppHeader} from 'src/components/main';
import {
  AddButton,
  EmptyShareRecordIndicator,
  IconHeader,
  ShareRecordBasicCreateModal,
  ShareRecordCard,
  ShareRecordCardList,
  ShareRecordEditModal,
} from 'src/components/page';
import type {BillCreateModalProps} from 'src/components/page/BillCreateModal';
import type {BillEditModalProps} from 'src/components/page/BillEditModal';
import type {ShareRecordBasicCreateModalProps} from 'src/components/page/ShareRecordBasicCreateModal';
import type {ShareRecordCardProps} from 'src/components/page/ShareRecordCard';
import type {ShareRecordEditModalProps} from 'src/components/page/ShareRecordEditModal';
import {createDefaultShareRecord} from 'src/formatters/share-record';
import {
  useAppDispatch,
  useAppHeight,
  useAppSelector,
  useMemberHistory,
} from 'src/hooks';
import {
  addShareRecord,
  removeShareRecord,
  updateShareRecord,
} from 'src/store/actions/share-record';
import type {ShareRecord} from 'src/types/resources';
import styles from './styles.module.scss';

export const App: FC = () => {
  const [isShareRecordBasicCreateModalOpen, setIsShareRecordBasicCreateModalOpen] = useState<boolean>(false);
  const [activeShareRecordIndex, setActiveShareRecordIndex] = useState<number | undefined>();
  const [isShareRecordEditModalOpen, setIsShareRecordEditModalOpen] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const shareRecords = useAppSelector(state => state.shareRecord.shareRecords);

  const {
    memberSearchResults,
    memberSearchKeywords,
    onMemberReferenceAdd,
    onMemberSearchOptionsChange,
    onMemberSearchKeywordAdd,
    onMemberSearchKeywordRemove,
  } = useMemberHistory();

  const memberSearchList = memberSearchResults?.map(({name}) => name);

  useAppHeight();

  const activeShareRecord: ShareRecord = (activeShareRecordIndex !== undefined && shareRecords[activeShareRecordIndex])
    ? shareRecords[activeShareRecordIndex]
    : createDefaultShareRecord();

  const onShareRecordAdd: MouseEventHandler<HTMLButtonElement> = () => {
    setIsShareRecordBasicCreateModalOpen(true);
  };

  const onShareRecordBasicSubmit: ShareRecordBasicCreateModalProps['onSubmit'] = shareRecordBasic => {
    dispatch(
      addShareRecord({
        ...shareRecordBasic,
        bills: [],
      }),
    );
  };

  const onShareRecordUpdate: ShareRecordEditModalProps['onShareRecordUpdate'] = shareRecordBasic => {
    if (activeShareRecordIndex === undefined) return;

    dispatch(
      updateShareRecord(
        activeShareRecordIndex,
        {
          ...activeShareRecord,
          ...shareRecordBasic,
        },
      ),
    );
  };

  type CreateOnShareRecordEdit = (index: number) => ShareRecordCardProps['onEdit'];
  const createOnShareRecordEdit: CreateOnShareRecordEdit = index => () => {
    setActiveShareRecordIndex(index);
    setIsShareRecordEditModalOpen(true);
  };

  type CreateOnShareRecordRemove = (index: number) => ShareRecordCardProps['onRemove'];
  const createOnShareRecordRemove: CreateOnShareRecordRemove = index => () => {
    dispatch(
      removeShareRecord(index),
    );
  };

  const onShareRecordEditModalExited: ShareRecordEditModalProps['onExited'] = () => {
    setActiveShareRecordIndex(undefined);
  };

  const onBillModalMemberSearch: BillCreateModalProps['onMemberSearch'] | BillEditModalProps['onMemberSearch'] = keyword => {
    onMemberSearchOptionsChange({
      keyword,
      limit: 5,
    });
  };

  return (
    <>
      <ShareRecordBasicCreateModal
        open={isShareRecordBasicCreateModalOpen}
        memberSearchList={memberSearchList}
        memberSearchKeywords={memberSearchKeywords}
        onOpenChange={setIsShareRecordBasicCreateModalOpen}
        onMemberSearch={onBillModalMemberSearch}
        onMemberCreate={onMemberReferenceAdd}
        onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
        onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
        onSubmit={onShareRecordBasicSubmit} />
      <ShareRecordEditModal
        shareRecord={activeShareRecord}
        open={isShareRecordEditModalOpen}
        memberSearchList={memberSearchList}
        memberSearchKeywords={memberSearchKeywords}
        onOpenChange={setIsShareRecordEditModalOpen}
        onMemberSearch={onBillModalMemberSearch}
        onMemberCreate={onMemberReferenceAdd}
        onMemberSearchKeywordAdd={onMemberSearchKeywordAdd}
        onMemberSearchKeywordRemove={onMemberSearchKeywordRemove}
        onShareRecordUpdate={onShareRecordUpdate}
        onExited={onShareRecordEditModalExited} />
      <main className={styles.main}>
        <AppHeader className={styles.header}>分攤計算器</AppHeader>
        <section className={styles.body}>
          {
            !shareRecords.length
              ? (
                <div
                  data-testid="empty-share-record-indicator-container"
                  className={styles.containerEmptyShareRecord}>
                  <EmptyShareRecordIndicator />
                  <Button
                    variant="primary"
                    className={styles.buttonCreateShareRecord}
                    dataAttrs={{
                      testid: 'add-share-record-button',
                    }}
                    onClick={onShareRecordAdd}>
                    來新增幾筆紀錄吧
                  </Button>
                </div>
              )
              : (
                <div className={styles.containerShareRecords}>
                  <IconHeader icon={Catalog}>分攤紀錄</IconHeader>
                  <ShareRecordCardList>
                    {
                      shareRecords.map((shareRecord, idx) => (
                        <ShareRecordCard
                          key={idx}
                          shareRecord={shareRecord}
                          onEdit={createOnShareRecordEdit(idx)}
                          onRemove={createOnShareRecordRemove(idx)} />
                      ))
                    }
                  </ShareRecordCardList>
                  <div className={styles.containerAddShareRecordButtonRounded}>
                    <ClickActiveHighlight>
                      <AddButton
                        dataAttrs={{
                          testid: 'add-share-record-button',
                        }}
                        onClick={onShareRecordAdd} />
                    </ClickActiveHighlight>
                  </div>
                </div>
              )
          }
        </section>
      </main>
    </>
  );
};
