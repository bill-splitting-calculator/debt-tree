import currencyCodes from 'currency-codes';

export const availableCurrencyCodes = currencyCodes.codes();
