export const overrideMessages = {
  'string.empty': '{{#label}} 不可為空',
  'string.min': '{{#label}} 長度必須至少要有 {{#limit}} 個字元長度',
  'string.max': '{{#label}} 長度必須至多只能有 {{#limit}} 個字元長度',
  'number.base': '{{#label}} 必須為數字',
  'number.min': '{{#label}} 長度必須至多只能有 {{#limit}} 個字元長度',
  'number.max': '{{#label}} 長度必須至多只能有 {{#limit}} 個字元長度',
  'number.unsafe': '{{#label}} 超過可使用上限',
  'number.precision': '{{#label}} 小數點不可超過 {{#limit}} 位數',
  'number.greater': '{{#label}} 必須大於 {{#limit}}',
  'array.min': '{{#label}} 必須至少包含 {{#limit}} 個項目',
  'any.only': '{{#label}} 不在選擇清單內',
  'any.required': '{{#label}} 必填',
};
