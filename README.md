# 分攤計算器

    計算費用在多人分攤下，每人需要平均支付給支付人多少費用，同時紀錄時間區間、人數以及總額，提供未來查詢使用。

## npm scripts

#### `npm start`

  * 以 `development` 模式執行App
  * 執行後可於 [http://localhost:3000](http://localhost:3000) 瀏覽執行內容
  * 程式碼有任何變動時，頁面會自動載入更新
  * 任何 linter 檢測出的錯誤都會紀錄在 console 中

### `npm run test:unit`
  * 使用 `jest` 執行單元測試，可在 `console` 中執行測試進階操作

### `npm run test:e2e`
  * 使用 `cypress` 執行 `end to end` 測試，執行後會自動開啟 `cypress` 測試環境

### `npm run build`
  * 以 `production` 模式編譯App，並將檔案輸出至 `build` 資料夾
  * 檔案編譯最佳化會在此階段執行

### `npm run analyze`
  * 以 `production` 模式編譯App，分析相關模組使用大小與關聯
  * 執行後可於 [http://localhost:8888](http://localhost:8888) 瀏覽執行內容

### `npm run storybook`
  * 預覽使用元件樣式與事件
  * 執行後可於 [http://localhost:6006/](http://localhost:6006/) 瀏覽執行內容
