beforeEach(() => {
  cy.clearLocalStorage()
    .setResolution('desktop');
});

describe('empty share record content', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('displays a empty content indication', () => {
    cy.getTestId('empty-share-record-indicator').as('indicator');

    cy.get('@indicator')
      .contains('(๑•ω•)ノ');

    cy.get('@indicator')
      .getTestId('empty-share-record-indicator-greeting')
      .each(el => {
        expect(el.text()).to.equal('嘿喲嘿喲');
      });
  });

  it('displays a button for adding create share record button', () => {
    cy.getTestId('add-share-record-button')
      .as('addButton')
      .should('have.lengthOf', 1)
      .and('have.text', '來新增幾筆紀錄吧');

    cy.getTestId('share-record-basic-create-modal')
      .should('not.exist');

    cy.get('@addButton')
      .click();

    cy.getTestId('share-record-basic-create-modal');
  });

  it('opens and closes create share record modal', () => {
    cy.getTestId('add-share-record-button')
      .click();

    cy.getTestId('share-record-basic-create-modal')
      .as('createShareRecordModal');

    cy.get('@createShareRecordModal')
      .getTestId('modal-header-back-icon-text')
      .as('createShareRecordBasicModalHeaderBack')
      .contains('返回');

    cy.get('@createShareRecordModal')
      .getTestId('modal-header-title')
      .contains('新增分攤紀錄');

    cy.get('@createShareRecordModal')
      .getTestId('modal-header-done-text')
      .as('modalHeaderDoneText')
      .should($el => {
        expect($el.text()).to.equal('儲存');
        expect($el[0].className).match(/disabled/);
      });

    cy.get('@createShareRecordBasicModalHeaderBack')
      .click();

    cy.getTestId('share-record-basic-create-modal')
      .should('not.exist');
  });

  it('creates share record and members history data', () => {
    cy.wrap(window.localStorage.getItem('redux-state'))
      .should(rawReduxState => {
        expect(rawReduxState).to.be.null;
      });

    cy.getTestId('add-share-record-button')
      .click();

    cy.getTestId('share-record-basic-create-modal-body')
      .as('createShareRecordModalBody')
      .should('be.visible');

    cy.get('@createShareRecordModalBody')
      .find('header')
      .first()
      .contains('基本設定');

    cy.get('@createShareRecordModalBody')
      .find('input')
      .should('have.length', 4);

    cy.get('@createShareRecordModalBody')
      .find('select')
      .should('have.length', 1);

    cy.get('@createShareRecordModalBody')
      .getTestId('share-record-name-input')
      .as('shareRecordNameInput')
      .should('have.attr', 'placeholder', '名稱');

    cy.get('@shareRecordNameInput')
      .type('share record name');

    cy.getTestId('share-record-main-host-input')
      .as('shareRecordMainHostInput')
      .should($el => {
        expect($el).match('[readonly]');
        expect($el).have.attr('placeholder', '主要付款人');
      });

    cy.get('@shareRecordMainHostInput')
      .click();

    cy.getTestId('share-record-main-host-search-input-dropdown-menu')
      .as('mainHostInputDropdownMenu');

    cy.getTestId('share-record-main-host-search-input-dropdown-menu-keyword-input')
      .as('mainHostInputDropdownMenuInput')
      .type('host');

    cy.get('@mainHostInputDropdownMenu')
      .contains('查無資料');

    cy.get('@mainHostInputDropdownMenu')
      .contains(' + 新增 host')
      .click();

    cy.get('@shareRecordMainHostInput')
      .should('have.value', 'host');

    cy.getTestId('share-record-main-host-reset-icon')
      .click();

    cy.get('@shareRecordMainHostInput')
      .should('have.value', '')
      .click();

    cy.get('@mainHostInputDropdownMenuInput')
      .should('have.value', '')
      .type('host');

    cy.get('@mainHostInputDropdownMenu')
      .contains('host')
      .click();

    cy.get('@shareRecordMainHostInput')
      .should('have.value', 'host');

    cy.getTestId('share-record-begin-date-date-picker-input')
      .as('shareRecordBeginDateInput')
      .click();

    cy.getTestId('share-record-begin-date-date-picker-calendar-container')
      .as('shareRecordBeginDateInputDropdownMenu')
      .find('section > span:nth-child(21)')
      .as('shareRecordBeginDateInputDropdownMenuDate')
      .invoke('text')
      .as('shareRecordBeginDateInputDropdownMenuDateText');

    cy.get('@shareRecordBeginDateInputDropdownMenuDate')
      .click();

    cy.get('@shareRecordBeginDateInputDropdownMenuDateText')
      .then(text => {
        const date = new Date();
        const month = `${date.getMonth() + 1}`.padStart(2, '0');

        cy.get('@shareRecordBeginDateInput')
          .should('have.value', `${date.getFullYear()}-${month}-${text}`);
      });

    cy.get('@shareRecordBeginDateInput')
      .invoke('val')
      .as('shareRecordBeginDateText');

    cy.getTestId('share-record-basic-create-modal')
      .getTestId('modal-header-done-text')
      .click();

    cy.getTestId('share-record-basic-create-modal')
      .should('not.exist');


    cy.wait(1000)
      .get('@shareRecordBeginDateText')
      .then(shareRecordBeginDateText => {
        const rawReduxState = window.localStorage.getItem('redux-state');

        expect(rawReduxState).not.to.be.null;

        if (rawReduxState) {
          const parseState = JSON.parse(rawReduxState);

          expect(parseState).to.deep.equal({
            memberHistory: {
              memberReferences: [
                {
                  name: 'host',
                },
              ],
              memberSearchKeywords: ['host'],
            },
            shareRecord: {
              shareRecords: [
                {
                  bills: [],
                  currency: 'TWD',
                  dates: [shareRecordBeginDateText, ''],
                  mainHost: 'host',
                  name: 'share record name',
                },
              ],
            },
          });
        }
      });
  });
});

describe('share record list', () => {
  beforeEach(() => {
    cy.loadReduxState('basic')
      .visit('/');
  });

  it('displays info about share record on the card', () => {
    cy.getTestId('share-record-card-list')
      .as('shareRecordCardList')
      .children()
      .should('have.length', 1)
      .first()
      .as('shareRecordCard');

    cy.get('@shareRecordCard')
      .getTestId('share-record-card-dates-container')
      .contains('2022-06-18 ~ 2022-06-19');

    cy.get('@shareRecordCard')
      .getTestId('share-record-card-name')
      .contains('share record name');

    cy.get('@shareRecordCard')
      .getTestId('share-record-card-member-count-container')
      .contains('0');

    cy.get('@shareRecordCard')
      .getTestId('share-record-card-bill-total-container')
      .contains('NT$0.00');
  });

  it('removes share record item by clicking trash icon on the card', () => {
    cy.getTestId('share-record-card-remove-icon-container')
      .as('shareRecordCardRemoveIconContainer')
      .click()
      .should(() => {
        const rawReduxState = window.localStorage.getItem('redux-state');

        expect(rawReduxState).not.to.be.null;

        if (rawReduxState) {
          const parseReduxState = JSON.parse(rawReduxState);

          expect(parseReduxState.shareRecord.shareRecords).to.have.length(0);
        }
      });

    cy.getTestId('empty-share-record-indicator-container');
  });
});

describe('edit share record', () => {
  beforeEach(() => {
    cy.loadReduxState('basic')
      .visit('/');

    cy.getTestId('share-record-card-list')
      .getTestId('share-record-card')
      .click();
  });

  it('edit the record through edit modal', () => {
    cy.getTestId('share-record-edit-modal')
      .as('shareRecordEditModal');

    cy.get('@shareRecordEditModal')
      .getTestId('share-record-name-input')
      .clear()
      .type('share record name modified');

    cy.get('@shareRecordEditModal')
      .getTestId('share-record-main-host-input')
      .click();

    cy.getTestId('share-record-main-host-search-input-dropdown-menu-keyword-input')
      .type('host modified');

    cy.getTestId('share-record-main-host-search-input-dropdown-menu')
      .contains(' + 新增 host modified')
      .click();

    cy.getTestId('share-record-begin-date-date-picker-input')
      .as('shareRecordBeginDateDatePickerInput')
      .click();

    cy.getTestId('share-record-begin-date-calendar')
      .find('section > span')
      .eq(10)
      .click();

    cy.getTestId('share-record-end-date-date-picker-input')
      .as('shareRecordEndDateDatePickerInput')
      .click();

    cy.getTestId('share-record-end-date-calendar')
      .find('section > span')
      .eq(11)
      .click();

    cy.getTestId('share-record-currency-select')
      .select('USD');

    cy.get('@shareRecordBeginDateDatePickerInput')
      .invoke('val')
      .as('shareRecordModifiedBeginDate');

    cy.get('@shareRecordEndDateDatePickerInput')
      .invoke('val')
      .as('shareRecordModifiedEndDate');

    cy.get('@shareRecordEditModal')
      .getTestId('modal-header-done-text')
      .click();

    cy.wait(1000)
      .then(() => {
        const rawReduxStatus = window.localStorage.getItem('redux-state');
        if (rawReduxStatus) {
          cy.get('@shareRecordModifiedBeginDate')
            .then(beginDateText => {
              cy.get('@shareRecordModifiedEndDate')
                .then(endDateText => {
                  const [editedShareRecord] = JSON.parse(rawReduxStatus)
                    .shareRecord
                    .shareRecords;

                  expect(editedShareRecord).to.deep.equal({
                    name: 'share record name modified',
                    mainHost: 'host modified',
                    dates: [beginDateText, endDateText],
                    currency: 'USD',
                    bills: [],
                  });
                });
            });
        }
      });
  });
});
