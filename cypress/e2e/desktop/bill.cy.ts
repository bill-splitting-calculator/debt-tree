beforeEach(() => {
  cy.setResolution('desktop');
});

beforeEach(() => {
  cy.clearLocalStorage();
});

describe('add bills', () => {
  beforeEach(() => {
    cy.beforeBillEditModalOpen('basic');
  });

  it('adds bill throught edit share recrod modal', () => {
    cy.getTestId('share-record-edit-modal')
      .as('shareRecordEditModal')
      .getTestId('bill-field-set')
      .getTestId('bill-field-set-add-bill')
      .contains('+ 新增款項')
      .click();

    cy.getTestId('bill-create-modal')
      .as('billCreateModal');

    cy.get('@billCreateModal')
      .getTestId('bill-item-input')
      .first()
      .type('Bill Item 1');

    cy.get('@billCreateModal')
      .getTestId('bill-amount-input')
      .first()
      .type('300');

    cy.get('@billCreateModal')
      .getTestId('add-bill-payment-link-text')
      .contains('+ 新增項目')
      .click();

    cy.get('@billCreateModal')
      .getTestId('bill-item-input')
      .eq(1)
      .type('Bill Item 2');

    cy.get('@billCreateModal')
      .getTestId('bill-amount-input')
      .eq(1)
      .type('600');

    cy.get('@billCreateModal')
      .getTestId('bill-use-currency-exchange-checkbox')
      .eq(1)
      .check();

    cy.get('@billCreateModal')
      .getTestId('bill-source-currency-select')
      .first()
      .select('JPY');

    cy.get('@billCreateModal')
      .getTestId('bill-exchange-rate-input')
      .first()
      .type('0.25');

    cy.get('@billCreateModal')
      .getTestId('host-input')
      .should('have.value', 'host');

    cy.get('@billCreateModal')
      .getTestId('member-picker')
      .as('memberPicker')
      .getTestId('member-tag-list')
      .as('memberPickerTagList')
      .getTestId('member-tag')
      .should('have.length', 1)
      .and('contain.text', 'host');

    cy.get('@memberPicker')
      .getTestId('member-picker-input')
      .as('memberPickerInput')
      .click();

    cy.getTestId('member-picker-search-input-dropdown-menu-keyword-input')
      .type('member 1');

    cy.getTestId('member-picker-search-input-dropdown-menu')
      .contains(' + 新增 member 1')
      .click();

    cy.get('@memberPickerInput')
      .should('have.value', '');

    cy.get('@memberPickerTagList')
      .children()
      .should('have.length', 2)
      .eq(1)
      .contains('member 1');

    cy.get('@billCreateModal')
      .getTestId('modal-header-done-text')
      .click();

    cy.get('@billCreateModal')
      .should('not.exist');

    cy.get('@shareRecordEditModal')
      .getTestId('modal-header-done-text')
      .click()
      .should(() => {
        const rawReduxState = window.localStorage.getItem('redux-state');

        if (rawReduxState) {
          const {
            shareRecord: {
              shareRecords: [
                {
                  bills: [
                    bill,
                  ],
                },
              ],
            },
          } = JSON.parse(rawReduxState);

          expect(bill).to.deep.equal({
            host: 'host',
            payments: [
              {
                useCurrencyExchange: false,
                item: 'Bill Item 1',
                amount: 300,
              },
              {
                useCurrencyExchange: true,
                item: 'Bill Item 2',
                amount: 600,
                sourceCurrency: 'JPY',
                exchangeRate: 0.25,
              },
            ],
            members: [
              {
                name: 'host',
              },
              {
                name: 'member 1',
              },
            ],
          });
        }
      });
  });
});

describe('split bills', () => {
  beforeEach(() => {
    cy.beforeBillEditModalOpen('full');
  });

  it('display bills', () => {
    cy.getTestId('share-record-edit-modal')
      .getTestId('bill-card-list')
      .getTestId('bill-card')
      .should('have.length', 2);

    cy.getTestId('bill-card')
      .eq(0)
      .as('firstBillCard')
      .getTestId('bill-card-host-container')
      .contains('host');

    cy.get('@firstBillCard')
      .getTestId('bill-card-remove-icon');

    cy.get('@firstBillCard')
      .getTestId('bill-card-payment-item')
      .should($el => {
        const elText = $el.text();

        [
          'Bill item 1',
          'Bill item 2',
          'NT$300.00',
          '¥300',
        ].forEach(text => {
          expect(elText).contains(text);
        });
      });

    cy.get('@firstBillCard')
      .getTestId('bill-card-member-statistics')
      .contains('host, member 1 與其他 1 位');

    cy.get('@firstBillCard')
      .getTestId('bill-card-currency-amount')
      .contains('NT$375.00');

    cy.getTestId('bill-card')
      .eq(1)
      .as('secondBillCard')
      .getTestId('bill-card-host-container')
      .contains('member 1');

    cy.get('@secondBillCard')
      .getTestId('bill-card-remove-icon');

    cy.get('@secondBillCard')
      .getTestId('bill-card-payment-item')
      .should($el => {
        const elText = $el.text();

        [
          'Bill item 3',
          'Bill item 4',
          'NT$600.00',
          '¥900',
        ].forEach(text => {
          expect(elText).contains(text);
        });
      });

    cy.get('@secondBillCard')
      .getTestId('bill-card-member-statistics')
      .contains('host, member 1 與其他 1 位');

    cy.get('@secondBillCard')
      .getTestId('bill-card-currency-amount')
      .contains('NT$825.00');

    cy.get('@firstBillCard')
      .click();

    cy.getTestId('bill-edit-modal')
      .getTestId('modal-header-back-icon-text')
      .click();

    cy.get('@secondBillCard')
      .click();

    cy.getTestId('bill-edit-modal')
      .getTestId('modal-header-back-icon-text')
      .click();
  });

  it('split bills for each members', () => {
    cy.getTestId('member-bill-collapse')
      .should('have.length', 2);

    cy.getTestId('member-bill-collapse')
      .as('memberBillCollapses');

    cy.get('@memberBillCollapses')
      .eq(0)
      .as('firstMemberBillCollapse');

    cy.get('@firstMemberBillCollapse')
      .getTestId('member-bill-collapse-member-name-container')
      .contains('host');

    cy.get('@firstMemberBillCollapse')
      .getTestId('host-bill-frame-legend')
      .contains('應支付 member 1 帳款');

    cy.get('@firstMemberBillCollapse')
      .getTestId('member-bill-item')
      .as('memberBillItems')
      .should('have.length', 4);

    cy.get('@memberBillItems')
      .eq(0)
      .should('contain.text', 'Bill item 3')
      .and('contain.text', 'NT$200.00');

    cy.get('@memberBillItems')
      .eq(1)
      .should('contain.text', 'Bill item 4')
      .and('contain.text', 'NT$75.00');

    cy.get('@memberBillItems')
      .eq(2)
      .should('contain.text', 'Bill item 1')
      .contains('-NT$100.00')
      .should($el => {
        expect($el[0].className).includes('negative');
      });

    cy.get('@memberBillItems')
      .eq(3)
      .should('contain.text', 'Bill item 2')
      .contains('-NT$25.00')
      .should($el => {
        expect($el[0].className).includes('negative');
      });

    cy.get('@memberBillCollapses')
      .eq(1)
      .as('secondMemberBillCollapse');

    cy.get('@secondMemberBillCollapse')
      .getTestId('host-bill-frame')
      .as('hostBillFrames')
      .should('have.length', 2);

    cy.get('@secondMemberBillCollapse')
      .getTestId('member-bill-collapse-member-name-container')
      .contains('member 2');

    cy.get('@hostBillFrames')
      .eq(0)
      .as('secondBillFirstHostBillFrames');

    cy.get('@secondBillFirstHostBillFrames')
      .getTestId('host-bill-frame-legend')
      .contains('應支付 host 帳款');

    cy.get('@secondBillFirstHostBillFrames')
      .getTestId('member-bill-item')
      .as('secondBillMemberBillItems')
      .should('have.length', 2);

    cy.get('@secondBillMemberBillItems')
      .eq(0)
      .should('contain.text', 'Bill item 1')
      .and('contain.text', 'NT$100.00');

    cy.get('@secondBillMemberBillItems')
      .eq(1)
      .should('contain.text', 'Bill item 2')
      .and('contain.text', 'NT$25.00');

    cy.get('@hostBillFrames')
      .eq(1)
      .as('secondBillSecondHostBillFrames');

    cy.get('@secondBillSecondHostBillFrames')
      .getTestId('host-bill-frame-legend')
      .contains('應支付 member 1 帳款');

    cy.get('@secondBillSecondHostBillFrames')
      .getTestId('member-bill-item')
      .as('secondBillMemberBillItems')
      .should('have.length', 2);

    cy.get('@secondBillMemberBillItems')
      .eq(0)
      .should('contain.text', 'Bill item 3')
      .and('contain.text', 'NT$200.00');

    cy.get('@secondBillMemberBillItems')
      .eq(1)
      .should('contain.text', 'Bill item 4')
      .and('contain.text', 'NT$75.00');
  });

  it('select, unselect and copy member bills', () => {
    cy.on('uncaught:exception', e => {
      if (e.message.includes('Document is not focused')) {
        return false;
      }
    });

    cy.getTestId('member-bill-field-set-cancel-select-link-text')
      .should('not.exist');

    cy.getTestId('member-bills-copy-indicator')
      .as('memberBillCopyIndicator')
      .should('not.contain.text', '已複製至剪貼簿');

    cy.get('@memberBillCopyIndicator')
      .getTestId('member-bill-field-set-copy-selected-member-bill-link-text')
      .should($el => {
        expect($el[0].className).includes('disabled');
      });

    cy.getTestId('member-bill-field-set-select-all-link-text')
      .click();

    cy.getTestId('member-bill-field-set-select-all-link-text')
      .should('not.exist');

    cy.getTestId('member-bill-field-set-copy-selected-member-bill-link-text')
      .as('memberBillFieldSetCopySelectedMemberBillLinkText')
      .should($el => {
        expect($el[0].className).not.includes('disabled');
      })
      .click();

    cy.get('@memberBillCopyIndicator')
      .contains('已複製至剪貼簿');

    cy.wait(3000)
      .get('@memberBillCopyIndicator')
      .should('not.contain.text', '已複製至剪貼簿');

    cy.getTestId('member-bill-collapse-checkbox')
      .each($el => {
        expect($el).be.checked;
      });

    cy.getTestId('member-bill-field-set-cancel-select-link-text')
      .click();

    cy.getTestId('member-bill-collapse-checkbox')
      .each($el => {
        expect($el).not.be.checked;
      });

    cy.getTestId('member-bill-field-set-select-all-link-text');

    cy.get('@memberBillFieldSetCopySelectedMemberBillLinkText')
      .should($el => {
        expect($el[0].className).includes('disabled');
      });
  });
});

describe('edit bill', () => {
  beforeEach(() => {
    cy.beforeBillEditModalOpen('full');
  });

  it('remove bill', () => {
    cy.getTestId('share-record-edit-modal')
      .as('shareRecordEditModal')
      .getTestId('bill-card')
      .as('billCards')
      .should('have.length', 2)
      .last()
      .getTestId('bill-card-remove-icon')
      .click();

    cy.get('@billCards')
      .should('have.length', 1);

    cy.get('@shareRecordEditModal')
      .getTestId('modal-header-done-text')
      .click();

    cy.get('@shareRecordEditModal');

    cy.wait(1000)
      .then(() => {
        const rawReduxStatus = window.localStorage.getItem('redux-state');

        if (rawReduxStatus) {
          cy.fixture('redux-state/full')
            .then(fullReduxState => {
              const {bills: editedBills} = JSON.parse(rawReduxStatus)
                .shareRecord
                .shareRecords[0];
              const remainedBills = fullReduxState
                .shareRecord
                .shareRecords[0]
                .bills
                .slice(0, 1);

              expect(editedBills).to.deep.equal(remainedBills);
            });
        }
      });
  });

  it('edit bill content', () => {
    cy.getTestId('share-record-edit-modal')
      .as('shareRecordEditModal')
      .getTestId('bill-card')
      .first()
      .as('firstBillCard')
      .click();

    cy.getTestId('bill-edit-modal')
      .as('billEditModal')
      .getTestId('remove-bill-link-text')
      .last()
      .click();

    cy.get('@billEditModal')
      .getTestId('bill-item-input')
      .clear()
      .type('Bill Item 1 Modified');

    cy.get('@billEditModal')
      .getTestId('bill-amount-input')
      .clear()
      .type('600');

    cy.get('@billEditModal')
      .getTestId('bill-use-currency-exchange-checkbox')
      .check();

    cy.get('@billEditModal')
      .getTestId('bill-source-currency-select')
      .select('AUD');

    cy.get('@billEditModal')
      .getTestId('bill-exchange-rate-input')
      .type('0.1');

    cy.get('@billEditModal')
      .getTestId('host-input')
      .click();

    cy.getTestId('host-search-input-dropdown-menu-keyword-input')
      .type('host modified');

    cy.getTestId('host-search-input-dropdown-menu')
      .contains(' + 新增 host modified')
      .click();

    cy.get('@billEditModal')
      .getTestId('member-picker-input')
      .click();

    cy.getTestId('member-picker-search-input-dropdown-menu-keyword-input')
      .type('member 3');

    cy.getTestId('member-picker-search-input-dropdown-menu')
      .contains(' + 新增 member 3')
      .click();

    cy.get('@billEditModal')
      .getTestId('member-tag')
      .first()
      .as('firstMemberTag')
      .contains('host');

    cy.get('@firstMemberTag')
      .getTestId('member-tag-remove-icon')
      .click();

    cy.get('@billEditModal')
      .getTestId('modal-header-done-text')
      .click();

    cy.get('@firstBillCard')
      .getTestId('bill-card-payment-item')
      .should($el => {
        const elText = $el.text();

        ['Bill Item 1 Modified', 'A$600.00'].forEach(text => {
          expect(elText).to.include(text);
        });
      });

    cy.get('@firstBillCard')
      .getTestId('bill-card-member-statistics')
      .contains('member 1, member 2 與其他 1 位');

    cy.get('@firstBillCard')
      .getTestId('bill-card-currency-amount')
      .contains('NT$60.00');

    cy.get('@shareRecordEditModal')
      .getTestId('modal-header-done-text')
      .click();

    cy.get('@shareRecordEditModal')
      .getTestId('modal-header-back-icon-text')
      .click();

    cy.wait(1000)
      .then(() => {
        const rawReduxStatus = window.localStorage.getItem('redux-state');

        if (rawReduxStatus) {
          cy.fixture('redux-state/full')
            .then(fullReduxState => {
              const [editedShareRecord] = JSON.parse(rawReduxStatus)
                .shareRecord
                .shareRecords;
              const [firstShareRecord] = fullReduxState
                .shareRecord
                .shareRecords;

              expect(editedShareRecord).to.deep.equal({
                ...firstShareRecord,
                bills: [
                  {
                    host: 'host modified',
                    payments: [
                      {
                        item: 'Bill Item 1 Modified',
                        amount: 600,
                        useCurrencyExchange: true,
                        sourceCurrency: 'AUD',
                        exchangeRate: 0.1,
                      },
                    ],
                    members: [
                      {
                        name: 'member 1',
                      },
                      {
                        name: 'member 2',
                      },
                      {
                        name: 'member 3',
                      },
                    ],
                  },
                  firstShareRecord.bills[1],
                ],
              });
            });
        }
      });
  });
});
