beforeEach(() => {
  cy.visit('/');
});

describe('basic', () => {
  it('page meta config', () => {
    cy.get('meta[name="viewport"]')
      .should('have.attr', 'content', 'width=device-width, initial-scale=1');

    cy.get('meta[name="theme-color"]')
      .should('have.attr', 'content', '#99b89e');

    cy.get('meta[name="description"]')
      .should('have.attr', 'content', '計算費用在多人分攤下，每人需要平均支付給支付人多少費用，同時紀錄時間區間、人數以及總額，提供未來查詢使用。');

    cy.title().should('eq', '分攤計算器');
  });

  it('rooted at root id', () => {
    cy.get('#root').should('have.lengthOf', 1);
  });

  it('has a header with app icon and name', () => {
    cy.get('main > header').as('appHeader');

    cy.get('@appHeader')
      .get('img')
      .should('have.lengthOf', 1)
      .and('have.attr', 'width', 24)
      .and('have.attr', 'height', 24)
      .and('have.attr', 'alt', 'App Logo');

    cy.get('@appHeader')
      .contains('分攤計算器')
      .should('have.lengthOf', 1);
  });
});
