// in cypress/support/index.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    getTestId(value: string): Chainable<JQuery<Element>>;

    loadReduxState(value: 'basic' | 'full'): Chainable<void>;

    setResolution(breakpoint: 'desktop' | 'pad' | 'mobile'): Chainable<null>;

    beforeBillEditModalOpen(value: 'basic' | 'full'): Chainable<null>;
  }
}
