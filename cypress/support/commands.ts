/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chai1nable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }
Cypress.Commands.add('getTestId', {prevSubject: ['optional', 'element']}, (subject, testId) => (
  subject
    ? cy.wrap(subject).find(`[data-testid=${testId}]`)
    : cy.get(`[data-testid=${testId}]`)
));

Cypress.Commands.add('loadReduxState', type => (
  cy.fixture(`redux-state/${type}`)
    .then(reduxState => {
      window.localStorage.setItem('redux-state', JSON.stringify(reduxState));
    })
));

Cypress.Commands.add('setResolution', breakpoint => {
  const breakpointMap: Record<typeof breakpoint, {width: number; height: number;}> = {
    desktop: {
      width: 1200,
      height: 768,
    },
    pad: {
      width: 768,
      height: 1000,
    },
    mobile: {
      width: 390,
      height: 840,
    },
  };

  const {width, height} = breakpointMap[breakpoint];

  return cy.viewport(width, height);
});

Cypress.Commands.add('beforeBillEditModalOpen', reduxStateType => {
  cy.loadReduxState(reduxStateType)
    .visit('/');

  cy.getTestId('share-record-card-list')
    .getTestId('share-record-card')
    .click();
});
