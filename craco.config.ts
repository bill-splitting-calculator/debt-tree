import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer';

const cracoConfig = {
  webpack: {
    plugins: [
      [
        new BundleAnalyzerPlugin({analyzerMode: 'server'}),
      ],
    ],
  },
};

export default cracoConfig;
